# BounceGame to-do list

* Expressive character animation

## Required basic functionality

* Gameplay
    - Jump
    - Calculate bounces
    * Level change
    * Tiles affect entities
* Interface
    * Pause screen
    * Game over
    * Main menu
* Graphics
    * Particle effects
    * Level background, parallax?

## Design

* Special game things (black holes, portals, whatever)
* Level themes/progression/"worlds"
    * Labs
    * Outside labs (city?)
    * Outer space
    * ???

## Long-term

* Sound effects
* Level editor/loader
* Particle effects

## Bugs

- Fullscreen mouse broken
* Tile (0,0) display bug

