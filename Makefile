##################################
# Makefile for procgame projects #
##################################

help:
	@echo ""
	@echo "Procgame Makefile Usage"
	@echo "======================="
	@echo ""
	@echo " What Do I Do?"
	@echo "---------------"
	@echo " tl;dr   'make game BUILD_TYPE=release'"
	@echo " If you are just trying to compile a game for yourself, you most likely"
	@echo "want 'make game BUILD_TYPE=release'. This will dynamic link with all"
	@echo "the dependency libraries (SDL2, GLEW, libYAML) so ensure you have those"
	@echo "installed. The 'xxd' utility is also required, unless 'PACK_SHADERS=no'"
	@echo "is set."
	@echo ""
	@echo " To make proper distributable builds, it is recommended to also use the"
	@echo "'STATIC_XXXX=yes' options for all the dependency libraries (ie. SDL2,"
	@echo "GLEW, YAML) so end-users aren't required to have them installed. This"
	@echo "requires static libraries to be present in the expected locations for"
	@echo "each library."
	@echo ""
	@echo " You will want to always invoke the Makefile targets with the exact same"
	@echo "options when working with the same build, or it may fail, or a broken"
	@echo "build may be produced!"
	@echo ""
	@echo ""
	@echo " Static libraries"
	@echo "------------------"
	@echo " When linking a third-party library, the directory structure is always"
	@echo "expected to be like so: '<LIB_DIR>/<LIB_NAME>/[*.h],[*.a]'."
	@echo "This may require moving the build results for each respective library"
	@echo "around, an operation which may be automated later, but currently isn't."
	@echo ""
	@echo ""
	@echo " Shader options"
	@echo "----------------"
	@echo " Procgame offers robust shader loading support. Shader sources can be"
	@echo "packed into the final binary and loaded from static memory at runtime."
	@echo "This is done when 'PACK_SHADERS=yes' is set. This behavior is also the"
	@echo "default behavior for 'BUILD_TYPE=release' builds. Shaders are loaded"
	@echo "(for compile-time packing OR for loading at runtime) from the directory"
	@echo "set in the 'SHADER_DIR' option."
	@echo ""
	@echo ""
	@echo " Command reference"
	@echo "-------------------"
	@echo "    'game':       Compile the project"
	@echo "    'clean'       Delete all the build files (one build directory only)"
	@echo "    'help':       Show this help message"
	@echo ""
	@echo ""
	@echo " Options reference"
	@echo "-------------------"
	@echo "    TARGET=target                    (default: procgame[.exe])"
	@echo "    BUILD_DIR=path/to/build          (default: ./build)"
	@echo "    BUILD_TYPE=debug|release         (default: debug)"
	@echo "    BUILD_PLATFORM=linux|windows     (default: linux)"
	@echo "    BUILD_CFLAGS=compiler flags      (default: empty)"
	@echo "    STATIC_LIB_DIR=path/to/libs      (default: ./libs/<platform>)"
	@echo "    STATIC_SDL2=yes|no               (default: no)"
	@echo "    STATIC_SDL2_DIR=path/to/libs     (default: STATIC_LIB_DIR)"
	@echo "    STATIC_GLEW=yes|no               (default: no)"
	@echo "    STATIC_GLEW_DIR=path/to/libs     (default: STATIC_LIB_DIR)"
	@echo "    STATIC_YAML=yes|no               (default: no)"
	@echo "    STATIC_YAML_DIR=path/to/libs     (default: STATIC_LIB_DIR/libYAML)"
	@echo "    SHADER_DIR=path/to/shaders       (default: ./src/procgl/shaders)"
	@echo "    PACK_SHADERS=yes|no              (default: no)"
	@echo ""



#########################################
# Basic build options                   #
#########################################
BUILD_DIR = ./build
BUILD_CODEGEN_DIR = $(BUILD_DIR)/gen
BUILD_TYPE = debug
BUILD_PLATFORM = linux

ifeq ($(BUILD_PLATFORM),windows)
    CC = x86_64-w64-mingw32-gcc
    TARGET = procgame.exe
    STATIC_LIB_DIR = ./libs/windows
else 
    CC = gcc
    TARGET = procgame
    STATIC_LIB_DIR = ./libs/linux
endif

SHADER_DIR = src/procgl/shaders
ifeq ($(BUILD_TYPE),debug)
    PACK_SHADERS = no
else
    PACK_SHADERS = yes
endif



#########################################
# Compile flags                         #
#########################################
CFLAGS_BASE := $(BUILD_CFLAGS) -g -Wall -Wno-unused-variable -Wno-unused-function -std=gnu11

ifeq ($(BUILD_TYPE),release)
    CFLAGS_BASE += -O3 -flto
else
    CFLAGS_BASE += -O0
endif

ifeq ($(BUILD_PLATFORM),windows)
    CFLAGS_PLATFORM := -Wl,--export-all-symbols -mwindows
else
    CFLAGS_PLATFORM := -rdynamic
endif



#########################################
# Linker flags                          #
#########################################
STATIC_LINK =
DYNAMIC_LINK =

LINK_SDL2_LINUX = -lSDL2
LINK_SDL2_WINDOWS = -lSDL2main -lSDL2

LINK_GLEW_LINUX = -lGLEW -lGL
LINK_GLEW_WINDOWS = -lglew32 -lopengl32

LINK_YAML_LINUX = -lyaml
LINK_YAML_WINDOWS = -lyaml


# Platform selection
ifeq ($(BUILD_PLATFORM),windows)
    LINK_SDL2 = $(LINK_SDL2_WINDOWS)
    LINK_GLEW = $(LINK_GLEW_WINDOWS)
    LINK_YAML = $(LINK_YAML_WINDOWS)
    LINK_PLATFORM := \
        -lmingw32 -lgdi32 -lwinmm -limm32 -lole32 -loleaut32 -lversion \
        -lopengl32 -lws2_32 -limagehlp
else
    LINK_SDL2 = $(LINK_SDL2_LINUX)
    LINK_GLEW = $(LINK_GLEW_LINUX)
    LINK_YAML = $(LINK_YAML_LINUX)
    LINK_PLATFORM := -lGL -lm
endif


# SDL2
ifeq ($(STATIC_SDL2),yes)
ifdef STATIC_SDL2_DIR
    LIB_SDL2 = -L$(STATIC_SDL2_DIR)
    INCLUDE_SDL2 = -I$(STATIC_SDL2_DIR)
else
    LIB_SDL2 = -L$(STATIC_LIB_DIR)
    INCLUDE_SDL2 = -I$(STATIC_LIB_DIR)
endif
    STATIC_LINK += $(LINK_SDL2)
else
    LIB_SDL2 =
    DYNAMIC_LINK += $(LINK_SDL2)
endif


# GLEW
ifeq ($(STATIC_GLEW),yes)
ifdef STATIC_GLEW_DIR
    LIB_GLEW = -L$(STATIC_GLEW_DIR)
    INCLUDE_GLEW = -I$(STATIC_GLEW_DIR)
else
    LIB_GLEW = -L$(STATIC_LIB_DIR)
    INCLUDE_GLEW = -I$(STATIC_LIB_DIR)
endif
    STATIC_LINK += $(LINK_GLEW)
    CFLAGS_BASE += -DGLEW_STATIC
else
    LIB_GLEW =
    DYNAMIC_LINK += $(LINK_GLEW)
endif


# libYAML
ifeq ($(STATIC_YAML),yes)
ifdef STATIC_YAML_DIR
    LIB_YAML = -L$(STATIC_YAML_DIR)
    INCLUDE_YAML = -I$(STATIC_YAML_DIR)
else
    LIB_YAML = -L$(STATIC_LIB_DIR)/libYAML
    INCLUDE_YAML = -I$(STATIC_LIB_DIR)/libYAML
endif
    STATIC_LINK += $(LINK_YAML)
else
    LIB_YAML =
    DYNAMIC_LINK += $(LINK_YAML)
endif



#########################################
# The Stuff                             #
#########################################
INCLUDES =  -Isrc -I$(BUILD_DIR) -I$(BUILD_CODEGEN_DIR) $(INCLUDE_GLEW) $(INCLUDE_SDL2)
CFLAGS =    $(CFLAGS_BASE) $(CFLAGS_PLATFORM)
LINKFLAGS = $(LIB_GLEW) $(LIB_SDL2) \
    -Wl,-Bstatic $(STATIC_LINK) -Wl,-Bdynamic $(DYNAMIC_LINK) $(LINK_PLATFORM)

GAME_OBJS =     $(patsubst src%,$(BUILD_DIR)/obj%,$(patsubst %.c,%.o,$(wildcard src/game/*.c)))
GAME_UI_OBJS =  $(patsubst src%,$(BUILD_DIR)/obj%,$(patsubst %.c,%.o,$(wildcard src/game/ui/*.c)))
PROCGAME_OBJS = $(patsubst src%,$(BUILD_DIR)/obj%,$(patsubst %.c,%.o,$(wildcard src/procgl/*.c)))
EXT_OBJS =      $(patsubst src%,$(BUILD_DIR)/obj%,$(patsubst %.c,%.o,$(wildcard src/procgl/ext/*.c)))

GAME_HEADERS =      $(wildcard src/game/*.h)
GAME_UI_HEADERS =   $(wildcard src/game/ui/*.h)
PROCGAME_HEADERS =  $(wildcard src/procgl/*.h)
EXT_HEADERS =       $(wildcard src/procgl/ext/*.h)

game: build_dirs shaders procgame

procgame: $(BUILD_DIR)/obj/main.o $(GAME_OBJS) $(GAME_UI_OBJS) $(PROCGAME_OBJS) $(EXT_OBJS)
	$(CC) $(CFLAGS) -o $(BUILD_DIR)/$(TARGET) $(BUILD_DIR)/obj/main.o \
        $(GAME_OBJS) $(GAME_UI_OBJS) $(PROCGAME_OBJS) $(EXT_OBJS) \
        $(LINKFLAGS)
$(BUILD_DIR)/obj/main.o: src/main.c $(GAME_HEADERS) $(PROCGAME_HEADERS) $(EXT_HEADERS)
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ src/main.c
$(BUILD_DIR)/obj/game/%.o: src/game/%.c $(GAME_HEADERS) $(PROCGAME_HEADERS) $(EXT_HEADERS)
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<
$(BUILD_DIR)/obj/game/ui/%.o: src/game/ui/%.c $(GAME_HEADERS) $(GAME_UI_HEADERS) $(PROCGAME_HEADERS) $(EXT_HEADERS)
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<
$(BUILD_DIR)/obj/procgl/%.o: src/procgl/%.c $(PROCGAME_HEADERS) $(EXT_HEADERS)
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<
$(BUILD_DIR)/obj/procgl/ext/%.o: src/procgl/ext/%.c $(EXT_HEADERS)
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<



#########################################
# Special Recipes                       #
#########################################
# Pack all the glsl sources into a C header file for release
ifeq ($(PACK_SHADERS),yes)
SHADER_SOURCES = $(wildcard $(SHADER_DIR)/*.glsl)
$(SHADER_SOURCES):
	xxd -i $@ | sed 's/\([0-9a-f]\)$$/\0, 0x00/' >> $(BUILD_CODEGEN_DIR)/shader_sources.glsl.h
shader_sources: $(SHADER_SOURCES)
shaders_preamble:
	echo "#ifndef PG_STATIC_SHADERS_INCLUDED" > $(BUILD_CODEGEN_DIR)/shader_sources.glsl.h
	echo "#define PG_STATIC_SHADERS_INCLUDED" >> $(BUILD_CODEGEN_DIR)/shader_sources.glsl.h
shaders_end:
	sed -i -e 's/unsigned char/static char/g' $(BUILD_CODEGEN_DIR)/shader_sources.glsl.h
	sed -i -e 's/unsigned int/static unsigned/g' $(BUILD_CODEGEN_DIR)/shader_sources.glsl.h
	sed -i -e 's/\(procgl_\|src_\|shaders_\|_glsl\)//g' $(BUILD_CODEGEN_DIR)/shader_sources.glsl.h
	echo "#endif" >> $(BUILD_CODEGEN_DIR)/shader_sources.glsl.h
shaders: shaders_preamble shader_sources shaders_end
else
shaders:
endif


# Create build directories
BUILD_OBJ_DIRS = $(BUILD_DIR)/obj \
                 $(BUILD_DIR)/obj/game $(BUILD_DIR)/obj/game/ui \
                 $(BUILD_DIR)/obj/procgl $(BUILD_DIR)/obj/procgl/ext
build_dirs: $(BUILD_DIR) $(BUILD_CODEGEN_DIR) $(BUILD_OBJ_DIRS)
$(BUILD_DIR):
	@mkdir -p $(BUILD_DIR)
$(BUILD_CODEGEN_DIR):
	@mkdir -p $(BUILD_CODEGEN_DIR)
$(BUILD_OBJ_DIRS):
	@mkdir -p $@


# Delete all build files
clean:
	rm -f $(BUILD_DIR)/obj/main.o $(GAME_OBJS) $(GAME_UI_OBJS) $(PROCGAME_OBJS) $(EXT_OBJS) \
          $(BUILD_CODEGEN_DIR)/shader_sources.glsl.h \
          $(BUILD_DIR)/$(TARGET) $(BUILD_DIR)/$(TARGET).exe



#########################################
.PHONY: $(SHADER_SOURCES) shaders help
