# BounceGame - Development log
This is a GAME development log, and for a game which was meant to be developed
from start to finish in a pretty short span of time (a few months maximum),
so if you're looking for some high-level software architecting this ain't the
place. While I do have a basic idea of a workable game architecture in mind
just from past experience in gamedev, my workflow for making a videogame
in an acceptable timeframe which is finished, polished, and shippable, is...
not particularly structured. I won't be laying out a huge framework for how
the game logic operates. I start with a very basic feature (a basic entity
system), then iterate and expand outward from there as I see development
targets that I feel can be completed in the shortest time possible. As I go,
I'll add new targets to a simple to-do list, maybe with some notes. Rinse,
repeat, ship.

I'm making this log because some people expressed interest in getting a look
at how I go about making a proper finished videogame as a solo developer, at
a pretty rapid pace. To that end I've included my free-form TODO file in the
git repository, as a secondary way to see my basic mental process of selecting
low-hanging tasks, and adding new tasks as I progress. I may also include
screenshots of the process, since everyone likes a visual aid and maybe some
people don't really know how (or want) to examine a project's history in git.

So this is my devlog. Hope you get some value out of it.

## Wednesday Sep 05 | Zero-th steps; a program that runs and does nothing
Basically `git branch` from my marsgame branch, nuke the whole game directory,
ie. all the gamecode, except for a bare-bones gamestate object which just
loads some font assets, creates a window, and does nothing else besides wait
for an exit signal.

Relevant commits: 1e228fe2, 8ce37504

## Saturday Sep 08 | First steps; make a bit of foundational structure
Since I had just been working on the marsgame entity system, I copied over
the entity structure from that, and cut out some parts which relied on systems
not implemented in this game yet (or which would never be). These entities
consist of:

* Spatial/physics data
    * Bounding box
    * Position/velocity (two copies; last frame and current frame)
* Game data common to all entities
    * Name (char[32]) - I usually only use this for debugging, but who knows
    * HP - might not always be used, but often enough
    * Bitfield (uint64_t) for entity flags. I'll use this for most "boolean"
        properties that entities might have
* Type data
    * Type id (int)
    * Type data (char[128]) - raw data block for use in behavior functions
    * Behavior functions - function pointers which define the basic interface
        for the various entity types. I start with just `tick` and `draw`,
        but I could add more later, for instance marsgame had those plus
        `raycast`, `use`, and `interact`. One might understand this as a sort
        of hard-coded ECS

They are allocated using procgame's mempool system, and stored/referred to with
indices into the (global) entity pool, as `bg_entpool_id_t`. This is purely
a memory allocation system. Entity ids are stored by the game in whatever data
structures I decide to use for actual game purposes (for instance an active vs.
inactive list, or a spatial partitioning structure).

This isn't much, and is "foundational" in a bit the same sense as some bags
of un-poured concrete are the foundation of a house. Maybe it is better to
think of this as the seed crystal from which the rest of the program will
grow. Everything after this will essentially be built with this as a reference
point.

Relevant commits: d0f521c3

## Sunday Sep 09 | Some interactivity!
With the entity structure added to the codebase, the next step is to implement
a player entity. At this early stage my goal isn't to actually make any
gameplay functionality. All I want is to have an entity which displays
on-screen in a way I expect it to, and which responds to player input *at all*.
In this case, that is by zooming around the screen when the WASD keys are
pressed.

Relevant commits: 0291d8ea

## Wednesday Sep 12 | Making a game-world
Some days lost to PC hardware trouble, then back to work.

Now that I can create new entity types easily, and they can be drawn on-screen
correctly, I want to make a structure for the next most important thing in the
game. That's a level for the entities to exist in, with walls and floors and
things. The strategy for this is the same as I did before: start out by just
making a basic structure which can draw things on-screen how I want, without
any game logic, collisions, etc. Those things will come a bit later, I think.

Relevant commits: f9b378aa

### Collision detection and response
I decided to tackle collision handling early since I knew this particular
mechanic would be integral to the rest of the game, due to the fact that the
primary mechanic is bouncing off walls or other objects. Fortunately since
this is a 2D game, the collision handling wasn't terribly difficult and I
implemented AABB vs. level, and circle vs. level collision functions. The code
appears to work as expected so far, and I'm pretty happy with the structure of
it, at least for now. Hopefully it will hold up when I get around to making
the sort of wild bouncing and jumping around physics I intend to do later.

Relevant commits: 5439d6e7

### About coordinate systems
The first problem I found when writing the collision code was that the player's
position coords were in pixels rather than a logically defined coordinate
space. Meanwhile, the level's tiles were being drawn at 16-pixel intervals. As
a result, the very first tests involved the player O colliding (incorrectly,
even) against a little 64x64 block of pixels, one pixel per tile, instead of
the tiles as drawn on-screen. Coordinate system mismatch! So I decided that the
coordinate system the game should use is one where each tile is 1 unit of
space, and 1 unit of space should correlate to 16 pixels when drawing. That
means the player's position must be multiplied by 16 in order to draw them
correctly, and the movement speed had to reduced so they didn't move a full
tile's width in each game tick. Figuring out a logical coordinate space is
something one has to do for every game, and ideally that space should be
consistent throughout all the systems in the game, otherwise you will be
constantly converting between different coordinate spaces to do basic logic.

A sharp-eyed reader might notice that I still have a coordinate inconsistency
right in this description: logical coordinates 1 per tile, but screen
coordinates 16 per tile!  I've made this decision specifically because I intend
for the game to always be drawn at the same resolution (right now I think
320x200), and scale up and/or letterbox to fit the user's preferred window
resolution. If this game were a 3D game, or would have vector graphics, or was
otherwise not meant to have precise and chunky pixel graphics, it would also be
good to devise a way to define positions in screen-space in a way which exactly
aligns with your in-game coordinate system. That would allow very easily
defining viewport positions, viewport zoom or scaling, etc. in a way that is
intuitive and consistent with everything else in the game. Then only the final
transformation required to draw real pixels is the only coordinate space
conversion required, and would be done in the low-level OpenGL or SDL (or
whatever you're using) functions.

This is even something one should think about
when using a GUI-based game engine which abstracts away all the raw drawing
logic for you. It will just make all your spatially-oriented game logic like
physics, targeting, or effect areas a lot easier to reason about as you work.

## Thursday-Friday Sep 13-14 | Actual game mechanics

Now that the most basic required features are implemented, I think I have
enough to make a first stab at the core game mechanic: bouncing off walls. My
first thought was to use a raycasting function on the level's grid, but that
presented some problems. First, the raycast would become much more complicated
when I eventually add some level features that I have in mind (especially ones
which can bend the trajectory when "in flight"). Second, a raycast would not
precisely match the geometry of the player which is a circle in motion, rather
than a ray through space. This might have caused the player to incorrectly pass
over a corner, or to overlap with walls when the ray is almost parallel to the
wall.  So I chose instead to use the "brute force" method for raycasting, in
which a circle is simply swept along the intended trajectory, one pixel (or
several pixels) at a time, checking for collisions at each step. Then a bounce
is calculated as a reflection (see `vec2_reflect` in game_math.h) of the flight
direction, with the collision's separating axis as the surface normal. Once a
bounce is calculated - in the code as it is now, the following four are
calculated at any given time to display for debugging purposes - the player
entity simply moves on a straight path toward the next one. No physics or
collisions are done when the player is in flight.

Relevant commits: 76c50fc9
