#include "procgl.h"

FILE* logfile = NULL;
enum pg_log_type log_level = PG_LOG_DEBUG;
enum pg_log_type last_log = PG_LOG_DEBUG;

void pg_log_open(const char* filename)
{
    logfile = fopen(filename, "wb");
    if(!logfile) {
        printf("[ERROR] Failed to open logfile for writing: %s\n", filename);
    }
}

void pg_log_close(void)
{
    if(logfile) fclose(logfile);
}

void pg_log_level(enum pg_log_type level)
{
    log_level = level;
}

static const char* log_type_string[] = {
    [PG_LOG_DEBUG] = "DEBUG",
    [PG_LOG_INFO] = "INFO",
    [PG_LOG_WARNING] = "WARNING",
    [PG_LOG_ERROR] = "ERROR",
    [PG_LOG_ERROR_FATAL] = "!!!FATAL ERROR!!!",
};

void pg_log_(enum pg_log_type type, const char* fmt,
             int src_line, const char* src_filename, ...)
{
    if(!logfile || (type == PG_LOG_CONTD && last_log < log_level)) {
        return;
    } else if(type != PG_LOG_CONTD && type < log_level) {
        last_log = type;
        return;
    }
    if(type != PG_LOG_CONTD) {
        fprintf(logfile, "  [%s] %s:%d\n",
                log_type_string[type], src_filename, src_line);
    }
    va_list args;
    va_start(args, src_filename);
    vfprintf(logfile, fmt, args);
    va_end(args);
    fprintf(logfile, "\n");
    fflush(logfile);
}
