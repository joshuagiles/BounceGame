/*  Standard library    */
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

/*  External dependencies   */
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <yaml.h>

/*  Third-party code which is integrated into the procgame codebase.
 *  May contain modifications relative to their "official" repositories.    */
#include "ext/stb_rect_pack.h"
#include "ext/stb_truetype.h"
#include "ext/linmath.h"
#include "ext/ksort.h"
#include "ext/cJSON.h"

/************************/
/*  Procgame headers    */
/************************/
/*  Some basic macro headers    */
#include "arr.h"
#include "mempool.h"
#include "htable.h"

/*  Other simple utility things */
#include "utility.h"
#include "data_type.h"
#include "json_utility.h"

/*  Basic procgame  */
#include "procgl_base.h"
#include "logging.h"
#include "input.h"

/*  Procedural generation   */
#include "wave.h"
#include "heightmap.h"

/*  Audio   */
#include "audio.h"

/*  Textures, renderbuffers, anything that goes in a GL texture */
#include "texture.h"
#include "rendertarget.h"
#include "buffertex.h"
#include "asset_image.h"
#include "font.h"

/*  Rendering   */
#include "viewer.h"
#include "shader.h"
#include "vertex_buffer.h"
#include "renderer.h"

/*  Built-in model and rigging structures   */
#include "armature.h"
#include "model.h"

/*  Convenience rendering operations    */
#include "quadbatch.h"
#include "ezpost.h"
#include "ezbox.h"

/*  UI  */
#include "easing.h"
#include "animation.h"
#include "ui.h"

/*  Game interface  */
#include "game_state.h"
