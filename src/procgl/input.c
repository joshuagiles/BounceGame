#include "procgl.h"

/************/
/*  Input   */
/************/

/*  Global input state information  */
static int user_exit;
/*  Keyboard    */
#define PG_INPUT_QUEUE_SIZE         (16)
#define PG_INPUT_KB_CONTROLS        (256)
static uint8_t kb_state[PG_INPUT_KB_CONTROLS];
static int kb_changes[PG_INPUT_QUEUE_SIZE];
static int kb_changed;
static int text_mode_active;
static char text_buffer[PG_INPUT_QUEUE_SIZE];
static char text_len;
/*  Mouse   */
#define PG_INPUT_MOUSE_CONTROLS     (32)
static uint8_t mouse_state[PG_INPUT_MOUSE_CONTROLS];
static int mouse_changes[PG_INPUT_QUEUE_SIZE];
static int mouse_changed;
static vec2 mouse_pos;
static vec2 mouse_motion;
static enum pg_mouse_mode mouse_mode;
/*  Gamepad (one only, for now) */
#define PG_INPUT_GAMEPAD_CONTROLS     (32)
static uint8_t gamepad_state[PG_INPUT_GAMEPAD_CONTROLS];
static uint8_t gamepad_changes[PG_INPUT_QUEUE_SIZE];
static uint8_t gamepad_changed;
static vec2 gamepad_stick[2];
static float gamepad_trigger[2];
/*  Gamepad configuration   */
static SDL_GameController* gamepad;
static SDL_JoystickID gamepad_id;
static float stick_dead_zone, stick_threshold;
static float trigger_dead_zone, trigger_threshold;

/*  Procgame input handling     */
void pg_input_poll(void)
{
    /*  First consume all the SDL events. This covers all physical button
     *  inputs, scroll wheeling, and gamepad [dis-]connects.    */
    SDL_Event e;
    while(SDL_PollEvent(&e)) {
        if(e.type == SDL_QUIT) {
            user_exit = 1;
        /************************/
        /*  Keyboard key events */
        } else if(e.type == SDL_KEYDOWN) {
            if(kb_state[e.key.keysym.scancode] != PG_CONTROL_HELD) {
                kb_state[e.key.keysym.scancode] = PG_CONTROL_HIT;
                kb_changes[kb_changed++] = e.key.keysym.scancode;
            }
        } else if(e.type == SDL_KEYUP) {
            kb_state[e.key.keysym.scancode] = PG_CONTROL_RELEASED;
            kb_changes[kb_changed++] = e.key.keysym.scancode;
        /*  Text input events   */
        } else if(e.type == SDL_TEXTINPUT && text_mode_active && text_len < 16) {
            int len = strlen(e.text.text);
            int buf_left = 16 - text_len;
            strncpy(text_buffer + text_len, e.text.text, buf_left);
            text_len = LM_MIN(16, text_len + len);
        /************************/
        /*  Mouse button events */
        } else if(e.type == SDL_MOUSEBUTTONDOWN || e.type == SDL_MOUSEBUTTONUP) {
            int down = (e.type == SDL_MOUSEBUTTONDOWN);
            switch(e.button.button) {
            case SDL_BUTTON_LEFT:
                kb_state[PG_LEFT_MOUSE] =
                    down ? PG_CONTROL_HIT : PG_CONTROL_RELEASED;
                kb_changes[kb_changed++] = PG_LEFT_MOUSE;
                break;
            case SDL_BUTTON_RIGHT:
                kb_state[PG_RIGHT_MOUSE] =
                    down ? PG_CONTROL_HIT : PG_CONTROL_RELEASED;
                kb_changes[kb_changed++] = PG_RIGHT_MOUSE;
                break;
            case SDL_BUTTON_MIDDLE:
                kb_state[PG_MIDDLE_MOUSE] =
                    down ? PG_CONTROL_HIT : PG_CONTROL_RELEASED;
                kb_changes[kb_changed++] = PG_MIDDLE_MOUSE;
                break;
            }
        /*  Mouse wheel events  */
        } else if(e.type == SDL_MOUSEWHEEL) {
            if(e.wheel.y == 1) {
                kb_state[PG_MOUSEWHEEL_UP] = PG_CONTROL_HIT;
                kb_changes[kb_changed++] = PG_MOUSEWHEEL_UP;
            } else if(e.wheel.y == -1) {
                kb_state[PG_MOUSEWHEEL_DOWN] = PG_CONTROL_HIT;
                kb_changes[kb_changed++] = PG_MOUSEWHEEL_DOWN;
            }
        /************************/
        /*  Gamepad button events   */
        } else if(e.type == SDL_CONTROLLERBUTTONDOWN && e.cbutton.which == gamepad_id) {
            if(gamepad_state[e.cbutton.button] != PG_CONTROL_HELD) {
                gamepad_state[e.cbutton.button] = PG_CONTROL_HIT;
                gamepad_changes[gamepad_changed++] = e.cbutton.button;
            }
        } else if(e.type == SDL_CONTROLLERBUTTONUP && e.cbutton.which == gamepad_id) {
            gamepad_state[e.cbutton.button] = PG_CONTROL_RELEASED;
            gamepad_changes[gamepad_changed++] = e.cbutton.button;
        /*  Gamepad device events   */
        } else if(e.type == SDL_CONTROLLERDEVICEREMOVED && e.cdevice.which == gamepad_id) {
            printf("Gamepad disconnected.\n");
            pg_input_gamepad_select(-1);
            SDL_GameControllerClose(gamepad);
            gamepad = NULL;
        } else if(e.type == SDL_CONTROLLERDEVICEADDED) {
            pg_input_gamepad_select(e.cdevice.which);
            printf("Gamepad connected.\n");
        }
    }
    /*  Next handle mouse position/motion via SDL. Coordinates are converted
     *  to [-1,1] in both axes, with (0,0) in the center of the window  */
    vec2 screen_sz = pg_screen_size();
    vec2 win_sz = pg_window_size();
    vec2 win_scale = vec2_div(win_sz, screen_sz);
    int mx, my;
    if(mouse_mode == PG_MOUSE_CAPTURE) {
        SDL_GetRelativeMouseState(&mx, &my);
        vec2 motion = vec2(mx, my);
        mouse_motion = vec2_add(mouse_motion, motion);
        mouse_pos = vec2_add(mouse_pos, motion);
    } else if(mouse_mode == PG_MOUSE_FREE) {
        SDL_GetMouseState(&mx, &my);
        vec2 mouse = vec2(mx, screen_sz.y - my);
        mouse = vec2_mul(mouse, win_scale);
        mouse = vec2(mouse.x / win_sz.x * 2.0f - 1.0f,
                     mouse.y / win_sz.y * 2.0f - 1.0f);
        /*
        vec2 mouse = vec2( ((float)mx / screen_sz.x) * 2.0f - 1.0f,
                           -(((float)my / screen_sz.y) * 2.0f - 1.0f) );*/
        mouse_motion = vec2_sub(mouse_pos, mouse);
        mouse_pos = mouse;
    }
    /*  Next all the gamepad axes are gotten (with SDL). The four stick axes
     *  (XY for both sticks), and both triggers. The values are adjusted based
     *  on the gamepad configuration, and if they exceed the configured button
     *  thresholds, they will also generate "button-style" inputs accessible
     *  with the button-oriented input functions.   */
    if(gamepad) {
        int16_t left[2], right[2], trigger[2];
        /*  Interface SDL   */
        left[0] = SDL_GameControllerGetAxis(gamepad, SDL_CONTROLLER_AXIS_LEFTX);
        left[1] = SDL_GameControllerGetAxis(gamepad, SDL_CONTROLLER_AXIS_LEFTY);
        right[0] = SDL_GameControllerGetAxis(gamepad, SDL_CONTROLLER_AXIS_RIGHTX);
        right[1] = SDL_GameControllerGetAxis(gamepad, SDL_CONTROLLER_AXIS_RIGHTY);
        trigger[0] = SDL_GameControllerGetAxis(gamepad, SDL_CONTROLLER_AXIS_TRIGGERLEFT);
        trigger[1] = SDL_GameControllerGetAxis(gamepad, SDL_CONTROLLER_AXIS_TRIGGERRIGHT);
        /*  Convert to floats in [-1,1] or [0,1] (for triggers) */
        gamepad_stick[0] = vec2( (float)left[0] / INT16_MAX, (float)left[1] / INT16_MAX );
        gamepad_stick[1] = vec2( (float)right[0] / INT16_MAX, (float)right[1] / INT16_MAX );
        gamepad_trigger[0] = trigger[0] / INT16_MAX;
        gamepad_trigger[1] = trigger[1] / INT16_MAX;
        /*  Adjust for joystick and trigger dead-zone configuration */
        float stick_live_zone = 1.0f - stick_dead_zone;
        if(fabsf(gamepad_stick[0].x) < stick_dead_zone) gamepad_stick[0].x = 0;
        else gamepad_stick[0].x = (gamepad_stick[0].x - stick_dead_zone) / stick_live_zone;
        if(fabsf(gamepad_stick[0].x) < stick_dead_zone) gamepad_stick[0].y = 0;
        else gamepad_stick[0].y = (gamepad_stick[0].y - stick_dead_zone) / stick_live_zone;
        if(fabsf(gamepad_stick[1].x) < stick_dead_zone) gamepad_stick[1].x = 0;
        else gamepad_stick[1].x = (gamepad_stick[1].x - stick_dead_zone) / stick_live_zone;
        if(fabsf(gamepad_stick[1].y) < stick_dead_zone) gamepad_stick[1].y = 0;
        else gamepad_stick[1].y = (gamepad_stick[1].y - stick_dead_zone) / stick_live_zone;
        if(gamepad_trigger[0] < trigger_dead_zone) gamepad_trigger[0] = 0;
        if(gamepad_trigger[1] < trigger_dead_zone) gamepad_trigger[1] = 0;
        /*  Also provide a button-like interface to the joysticks and triggers  */
        if(vec2_len(gamepad_stick[0]) > stick_threshold) {
            if(gamepad_state[PG_LEFT_STICK] != PG_CONTROL_HELD) {
                gamepad_state[PG_LEFT_STICK] = PG_CONTROL_HIT;
                gamepad_changes[gamepad_changed++] = PG_LEFT_STICK;
            }
        } else if(gamepad_state[PG_LEFT_STICK] == PG_CONTROL_HELD) {
            gamepad_state[PG_LEFT_STICK] = PG_CONTROL_RELEASED;
            gamepad_changes[gamepad_changed++] = PG_LEFT_STICK;
        }
        if(vec2_len(gamepad_stick[1]) > stick_threshold) {
            if(gamepad_state[PG_RIGHT_STICK] != PG_CONTROL_HELD) {
                gamepad_state[PG_RIGHT_STICK] = PG_CONTROL_HIT;
                gamepad_changes[gamepad_changed++] = PG_RIGHT_STICK;
            }
        } else if(gamepad_state[PG_RIGHT_STICK] == PG_CONTROL_HELD) {
            gamepad_state[PG_RIGHT_STICK] = PG_CONTROL_RELEASED;
            gamepad_changes[gamepad_changed++] = PG_RIGHT_STICK;
        }
        if(gamepad_trigger[0] > trigger_threshold) {
            if(gamepad_state[PG_LEFT_TRIGGER] != PG_CONTROL_HELD) {
                gamepad_state[PG_LEFT_TRIGGER] = PG_CONTROL_HIT;
                gamepad_changes[gamepad_changed++] = PG_LEFT_TRIGGER;
            }
        } else if(gamepad_state[PG_LEFT_TRIGGER] == PG_CONTROL_HELD) {
            gamepad_state[PG_LEFT_TRIGGER] = PG_CONTROL_RELEASED;
            gamepad_changes[gamepad_changed++] = PG_LEFT_TRIGGER;
        }
        if(gamepad_trigger[1] > trigger_threshold) {
            if(gamepad_state[PG_RIGHT_TRIGGER] != PG_CONTROL_HELD) {
                gamepad_state[PG_RIGHT_TRIGGER] = PG_CONTROL_HIT;
                gamepad_changes[gamepad_changed++] = PG_RIGHT_TRIGGER;
            }
        } else if(gamepad_state[PG_RIGHT_TRIGGER] == PG_CONTROL_HELD) {
            gamepad_state[PG_RIGHT_TRIGGER] = PG_CONTROL_RELEASED;
            gamepad_changes[gamepad_changed++] = PG_RIGHT_TRIGGER;
        }
    }
}

void pg_input_flush(void)
{
    int i;
    for(i = 0; i < kb_changed; ++i) {
        uint8_t c = kb_changes[i];
        switch(kb_state[c]) {
        case 0: break;
        case PG_CONTROL_HIT:
            kb_state[c] = PG_CONTROL_HELD;
            break;
        case PG_CONTROL_HELD: case PG_CONTROL_RELEASED:
            kb_state[c] = PG_CONTROL_OFF;
            break;
        }
    }
    mouse_motion = vec2(0);
    kb_changed = 0;
    text_len = 0;
    if(!gamepad) return;
    for(i = 0; i < gamepad_changed; ++i) {
        uint8_t c = gamepad_changes[i];
        switch(gamepad_state[c]) {
        case 0: break;
        case PG_CONTROL_HIT:
            gamepad_state[c] = PG_CONTROL_HELD;
            break;
        case PG_CONTROL_HELD: case PG_CONTROL_RELEASED:
            gamepad_state[c] = PG_CONTROL_OFF;
            break;
        }
    }
    gamepad_changed = 0;
}

void pg_input_reset(void)
{
    memset(kb_state, 0, sizeof(kb_state));
    memset(mouse_state, 0, sizeof(mouse_state));
    memset(gamepad_state, 0, sizeof(gamepad_state));
}

int pg_input_user_exit(void)
{
    return user_exit;
}

/************************/
/*  Keyboard input      */
/************************/
enum pg_input_state pg_input_kb_state(int ctrl)
{
    return kb_state[ctrl];
}

int pg_input_kb_check(int ctrl, enum pg_input_state event)
{
    if(kb_state[ctrl] & event) return 1;
    int changes = 0;
    int i;
    for(i = 0; i < kb_changed && i < 16; ++i) {
        changes += (kb_changes[i] == ctrl);
        if(changes > 1) return 1;
    }
    return 0;
}

int pg_input_kb_first(void)
{
    if(!kb_changed) return 0;
    int i = 0;
    while(i < kb_changed && i < 16 
    && kb_state[kb_changes[i]] == PG_CONTROL_RELEASED)
        ++i;
    if(i == kb_changed || i == 16) return 0;
    return kb_changes[i];
}

void pg_input_kb_text_mode(int mode)
{
    text_mode_active = mode;
}

int pg_input_kb_copy_text(char* out, int n)
{
    int len = LM_MIN(n, text_len);
    strncpy(out, text_buffer, len);
    out[len] = '\0';
    return len;
}

const char* pg_input_kb_ctrl_name(int ctrl)
{
    return SDL_GetKeyName(SDL_GetKeyFromScancode(ctrl));
}

/************************/
/*  Mouse input         */
/************************/

enum pg_input_state pg_input_mouse_state(int ctrl)
{
    return mouse_state[ctrl];
}

int pg_input_mouse_check(int ctrl, enum pg_input_state event)
{
    if(mouse_state[ctrl] & event) return 1;
    int changes = 0;
    int i;
    for(i = 0; i < mouse_changed; ++i) {
        changes += (mouse_changes[i] == ctrl);
        if(changes > 1) return 1;
    }
    return 0;
}

int pg_input_mouse_first(void)
{
    if(!mouse_changed) return 0;
    int i = 0;
    while(i < mouse_changed && mouse_state[mouse_changes[i]] == PG_CONTROL_RELEASED) ++i;
    if(i == mouse_changed) return 0;
    return mouse_changes[i];
}

vec2 pg_input_mouse_pos(void)
{
    return mouse_pos;
}

vec2 pg_input_mouse_motion(void)
{
    return mouse_motion;
}

void pg_input_mouse_mode(enum pg_mouse_mode mode)
{
    if(mode == PG_MOUSE_CAPTURE) {
        SDL_SetRelativeMouseMode(SDL_ENABLE);
        SDL_GetRelativeMouseState(NULL, NULL);
        mouse_mode = PG_MOUSE_CAPTURE;
    } else if(mode == PG_MOUSE_FREE) {
        SDL_SetRelativeMouseMode(SDL_DISABLE);
        mouse_mode = PG_MOUSE_FREE;
    }
}

const char* pg_input_mouse_ctrl_name(int ctrl)
{
    static const char* mouse_input_names[] = {
        "WHEEL UP", "WHEEL DOWN",
        "LEFT MOUSE", "RIGHT MOUSE", "MIDDLE MOUSE" };
    if(ctrl >= PG_MOUSEWHEEL_UP && ctrl <= PG_MIDDLE_MOUSE) {
        return mouse_input_names[ctrl - PG_MOUSEWHEEL_UP];
    }
    return "<null>";
}

/************************/
/*  Gamepad input       */
/************************/

enum pg_input_state pg_input_gamepad_state(int ctrl)
{
    return kb_state[ctrl];
}

int pg_input_gamepad_check(int ctrl, enum pg_input_state state)
{
    if(gamepad_state[ctrl] & state) return 1;
    int changes = 0;
    int i;
    for(i = 0; i < gamepad_changed; ++i) {
        changes += (gamepad_changes[i] == ctrl);
        if(changes > 1) return 1;
    }
    return 0;
}

int pg_input_gamepad_first(void)
{
    if(!gamepad_changed) return 0;
    int i = 0;
    while(i < gamepad_changed && gamepad_state[gamepad_changes[i]] == PG_CONTROL_RELEASED) ++i;
    if(i == gamepad_changed) return 0;
    return gamepad_changes[i];
}

vec2 pg_input_gamepad_stick(int side)
{
    return gamepad_stick[side % 2];
}

float pg_input_gamepad_trigger(int side)
{
    return gamepad_trigger[side % 2];
}

void pg_input_gamepad_select(int gpad_idx)
{
    if(gpad_idx == -1) {
        gamepad_id = -1;
        return;
    }
    gamepad = SDL_GameControllerOpen(gpad_idx);
    if(!gamepad) {
        printf("procgame failed to open gamepad %d\n", gpad_idx);
        gamepad_id = -1;
    } else {
        gamepad_id = gpad_idx;
        SDL_Joystick* joy = SDL_GameControllerGetJoystick(gamepad);
        gamepad_id = SDL_JoystickInstanceID(joy);
    }
}

int pg_input_gamepad_selected(void)
{
    return gamepad_id;
}

void pg_input_gamepad_config(float new_stick_dead_zone, float new_stick_threshold,
                             float new_trigger_dead_zone, float new_trigger_threshold)
{
    stick_dead_zone = new_stick_dead_zone;
    stick_threshold = new_stick_threshold;
    trigger_dead_zone = new_trigger_dead_zone;
    trigger_threshold = new_trigger_threshold;
}

const char* pg_input_gamepad_ctrl_name(int ctrl)
{
    static const char* gpad_input_names[PG_CONTROLLER_MAX] = {
        [SDL_CONTROLLER_BUTTON_A] = "A",
        [SDL_CONTROLLER_BUTTON_B] = "B",
        [SDL_CONTROLLER_BUTTON_X] = "X",
        [SDL_CONTROLLER_BUTTON_Y] = "Y",
        [SDL_CONTROLLER_BUTTON_BACK] = "BACK",
        [SDL_CONTROLLER_BUTTON_GUIDE] = "GUIDE",
        [SDL_CONTROLLER_BUTTON_START] = "START",
        [SDL_CONTROLLER_BUTTON_LEFTSTICK] = "Left Stick",
        [SDL_CONTROLLER_BUTTON_RIGHTSTICK] = "Right Stick",
        [SDL_CONTROLLER_BUTTON_LEFTSHOULDER] = "Left Shoulder",
        [SDL_CONTROLLER_BUTTON_RIGHTSHOULDER] = "R Shoulder",
        [SDL_CONTROLLER_BUTTON_DPAD_UP] = "D-Pad Up",
        [SDL_CONTROLLER_BUTTON_DPAD_DOWN] = "D-Pad Down",
        [SDL_CONTROLLER_BUTTON_DPAD_LEFT] = "D-Pad Left",
        [SDL_CONTROLLER_BUTTON_DPAD_RIGHT] = "D-Pad Right",
        [SDL_CONTROLLER_BUTTON_MAX] = "<none>",
        [PG_LEFT_STICK] = "Left Joystick",
        [PG_RIGHT_STICK] = "Right Joystick",
        [PG_LEFT_TRIGGER] = "Left Trigger",
        [PG_RIGHT_TRIGGER] = "Right Trigger" };
    if(ctrl == SDL_CONTROLLER_BUTTON_INVALID) return "<none>";
    else if(ctrl > PG_RIGHT_TRIGGER) return "<none>";
    else return gpad_input_names[ctrl];
}
