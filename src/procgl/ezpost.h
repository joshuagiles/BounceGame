/****************************************/
/*  Post-processing shaders             */
/****************************************/

/*  Wavy distortion effect  */
void pg_ezpost_sine(struct pg_renderpass* pass, struct pg_rendertarget* target);
void pg_ezpost_sine_add(struct pg_renderpass* pass, vec2 axis,
                        float frequency, float phase, float amplitude);

/*  Separable blur shader   */
void pg_ezpost_blur(struct pg_renderpass* pass, struct pg_rendertarget* target);
void pg_ezpost_blur_add(struct pg_renderpass* pass, vec2 axis0, vec2 axis1);

/*  Depth-fog shader    */
void pg_ezpost_fog(struct pg_renderpass* pass, struct pg_rendertarget* target);
void pg_ezpost_fog_add(struct pg_renderpass* pass, vec3 color, float near, float far);

/*  Pass the input directly to the screen   */
void pg_ezpost_screen(struct pg_renderpass* pass, struct pg_rendertarget* src);
