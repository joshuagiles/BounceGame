/************/
/*  Input   */
/************/
/** \defgroup Input */
/**@{*/

/** @name       Button inputs
 *  Button inputs are handled with a queue/commit system.
 *
 *  `pg_input_poll()` gathers all pending inputs to the program and stores them
 *  in input queues for each device.
 *
 *  `pg_input_flush()` empties the input queues and applies all the changes to
 *  the logical input state.
 *
 *  Between those two functions, 'check' functions are provided for all input
 *  devices, which can determine if a particular action has been taken on a
 *  button input, even though the logical input state will always be On/Off.
 *  (`PG_CONTROL_OFF` and `PG_CONTROL_HELD` input states)
 *
 *  Here is a timeline of a button-press and input states:
 *  ```
 *  Action:                 |Press  |Poll   |Flush  |Release|Poll   |Flush
 *  Input queue:            |       |Press  |       |       |Release|
 *  Internal state:     OFF |OFF    |OFF    |ON     |ON     |ON     |OFF
 *  check(OFF):         1   |1      |1      |0      |0      |0      |1
 *  check(HIT):         0   |0      |1      |0      |0      |0      |0
 *  check(HELD):        0   |0      |0      |1      |1      |1      |0
 *  check(RELEASED):    0   |0      |0      |0      |0      |1      |0
 *  ```
*/

enum pg_input_state {
    PG_CONTROL_OFF =        (1 << 0),
    PG_CONTROL_HIT =        (1 << 1),
    PG_CONTROL_HELD =       (1 << 2),
    PG_CONTROL_RELEASED =   (1 << 3),
};

/** @name       Input management  */
/** Poll all the input devices and store the result in the input queue  */
void pg_input_poll(void);
/** Clear the input queue and commit all the changes to the logical input state */
void pg_input_flush(void);
/** Clear all input states and clear the input queue    */
void pg_input_reset(void);
/** Check if an exit signal has been received   */
int pg_input_user_exit(void);

/** @name       Keyboard input    */
/** Get the logical state of a keyboard button (OFF or HELD)    */
enum pg_input_state pg_input_kb_state(int ctrl);
/** Check for a particular keyboard input   */
int         pg_input_kb_check(int ctrl, enum pg_input_state event);
/** Get the first keyboard button hit (0 if none)    */
int         pg_input_kb_first(void);
/** Enable or disable keyboard text inputs (may interfere with regular key inputs)  */
void        pg_input_kb_text_mode(int mode);
/** Copy all queued text inputs to a char buffer    */
int         pg_input_kb_copy_text(char* out, int n);
/** Get a string describing a keyboard button   */
const char* pg_input_kb_ctrl_name(int ctrl);

/** @name       Mouse input   */
enum pg_input_mouse {
    PG_MOUSEWHEEL_UP,
    PG_MOUSEWHEEL_DOWN,
    PG_LEFT_MOUSE,
    PG_RIGHT_MOUSE,
    PG_MIDDLE_MOUSE,
};

enum pg_mouse_mode {
    PG_MOUSE_CAPTURE,
    PG_MOUSE_FREE,
};

enum pg_input_state pg_input_mouse_state(int ctrl);
int         pg_input_mouse_check(int ctrl, enum pg_input_state event);
int         pg_input_mouse_first(void);
void        pg_input_mouse_mode(enum pg_mouse_mode mode);
vec2        pg_input_mouse_pos(void);
vec2        pg_input_mouse_motion(void);
const char* pg_input_mouse_ctrl_name(int ctrl);

/** @name       Gamepad input */
enum pg_input_gamepad {
    PG_LEFT_STICK = (SDL_CONTROLLER_BUTTON_MAX + 1),
    PG_RIGHT_STICK,
    PG_LEFT_TRIGGER,
    PG_RIGHT_TRIGGER,
    PG_CONTROLLER_MAX,
};

enum pg_input_state pg_input_gamepad_state(int ctrl);
int         pg_input_gamepad_check(int ctrl, enum pg_input_state event);
int         pg_input_gamepad_first(void);
vec2        pg_input_gamepad_stick(int side);
float       pg_input_gamepad_trigger(int side);
const char* pg_input_gamepad_ctrl_name(int ctrl);

/** @name       Gamepad config  */
void    pg_input_gamepad_select(int gpad_idx);
int     pg_input_gamepad_selected(void);
void    pg_input_gamepad_config(float stick_dead_zone, float stick_threshold,
                                float trigger_dead_zone, float trigger_threshold);

/**@}*/

