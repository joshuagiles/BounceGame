#version 330

uniform mat4 pg_matrix_mvp;
uniform float tex_layer;

uniform mat4 bone_mats[32];

in vec3 v_position;
in vec3 v_tex_coord;
in vec3 v_rigging;

out vec3 f_tex_coord;

void main()
{
    f_tex_coord = v_tex_coord;
    vec4 new_pos =
        ((bone_mats[int(v_rigging.x)] * v_rigging.z) * vec4(v_position.xyz, 1)) +
        ((bone_mats[int(v_rigging.y)] * (1 - v_rigging.z)) * vec4(v_position.xyz, 1));
    gl_Position = pg_matrix_mvp * vec4(new_pos.xyz, 1);
}
