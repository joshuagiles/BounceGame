#version 330

/*  Color   */
uniform sampler2DArray pg_texture_0;
uniform int pg_tex_layer_0;
/*  Depth   */
uniform sampler2DArray pg_texture_1;
uniform int pg_tex_layer_1;

uniform vec3 fog_color;
uniform vec2 fog_plane;

in vec2 f_tex_coord;

out vec4 frag_color;

void main()
{
    vec4 orig_color = texture(pg_texture_0, vec3(f_tex_coord, pg_tex_layer_0));
    float z_b = texture(pg_texture_1, vec3(f_tex_coord, pg_tex_layer_1)).r;
    float z_n = z_b;
    float depth = 2 * 100 * 0.01 / (100 + 0.01 - (100 - 0.01) * z_n);
    float fog_depth = (depth - fog_plane.x) / (fog_plane.y - fog_plane.x);
    fog_depth = clamp(fog_depth, 0, 1);
    frag_color = mix(vec4(orig_color.rgb, 1), vec4(fog_color.rgb, 1), fog_depth);
}

