#version 330

const vec4 verts[6] = vec4[](
           vec4(-0.5, 0.5, 0.00001, 0.00001),
           vec4(-0.5, -0.5,  0.00001, 0.99999),
           vec4(0.5, 0.5,  0.99999, 0.00001),
           vec4(0.5, 0.5,  0.99999, 0.00001),
           vec4(-0.5, -0.5,  0.00001, 0.99999),
           vec4(0.5, -0.5,   0.99999, 0.99999));

uniform mat4 pg_matrix_mvp;

#define sprites pg_texture_0
uniform samplerBuffer sprites;
uniform int sprite_offset;

out vec4 f_tex_coord;
out vec4 f_color_mod;
out vec4 f_color_add;

vec4 unpack_uint(uint u)
{
    return vec4(
        float((u & uint(0xFF000000)) >> 24),
        float((u & uint(0x00FF0000)) >> 16),
        float((u & uint(0x0000FF00)) >> 8),
        float((u & uint(0x000000FF)) >> 0)) / 255;
}

vec3 quat_apply(vec4 q, vec3 v)
{
    return v + 2.0 * cross(q.xyz, cross(q.xyz, v) + q.w * v);
}

void main()
{
    int buf_offset = gl_VertexID / 6 * 4;
    vec4 batch[4] = vec4[](
        vec4(texelFetch(sprites, sprite_offset + 0 + buf_offset)),
        vec4(texelFetch(sprites, sprite_offset + 1 + buf_offset)),
        vec4(texelFetch(sprites, sprite_offset + 2 + buf_offset)),
        vec4(texelFetch(sprites, sprite_offset + 3 + buf_offset)) );
    vec3 pos = batch[0].xyz;
    vec2 scale = vec2(batch[0].w, batch[1].x);
    f_color_add = unpack_uint(floatBitsToUint(batch[1].y));
    f_color_mod = unpack_uint(floatBitsToUint(batch[1].z));
    uint tex_opts = floatBitsToUint(batch[1].w);
    float tex_layer =  float(tex_opts & uint(0x0000FFFF));
    float tex_select = float(tex_opts & uint(0xFFFF0000));
    vec4 tex_frame = batch[2];
    vec4 rotation = batch[3];
    vec3 vert = pos + quat_apply(rotation, vec3(verts[gl_VertexID % 6].xy * scale.xy, 0));
    f_tex_coord = vec4(
        mix(tex_frame.x, tex_frame.z, verts[gl_VertexID % 6].z),
        mix(tex_frame.y, tex_frame.w, verts[gl_VertexID % 6].w),
        tex_layer, tex_select);
    gl_Position = pg_matrix_mvp * vec4(vert.xyz, 1);
}


