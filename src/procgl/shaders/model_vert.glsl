#version 330

uniform mat4 pg_matrix_mvp;

#define sprites pg_texture_0
uniform samplerBuffer sprites;
uniform int sprite_offset;

in vec3 v_position;
in vec2 v_tex_coord;

out vec3 f_tex_coord;
out vec4 f_color_mod;
out vec4 f_color_add;

void main()
{
    f_color_mod = vec4(1, 1, 1, 1);
    f_color_add = vec4(0, 0, 0, 0);
    f_tex_coord = vec3(v_tex_coord.xy, 0);
    gl_Position = pg_matrix_mvp * vec4(v_position.xyz, 1);
}



