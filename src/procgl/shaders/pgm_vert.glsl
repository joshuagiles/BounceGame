#version 330

uniform mat4 pg_matrix_mvp;

in vec3 v_position;
in vec3 v_tex_coord;

out vec3 f_tex_coord;

void main()
{
    f_tex_coord = v_tex_coord;
    gl_Position = pg_matrix_mvp * vec4(v_position.xyz, 1);
}
