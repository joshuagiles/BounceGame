
const struct tex_format {
    int channels;
    enum pg_data_type pgtype;
    GLenum iformat, pixformat, type;
    size_t size;
} gl_tex_formats[PG_NUM_DATA_TYPES];


struct pg_texture_opts {
    vec4 border;
    GLint wrap_x, wrap_y;
    GLint filter_min, filter_mag;
    GLint swizzle[4];
};

#define PG_TEXTURE_OPTS(...) \
    (&(struct pg_texture_opts){ \
        .border = {}, \
        .wrap_x = GL_REPEAT, .wrap_y = GL_REPEAT, \
        .filter_min = GL_LINEAR, .filter_mag = GL_LINEAR, \
        .swizzle = { GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA }, \
        __VA_ARGS__ })

struct pg_texture {
    /*  Base memory allocation. Texture data first, then atlas data.    */
    void* data;
    /*  Texture atlas info; This is a pointer to memory immediately
        following the pixel data, with one atlas per texture layer */
    struct pg_texture_layer {
        /*  Layer size in UV and pixel coords   */
        vec2 uv, px;
    }* layer_info;
    /*  Maximum dimensions among the tex layers managed by this pg_texture.
        May not be the actual size of an individual layer, but guaranteed
        to be >= that size. */
    int w, h;
    /*  Size of individual texture in the full allocation   */
    size_t stride;
    /*  Number of textures in the pixel data    */
    int layers;
    /*  Number of color channels    */
    int channels;
    /*  Base data type of a color channel in the texture    */
    enum pg_data_type type;
    size_t pixel_stride;
    /*  GL texture info     */
    enum { PG_TEXTURE_IMAGE, PG_TEXTURE_DEPTH, PG_TEXTURE_DEPTH_STENCIL } usage;
    GLuint handle;
    struct pg_texture_opts opts;
};

typedef struct {
    vec2 frame[2];
    int layer;
} pg_tex_frame_t;

/*  Load a texture from a file, to a 4-channel 32bpp RGBA texture   */
void pg_texture_init_from_file(struct pg_texture* tex, const char* file,
                               struct pg_texture_opts* opts);
void pg_texture_from_files(struct pg_texture* tex, int num_files,
                           const char** files, struct pg_texture_opts* opts);
/*  Initialize an empty texture */
void pg_texture_init(struct pg_texture* tex, enum pg_data_type type,
                     int w, int h, int layers, struct pg_texture_opts* opts);
void pg_texture_init_depth(struct pg_texture* tex, int w, int h, int layers,
                           struct pg_texture_opts* opts);
void pg_texture_init_depth_stencil(struct pg_texture* tex, int w, int h, int layers,
                                   struct pg_texture_opts* opts);
void pg_texture_init_buffer(struct pg_texture* tex, enum pg_data_type type,
                            uint32_t len);
/*  Free a texture initialized with above functions */
void pg_texture_deinit(struct pg_texture* tex);

/*  Bind texture to texture unit GL_TEXTURE0+slot   */
void pg_texture_bind(struct pg_texture* tex, int slot);
/*  Upload texture data to the GPU  */
void pg_texture_buffer(struct pg_texture* tex);
/*  Set GL texture parameters (uploaded with next call to pg_texture_buffer) */
void pg_texture_options(struct pg_texture* tex, struct pg_texture_opts* opts);
void pg_texture_buffer_opts(struct pg_texture_opts* opts);

/********************************************************/
/*  Texture frame (sub-rectangles) handling utilities   */
/********************************************************/
static inline pg_tex_frame_t pg_tex_frame(vec2 f0, vec2 f1, int layer) {
    return (pg_tex_frame_t){ .frame = { f0, f1 }, .layer = layer };
}
/*  Coordinate conversion functions */
pg_tex_frame_t pg_tex_frame_px_layer(struct pg_texture* tex, int layer);
pg_tex_frame_t pg_tex_frame_px_to_uv_raw(struct pg_texture* tex, pg_tex_frame_t* in);
pg_tex_frame_t pg_tex_frame_px_to_uv_pad(struct pg_texture* tex, pg_tex_frame_t* in);
pg_tex_frame_t pg_tex_frame_uv_raw_to_px(struct pg_texture* tex, pg_tex_frame_t* in);
pg_tex_frame_t pg_tex_frame_uv_pad_to_px(struct pg_texture* tex, pg_tex_frame_t* in);
/*  Fix UV frame for a texture layer to correct/uncorrect for padding   */
pg_tex_frame_t pg_tex_frame_uv_pad(struct pg_texture* tex, pg_tex_frame_t* in);
pg_tex_frame_t pg_tex_frame_uv_unpad(struct pg_texture* tex, pg_tex_frame_t* in);
/*  Flip a frame on given axes  */
pg_tex_frame_t pg_tex_frame_flip(pg_tex_frame_t* in, int x, int y);
/*  Rotate a frame in quarter-turn increments   */
pg_tex_frame_t pg_tex_frame_turn(pg_tex_frame_t* in, int turns);
/*  Intuitively transform a texframe: (x,y) + offset, (w,h) * scale */
pg_tex_frame_t pg_tex_frame_tx(pg_tex_frame_t* in, vec2 const offset, vec2 const scale);

/*  Write a raw chunk of pixel data to a texture (make sure the data type matches!) */
void pg_texture_write_pixels(struct pg_texture* tex, void* data,
                             int layer, int x, int y, int w, int h);
