#include "procgl.h"

static SDL_GLContext pg_context;
static SDL_Window* pg_window;
int screen_w, screen_h;
int render_w, render_h;

static SDL_GameController* pg_gamepad = NULL;
static SDL_JoystickID gamepad_id = -1;
static uint8_t gamepad_state[32] = {};
static uint8_t gamepad_changes[16] = {};
static uint8_t gamepad_changed = 0;
static vec2 gamepad_stick[2] = {};
static float gamepad_trigger[2] = {};
float pg_stick_dead_zone, pg_stick_threshold;
float pg_trigger_dead_zone, pg_trigger_threshold;

static uint8_t ctrl_state[256] = {};
static uint8_t ctrl_changes[16] = {};
static uint8_t ctrl_changed = 0;
static vec2 mouse_pos = {};
static vec2 mouse_motion = {};
static int mouse_relative = 0;
static int text_mode = 0;
static char text_input[16];
static char text_len;
static int user_exit = 0;

int pg_init(int w, int h, int fullscreen, const char* window_title)
{
    SDL_Init(SDL_INIT_EVERYTHING);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                        SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    SDL_GL_SetSwapInterval(0);
    SDL_DisplayMode display;
    SDL_GetDesktopDisplayMode(0, &display);
    screen_w = fullscreen ? display.w : w;
    screen_h = fullscreen ? display.h : h;
    render_w = w;
    render_h = h;
    user_exit = 0;
    pg_window = SDL_CreateWindow(window_title,
                                 SDL_WINDOWPOS_UNDEFINED,
                                 SDL_WINDOWPOS_UNDEFINED,
                                 screen_w, screen_h, SDL_WINDOW_OPENGL |
                                 (fullscreen ? SDL_WINDOW_FULLSCREEN : 0));
    pg_context = SDL_GL_CreateContext(pg_window);
    SDL_GL_SetSwapInterval(1);
    glewExperimental = GL_TRUE;
    glewInit();
    glGetError();
    /*  Init audio system   */
    pg_audio_init();
    /*  Load gamepad mappings   */
    SDL_GameControllerAddMappingsFromFile("gamecontrollerdb.txt");
    return 1;
}

void pg_deinit(void)
{
    pg_audio_init();
    SDL_GL_DeleteContext(pg_context);
    SDL_DestroyWindow(pg_window);
    SDL_Quit();
}

/*************************/
/*  Window management    */
/*************************/
void pg_window_resize(int w, int h, int fullscreen)
{
    SDL_DisplayMode display;
    SDL_GetDesktopDisplayMode(0, &display);
    screen_w = fullscreen ? display.w : w;
    screen_h = fullscreen ? display.h : h;
    render_w = w;
    render_h = h;
    SDL_SetWindowSize(pg_window, screen_w, screen_h);
    SDL_SetWindowFullscreen(pg_window, fullscreen ? SDL_WINDOW_FULLSCREEN : 0);
}

vec2 pg_screen_size(void)
{
    return vec2(screen_w, screen_h);
}

vec2 pg_window_size(void)
{
    return vec2(render_w, render_h);
}

float pg_window_aspect(void)
{
    return (float)render_w / render_h;
}

void pg_window_swap(void)
{
    SDL_GL_SwapWindow(pg_window);
}

void pg_window_dst(void)
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, screen_w, screen_h);
}


/****************************/
/*  Performance testing     */
/****************************/
double pg_time(void)
{
    return SDL_GetTicks() * 0.001;
}

uint64_t pg_perf_time(void)
{
    return SDL_GetPerformanceCounter();
}

double pg_perf_time_diff(uint64_t start, uint64_t end)
{
    return (double)(end - start) / (double)SDL_GetPerformanceFrequency();
}

static double last_time = 0;
static double framerate = 0;
static double framerate_last_update = 0;
static double framerate_update_interval = 0.5;
void pg_calc_framerate(double new_time)
{
    if(new_time >= framerate_last_update + framerate_update_interval) {
        framerate_last_update = new_time;
        framerate += 1.0f / (new_time - last_time);
        framerate /= 2;
    }
    last_time = new_time;
}

float pg_framerate(void)
{
    return framerate;
}

/************************************************/
/*  Standardized direction vectors and things   */
/************************************************/
enum pg_direction pg_get_direction(vec3 v)
{
    if(vec3_is_zero(v)) return PG_NO_DIRECTION;
    vec3 v_abs = vec3_abs(v);
    int max_axis = (v_abs.x > v_abs.y ?
                    (v_abs.x > v_abs.z ? 0 : 2) :
                    (v_abs.y > v_abs.z ? 1 : 2));
    switch(max_axis) {
    case 0: return v.x < 0 ? PG_X_NEG : PG_X_POS; // left : right
    case 1: return v.y < 0 ? PG_Y_NEG : PG_Y_POS; // front : back
    case 2: return v.z < 0 ? PG_Z_NEG : PG_Z_POS; // down : up
    default: return PG_NO_DIRECTION;
    }
}
