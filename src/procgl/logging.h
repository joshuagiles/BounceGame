enum pg_log_type {
    PG_LOG_CONTD,
    PG_LOG_DEBUG,
    PG_LOG_INFO,
    PG_LOG_WARNING,
    PG_LOG_ERROR,
    PG_LOG_ERROR_FATAL,
};

/****************************/
/*  Standardized logging    */
/****************************/
void pg_log_open(const char* log_filename);
void pg_log_close(void);
void pg_log_level(enum pg_log_type level);

#define pg_log(TYPE, MSG, ...) \
    pg_log_(TYPE, MSG, __LINE__, __FILE__, ## __VA_ARGS__)
void pg_log_(enum pg_log_type type, const char* fmt,
            int src_line, const char* src_filename, ...);

