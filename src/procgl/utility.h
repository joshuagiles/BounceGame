/*  Basically random macros     */
#define BREAKPOINT  raise(SIGINT)

#define RANDI(r)    (rand() % (r))
#define RANDF(r)    ((float)rand() / RAND_MAX * (r))
#define RANDF2(r)   (((float)rand() / RAND_MAX * 2 - 1) * (r))

#define VEC4_TO_UINT(v) (uint32_t)( \
            (((uint32_t)(LM_SATURATE((v).x) * 255) & 0xFF) << 24) | \
            (((uint32_t)(LM_SATURATE((v).y) * 255) & 0xFF) << 16) | \
            (((uint32_t)(LM_SATURATE((v).z) * 255) & 0xFF) << 8) | \
            (((uint32_t)(LM_SATURATE((v).w) * 255) & 0xFF) << 0) )
