struct pg_game_state {
    unsigned ticks;     /*  Ticks done                                  */
    unsigned tick_rate; /*  Ticks per second                            */
    unsigned tick_max;  /*  Max ticks per update                        */
    double tick_secs;   /*  The time (in seconds) each tick is allotted */
    double time_secs;   /*  Time in seconds (fixed step)                */
    double time_ticks_f;/*  Time in ticks (variable step)               */
    double time_secs_f; /*  Time in seconds (variable step)             */
    double tick_over;   /*  Time since last tick (fraction of a tick)   */
    double secs_over;   /*  Time since last tick (seconds)              */
    int running;        /*  Still running?                              */
    /*  Game interface  */
    void* data;
    void (*tick)(struct pg_game_state*);
    void (*draw)(struct pg_game_state*);
    void (*deinit)(void*);
};

void pg_game_state_init(struct pg_game_state* state, float start_time,
                        unsigned tick_rate, unsigned tick_max);
void pg_game_state_deinit(struct pg_game_state* state);
void pg_game_state_update(struct pg_game_state* state, float new_time);
void pg_game_state_draw(struct pg_game_state* state);
