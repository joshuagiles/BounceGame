#include "procgl.h"

void pg_vertex_buffer_init(struct pg_vertex_buffer* vbuf, int num_attribs,
                   const struct pg_vertex_attrib* attribs)
{
    vbuf->vertex_stride = 0;
    uint32_t vertex_offset = 0;
    int i;
    for(i = 0; i < num_attribs; ++i) {
        vbuf->attribs[i] = attribs[i];
        vbuf->attribs[i].offset = vertex_offset;
        vertex_offset += attribs[i].size;
        vbuf->vertex_stride += attribs[i].size;
    }
    vbuf->num_attribs = num_attribs;
    vbuf->vertex_data = NULL;
    vbuf->vertex_len = 0;
    vbuf->vertex_cap = 0;
    HTABLE_INIT(vbuf->vao_table, 4);
    ARR_INIT(vbuf->faces);
    ARR_INIT(vbuf->vertex_info);
    glGenBuffers(1, &vbuf->vbo);
    glGenBuffers(1, &vbuf->ebo);
}

void pg_vertex_buffer_deinit(struct pg_vertex_buffer* vbuf)
{
    free(vbuf->vertex_data);
    glDeleteBuffers(1, &vbuf->vbo);
    glDeleteBuffers(1, &vbuf->ebo);
    GLuint* vao;
    HTABLE_ITER vao_iter;
    HTABLE_ITER_BEGIN(vbuf->vao_table, vao_iter);
    while(!HTABLE_ITER_END(vbuf->vao_table, vao_iter)) {
        HTABLE_ITER_NEXT_PTR(vbuf->vao_table, vao_iter, vao);
        glDeleteVertexArrays(1, vao);
    }
    HTABLE_DEINIT(vbuf->vao_table);
}

void pg_vertex_buffer_spec(struct pg_vertex_buffer* vbuf, struct pg_shader* shader)
{
    GLuint* check_vao;
    HTABLE_GET(vbuf->vao_table, shader->name, check_vao);
    if(check_vao) return;
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbuf->vbo);
    int matched_attribs = 0;
    int i, j;
    for(i = 0; i < vbuf->num_attribs; ++i) {
        for(j = 0; j < shader->num_attribs; ++j) {
            struct pg_vertex_attrib* m_attr = &vbuf->attribs[i];
            struct pg_shader_attrib* sh_attr = &shader->attribs[j];
            if(strncmp(m_attr->name, sh_attr->name, 32) == 0) {
                ++matched_attribs;
                if(m_attr->as_integer) {
                    glVertexAttribIPointer(sh_attr->index,
                                           m_attr->elements,
                                           m_attr->type,
                                           vbuf->vertex_stride,
                                           (void*)m_attr->offset);
                } else {
                    glVertexAttribPointer(sh_attr->index,
                                          m_attr->elements,
                                          m_attr->type,
                                          m_attr->normalized,
                                          vbuf->vertex_stride,
                                          (void*)m_attr->offset);
                }
                glEnableVertexAttribArray(sh_attr->index);
            }
        }
    }
    glBindVertexArray(0);
    HTABLE_SET(vbuf->vao_table, shader->name, vao);
}

void pg_vertex_buffer_begin(struct pg_vertex_buffer* vbuf, struct pg_shader* shader)
{
    GLuint* check_vao = NULL;
    HTABLE_GET(vbuf->vao_table, shader->name, check_vao);
    if(!check_vao) {
        glBindVertexArray(0);
        printf("Model not specified for use with shader %s\n", shader->name);
        return;
    }
    glBindVertexArray(*check_vao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbuf->ebo);
}

void pg_vertex_buffer_buffer(struct pg_vertex_buffer* vbuf)
{
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vbuf->vbo);
    glBufferData(GL_ARRAY_BUFFER, vbuf->vertex_len * vbuf->vertex_stride,
                 vbuf->vertex_data, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbuf->ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, vbuf->faces.len * sizeof(struct pg_vertex_buffer_face),
                 vbuf->faces.data, GL_STATIC_DRAW);
}

void pg_vertex_buffer_reserve_verts(struct pg_vertex_buffer* vbuf, uint32_t n)
{
    if(n <= vbuf->vertex_cap) return;
    vbuf->vertex_data = realloc(vbuf->vertex_data, n * vbuf->vertex_stride);
    memset(vbuf->vertex_data + (vbuf->vertex_cap * vbuf->vertex_stride), 0,
           (n - vbuf->vertex_cap) * vbuf->vertex_stride);
    vbuf->vertex_cap = n;
}

void pg_vertex_buffer_reserve_faces(struct pg_vertex_buffer* vbuf, uint32_t n)
{
    ARR_RESERVE(vbuf->faces, n);
}

uint32_t pg_vertex_buffer_push_vertex(struct pg_vertex_buffer* vbuf, void* vertex)
{
    pg_vertex_buffer_reserve_verts(vbuf, vbuf->vertex_cap + 1);
    memcpy(vbuf->vertex_data + (vbuf->vertex_len * vbuf->vertex_stride),
           vertex, vbuf->vertex_stride);
    ARR_PUSH(vbuf->vertex_info, (struct pg_vertex_face_info){});
    return vbuf->vertex_len++;
}

void pg_vertex_buffer_set_vertex(struct pg_vertex_buffer* vbuf, int i, void* vertex)
{
    if(i >= vbuf->vertex_len) return;
    memcpy(vbuf->vertex_data + (i * vbuf->vertex_stride),
           vertex, vbuf->vertex_stride);
}

void pg_vertex_buffer_add_face(struct pg_vertex_buffer* vbuf, uint32_t v0, uint32_t v1, uint32_t v2)
{
    uint32_t face_idx = vbuf->faces.len;
    if(v0 >= vbuf->vertex_len || v1 >= vbuf->vertex_len
    || v2 >= vbuf->vertex_len) return;
    ARR_PUSH(vbuf->faces, (struct pg_vertex_buffer_face){{ v0, v1, v2 }});
}

void pg_vertex_buffer_deduplicate(struct pg_vertex_buffer* vbuf,
        int (*vert_cmp)(const void*, const void*),
        void (*vert_merge)(void*, const void*))
{
    uint32_t v0, v1, k;
    struct pg_vertex_buffer_face* face;
    for(v0 = 0; v0 < vbuf->vertex_len - 1; ++v0) for(v1 = v0+1; v1 < vbuf->vertex_len; ++v1) {
        void* v0_ptr = vbuf->vertex_data + vbuf->vertex_stride * v0;
        void* v1_ptr = vbuf->vertex_data + vbuf->vertex_stride * v1;
        int v_eq = vert_cmp(v0_ptr, v1_ptr);
        if(!v_eq) continue;
        if(vert_merge) vert_merge(v0_ptr, v1_ptr);
        uint32_t v_last = vbuf->vertex_len - 1;
        void* v_last_ptr = vbuf->vertex_data + vbuf->vertex_stride * v_last;
        if(v1 != v_last) {
            memcpy(v1_ptr, v_last_ptr, vbuf->vertex_stride);
            --vbuf->vertex_len;
        }
        ARR_FOREACH_PTR(vbuf->faces, face, k) {
            if(face->v[0] == v1) face->v[0] = v0;
            else if(face->v[0] == v_last) face->v[0] = v1;
            if(face->v[1] == v1) face->v[1] = v0;
            else if(face->v[1] == v_last) face->v[1] = v1;
            if(face->v[2] == v1) face->v[2] = v0;
            else if(face->v[2] == v_last) face->v[2] = v1;
        }
    }
}

void pg_vertex_buffer_clear(struct pg_vertex_buffer* vbuf)
{
    vbuf->vertex_len = 0;
    ARR_TRUNCATE(vbuf->faces, 0);
    ARR_TRUNCATE(vbuf->vertex_info, 0);
}
