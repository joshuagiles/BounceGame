#include "procgl.h"

void pg_armature_load(struct pg_armature* rig, const char* filename)
{
    FILE* file = fopen(filename, "r");
    if(!file) {
        printf("procgame ERROR: Failed to load rig file: %s\n", filename);
        return;
    }
    HTABLE_INIT(rig->bone_table, 8);
    HTABLE_INIT(rig->sequence_table, 8);
    HTABLE_INIT(rig->pose_table, 8);
    rig->n_seqs = 0;
    /*  Read rig data   */
    char parent_names[PG_ARMATURE_MAX_BONES][32];
    char name[32];
    int i, j;
    int n_bones, n_poses;
    fscanf(file, " RIG: bones: %d , poses: %d", &n_bones, &n_poses);
    rig->n_bones = n_bones;
    rig->n_poses = n_poses;
    for(i = 0; i < n_bones; ++i) {
        fscanf(file, " bone %*d: %32s ", rig->bones[i].name);
        HTABLE_SET(rig->bone_table, rig->bones[i].name, i);
        float bone_len;
        vec3 pos;
        quat orient;
        fscanf(file, " parent %32s", parent_names[i]);
        fscanf(file, " pos ( %f , %f , %f ) len ( %f )", &pos.x, &pos.y, &pos.z, &bone_len);
        fscanf(file, " orient ( %f , %f , %f, %f )", &orient.x, &orient.y, &orient.z, &orient.w);
        rig->bones[i].local_origin = pos;
        rig->bones[i].local_space[0] = quat_mul_vec3(orient, vec3_X());
        rig->bones[i].local_space[1] = quat_mul_vec3(orient, vec3_Y());
        rig->bones[i].local_space[2] = quat_mul_vec3(orient, vec3_Z());
        rig->bones[i].local_quat = orient;
        rig->bones[i].len = bone_len;
    }
    /*  Get all the parent indices from the names, and build children arrays    */
    for(i = 0; i < n_bones; ++i) {
        if(parent_names[i][0]) {
            int parent_idx = -1;
            HTABLE_GET_V(rig->bone_table, parent_names[i], parent_idx);
            rig->bones[i].parent = parent_idx;
            if(parent_idx == -1) continue;
            struct pg_armature_bone* parent_bone = &rig->bones[parent_idx];
            parent_bone->children[parent_bone->n_children++] = i;
        }
    }
    /*  Read the pose list  */
    for(i = 0; i < n_poses; ++i) {
        int pose_idx;
        fscanf(file, " pose %d: %32s", &pose_idx, name);
        HTABLE_SET(rig->pose_table, name, pose_idx);
        /*  If the name looks like "SomeName.N", then add it to a sequence
            called "SomeName", creating a new one if it doesn't exist   */
        char* name_dot;
        if((name_dot = strchr(name, '.'))) {
            int* seq_idx;
            int len = name_dot - name;
            HTABLE_NGET(rig->sequence_table, name, len, seq_idx);
            if(!seq_idx) {
                HTABLE_NSET(rig->sequence_table, name, len, rig->n_seqs);
                rig->seqs[rig->n_seqs++] = (struct pg_armature_sequence){
                    .poses = { i }, .n_poses = 1 };
            } else {
                struct pg_armature_sequence* seq = &rig->seqs[*seq_idx];
                seq->poses[seq->n_poses++] = i;
            }
        }
        /*  Read the pose data  */
        for(j = 0; j < n_bones; ++j) {
            fscanf(file, " bone %*d: %*32s");
            fscanf(file, " move ( %f , %f , %f )",
                &rig->poses[i].bones[j].move.x, &rig->poses[i].bones[j].move.y,
                &rig->poses[i].bones[j].move.z );
            fscanf(file, " rot ( %f , %f , %f , %f )",
                &rig->poses[i].bones[j].rot.x, &rig->poses[i].bones[j].rot.y,
                &rig->poses[i].bones[j].rot.z, &rig->poses[i].bones[j].rot.w );
        }
    }
    fclose(file);
}

struct pg_armature_sequence* pg_armature_get_sequence(struct pg_armature* rig, char* name)
{
    int* seq_idx;
    HTABLE_GET(rig->sequence_table, name, seq_idx);
    if(!seq_idx) return &rig->seqs[0];
    return &rig->seqs[*seq_idx];
}

int pg_armature_pose_by_name(struct pg_armature* rig, char* name)
{
    int* pose_idx;
    HTABLE_GET(rig->pose_table, name, pose_idx);
    if(!pose_idx) return 0;
    return *pose_idx;
}

int pg_armature_bone_by_name(struct pg_armature* rig, char* name)
{
    int* bone_idx;
    HTABLE_GET(rig->bone_table, name, bone_idx);
    if(!bone_idx) return 0;
    return *bone_idx;
}

void pg_armature_get_lerp_pose(struct pg_armature* rig, struct pg_armature_pose* out,
                               int pose_0, int pose_1, float pose_lerp)
{
    struct pg_armature_bone_tx* b0 = rig->poses[pose_0].bones;
    struct pg_armature_bone_tx* b1 = rig->poses[pose_1].bones;
    struct pg_armature_bone_tx* b_out = out->bones;
    int i;
    for(i = 0; i < rig->n_bones; ++i, ++b0, ++b1, ++b_out) {
        b_out->move = vec3_lerp(b0->move, b1->move, pose_lerp);
        b_out->rot = quat_norm(quat_lerp(b0->rot, b1->rot, pose_lerp));
    }
    out->n_bones = rig->n_bones;
}

void pg_armature_pose_empty(struct pg_armature_pose* pose)
{
    int i;
    for(i = 0; i < PG_ARMATURE_MAX_BONES; ++i) {
        pose->bones[i] = (struct pg_armature_bone_tx){
            .rot = quat_identity(),
            .move = vec3(0,0,0),
        };
    }
}

void pg_armature_pose_lerp(struct pg_armature* rig, struct pg_armature_pose* out,
                           struct pg_armature_pose* p0, struct pg_armature_pose* p1,
                           float pose_lerp)
{
    struct pg_armature_bone_tx* b0 = p0->bones;
    struct pg_armature_bone_tx* b1 = p1->bones;
    struct pg_armature_bone_tx* b_out = out->bones;
    int i;
    for(i = 0; i < rig->n_bones; ++i, ++b0, ++b1, ++b_out) {
        b_out->move = vec3_lerp(b0->move, b1->move, pose_lerp);
        b_out->rot = quat_norm(quat_lerp(b0->rot, b1->rot, pose_lerp));
    }
    out->n_bones = rig->n_bones;
}

void pg_armature_pose_apply(struct pg_armature* rig, struct pg_armature_pose* out,
                           struct pg_armature_pose* p0, struct pg_armature_pose* p1)
{
    struct pg_armature_bone_tx* b0 = p0->bones;
    struct pg_armature_bone_tx* b1 = p1->bones;
    struct pg_armature_bone_tx* b_out = out->bones;
    int i;
    for(i = 0; i < rig->n_bones; ++i, ++b0, ++b1, ++b_out) {
        b_out->rot = quat_norm(quat_mul(b1->rot, b0->rot));
        b_out->move = vec3_add(quat_mul_vec3(b1->rot, b0->move), b1->move);
    }
    out->n_bones = rig->n_bones;
}

static int enqueue_bone_children(struct pg_armature_bone* bone, int* bq, int bq_len)
{
    int i;
    for(i = 0; i < bone->n_children; ++i)
        bq[bq_len++] = bone->children[i];
    return bq_len;
}

void pg_armature_pose_propagate(struct pg_armature* rig, struct pg_armature_pose* out,
                                struct pg_armature_pose* pose)
{
    int bq[PG_ARMATURE_MAX_BONES] = { 0 };
    int bq_len = 1;
    while(bq_len) {
        int bone_idx = bq[--bq_len];
        int parent_idx = rig->bones[bone_idx].parent;
        struct pg_armature_bone* bone = &rig->bones[bone_idx];
        struct pg_armature_bone_tx* b_out = &out->bones[bone_idx];
        struct pg_armature_bone_tx* b_pose = &pose->bones[bone_idx];
        if(parent_idx == -1) {
            b_out = b_pose;
            bq_len = enqueue_bone_children(bone, bq, bq_len);
            continue;
        }
        struct pg_armature_bone_tx* b_parent = &out->bones[parent_idx];
        struct pg_armature_bone* parent = &rig->bones[parent_idx];
        vec3 parent_vec = vec3_sub(bone->local_origin, parent->local_origin);
        vec3 parent_vec_rot = quat_mul_vec3(b_parent->rot, parent_vec);
        vec3 parent_diff = vec3_sub(parent_vec_rot, parent_vec);
        vec3 pose_move = quat_mul_vec3(b_parent->rot, b_pose->move);
        b_out->move = vec3_add(vec3_add(parent_diff, pose_move), b_parent->move);
        b_out->rot = quat_mul(b_parent->rot, b_pose->rot);
        bq_len = enqueue_bone_children(&rig->bones[bone_idx], bq, bq_len);
    }
}

void pg_armature_pose_edit_bone(struct pg_armature* rig, struct pg_armature_pose* pose,
                                int bone_idx, quat rot, vec3 move)
{
    struct pg_armature_bone_tx* b_out = &pose->bones[bone_idx];
    b_out->rot = quat_norm(quat_mul(rot, b_out->rot));
    b_out->move = vec3_add(quat_mul_vec3(rot, b_out->move), move);
}

void pg_armature_pose_get_bone(struct pg_armature* rig, struct pg_armature_pose* pose,
                               struct pg_armature_bone_tx* tx_out, int bone_idx)
{
    struct pg_armature_bone_tx* b_pose = &pose->bones[bone_idx];
    *tx_out = *b_pose;
}

void pg_armature_pose_get_attachment(struct pg_armature* rig, struct pg_armature_pose* pose,
                                  struct pg_armature_bone_tx* tx_out, int bone_idx,
                                  vec3 attach_point)
{
    struct pg_armature_bone* bone = &rig->bones[bone_idx];
    struct pg_armature_bone_tx* b_pose = &pose->bones[bone_idx];
    quat attach_quat = quat_mul(b_pose->rot, bone->local_quat);
    vec3 attach_move = quat_mul_vec3(attach_quat, attach_point);
    tx_out->move = vec3_add(vec3_add(bone->local_origin, b_pose->move), attach_move);
    tx_out->rot = attach_quat;
}

void pg_armature_bone_transform(struct pg_armature_bone_tx* tx, quat rot, vec3 move, float scale)
{
    tx->rot = quat_mul(rot, tx->rot);
    tx->move = vec3_add(move, vec3_scale(quat_mul_vec3(rot, tx->move), scale));
}
