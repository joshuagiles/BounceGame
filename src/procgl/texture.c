#include "procgl.h"
#include "ext/lodepng.h"

const struct tex_format gl_tex_formats[PG_NUM_DATA_TYPES] = {
    [PG_UBYTE] = { 1, PG_UBYTE, GL_R8, GL_RED, GL_UNSIGNED_BYTE, sizeof(uint8_t) },
    [PG_UBVEC2] = { 2, PG_UBYTE, GL_RG8, GL_RG, GL_UNSIGNED_BYTE, sizeof(uint8_t) * 2 },
    [PG_UBVEC3] = { 3, PG_UBYTE, GL_RGB8, GL_RGB, GL_UNSIGNED_BYTE, sizeof(uint8_t) * 3 },
    [PG_UBVEC4] = { 4, PG_UBYTE, GL_RGBA8, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, sizeof(uint8_t) * 4 },
    [PG_UINT] = { 1, PG_UINT, GL_R32UI, GL_RED, GL_UNSIGNED_INT, sizeof(uint32_t) },
    [PG_UVEC2] = { 2, PG_UINT, GL_RG32UI, GL_RG, GL_UNSIGNED_INT, sizeof(uint32_t) * 2 },
    [PG_UVEC3] = { 3, PG_UINT, GL_RGB32UI, GL_RGB, GL_UNSIGNED_INT, sizeof(uint32_t) * 3 },
    [PG_UVEC4] = { 4, PG_UINT, GL_RGBA32UI, GL_RGBA, GL_UNSIGNED_INT, sizeof(uint32_t) * 4 },
    [PG_INT] = { 1, PG_INT, GL_R32I, GL_RED, GL_INT, sizeof(int32_t) },
    [PG_IVEC2] = { 2, PG_INT, GL_RG32I, GL_RG, GL_INT, sizeof(int32_t) * 2 },
    [PG_IVEC3] = { 3, PG_INT, GL_RGB32I, GL_RGB, GL_INT, sizeof(int32_t) * 3 },
    [PG_IVEC4] = { 4, PG_INT, GL_RGBA32I, GL_RGBA, GL_INT, sizeof(int32_t) * 4 },
    [PG_FLOAT] = { 1, PG_FLOAT, GL_R32F, GL_RED, GL_FLOAT, sizeof(float) },
    [PG_VEC2] = { 2, PG_FLOAT, GL_RG32F, GL_RG, GL_FLOAT, sizeof(float) * 2 },
    [PG_VEC3] = { 3, PG_FLOAT, GL_RGB32F, GL_RGB, GL_FLOAT, sizeof(float) * 3 },
    [PG_VEC4] = { 4, PG_FLOAT, GL_RGBA32F, GL_RGBA, GL_FLOAT, sizeof(float) * 4  }
};

void pg_texture_init_from_file(struct pg_texture* tex, const char* file,
                               struct pg_texture_opts* opts)
{
    pg_texture_from_files(tex, 1, &file, opts);
}

void pg_texture_from_files(struct pg_texture* tex, int num_files,
                           const char** files, struct pg_texture_opts* opts)
{
    uint8_t* tex_data[num_files];
    uvec2 tex_size[num_files];
    uvec2 max_size = uvec2();
    unsigned error = 0;
    /*  First load all the texture data individually    */
    int i;
    for(i = 0; i < num_files; ++i) {
        if(files[i][0] == ':') {
            sscanf(files[i], ":%i,%i", &tex_size[i].x, &tex_size[i].y);
            tex_data[i] = NULL;
        } else {
            error = lodepng_decode32_file(&tex_data[i],
                        &tex_size[i].x, &tex_size[i].y, files[i]);
            if(error) {
                tex_data[i] = NULL;
                tex_size[i] = uvec2(0, 0);
                pg_log(PG_LOG_ERROR, "Failed to load PNG file '%s'\nLodePNG error code %u: %s",
                       files[i], error, lodepng_error_text(error));
                continue;
            }
        }
        if(tex_size[i].x > max_size.x || tex_size[i].y > max_size.y) {
            max_size = tex_size[i];
        }
    }
    /*  Now allocate a big single allocation to hold all of them    */
    size_t pixel_stride = sizeof(uint8_t) * 4;
    size_t tex_stride = max_size.x * max_size.y * pixel_stride;
    tex->data = calloc(1, (tex_stride * num_files) +
                        sizeof(struct pg_texture_layer) * num_files);
    tex->layer_info = tex->data + (tex_stride * num_files);
    /*  Copy the individual texture data into the big allocation and record
        the proportion of texture space used for each one (some textures may
        be smaller than the full size, which must be taken into account
        when calculating frames in a texture atlas  */
    for(i = 0; i < num_files; ++i) {
        if(uvec2_is_zero(tex_size[i])) continue;
        struct pg_texture_layer* dst_layer = tex->layer_info + i;
        dst_layer->uv = vec2( (float)tex_size[i].x / (float)max_size.x,
                                    (float)tex_size[i].y / (float)max_size.y );
        dst_layer->px = vec2(vec_cast2(tex_size[i]));
        if(!tex_data[i]) continue;
        uint8_t* dst_data = tex->data + i * tex_stride;
        int x, y;
        for(x = 0; x < tex_size[i].x; ++x) for(y = 0; y < tex_size[i].y; ++y) {
            ubvec4* src_pix = (ubvec4*)(tex_data[i] + (x * 4) + (y * tex_size[i].x * 4));
            ubvec4* dst_pix = (ubvec4*)(dst_data + (x * 4) + (y * max_size.x * 4));
            *dst_pix = ubvec4(VEC_WZYX(*src_pix));
        }
        free(tex_data[i]);
    }
    /*  Store all the bookkeeping stuff */
    tex->type = PG_UBYTE;
    tex->stride = tex_stride;
    tex->pixel_stride = pixel_stride;
    tex->usage = PG_TEXTURE_IMAGE;
    tex->channels = 4;
    tex->layers = num_files;
    tex->w = max_size.x;
    tex->h = max_size.y;
    if(!opts) tex->opts = *PG_TEXTURE_OPTS();
    else tex->opts = *opts;
    glGenTextures(1, &tex->handle);
    pg_texture_buffer(tex);
}

void pg_texture_init(struct pg_texture* tex, enum pg_data_type type,
                     int w, int h, int layers, struct pg_texture_opts* opts)
{
    if(type < PG_UBYTE || type >= PG_MATRIX) {
        *tex = (struct pg_texture){};
        return;
    }
    const struct tex_format* fmt = &gl_tex_formats[type];
    tex->type = fmt->pgtype;
    tex->usage = PG_TEXTURE_IMAGE;
    tex->channels = fmt->channels;
    tex->stride = fmt->size * w * h;
    tex->pixel_stride = fmt->size;
    tex->data = calloc(1, (tex->stride * layers) +
                       (sizeof(struct pg_texture_layer) * layers));
    tex->layer_info = (struct pg_texture_layer*)(tex->data + (tex->stride * layers));
    tex->layers = layers;
    tex->w = w;
    tex->h = h;
    if(!opts) tex->opts = *PG_TEXTURE_OPTS();
    else tex->opts = *opts;
    glGenTextures(1, &tex->handle);
    pg_texture_buffer(tex);
}

void pg_texture_init_depth(struct pg_texture* tex, int w, int h, int layers,
                           struct pg_texture_opts* opts)
{
    tex->type = PG_FLOAT;
    tex->usage = PG_TEXTURE_DEPTH;
    tex->channels = 1;
    tex->stride = sizeof(float) * w * h;
    tex->data = calloc(1, (tex->stride * layers) +
                        (sizeof(struct pg_texture_layer) * layers));
    tex->layer_info = (struct pg_texture_layer*)(tex->data + (tex->stride * layers));
    tex->layers = layers;
    tex->w = w;
    tex->h = h;
    if(!opts) tex->opts = *PG_TEXTURE_OPTS();
    else tex->opts = *opts;
    glGenTextures(1, &tex->handle);
    pg_texture_buffer(tex);
}

void pg_texture_init_depth_stencil(struct pg_texture* tex, int w, int h, int layers,
                                   struct pg_texture_opts* opts)
{
    tex->type = PG_FLOAT;
    tex->usage = PG_TEXTURE_DEPTH_STENCIL;
    tex->channels = 1;
    tex->stride = sizeof(float) * w * h;
    tex->data = calloc(1, (tex->stride * layers) +
                        (sizeof(struct pg_texture_layer) * layers));
    tex->layer_info = (struct pg_texture_layer*)(tex->data + (tex->stride * layers));
    tex->layers = layers;
    tex->w = w;
    tex->h = h;
    if(!opts) tex->opts = *PG_TEXTURE_OPTS();
    else tex->opts = *opts;
    glGenTextures(1, &tex->handle);
    pg_texture_buffer(tex);
}

void pg_texture_deinit(struct pg_texture* tex)
{
    if(!tex->data) return;
    glDeleteTextures(1, &tex->handle);
    free(tex->data);
}

void pg_texture_options(struct pg_texture* tex, struct pg_texture_opts* opts)
{
    tex->opts = *opts;
}

void pg_texture_bind(struct pg_texture* tex, int slot)
{
    if(tex->type == PG_NULL) return;
    glActiveTexture(GL_TEXTURE0 + slot);
    glBindTexture(GL_TEXTURE_2D_ARRAY, tex->handle);
}

void pg_texture_buffer_opts(struct pg_texture_opts* opts)
{
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, opts->wrap_x);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, opts->wrap_y);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, opts->filter_min);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, opts->filter_mag);
    //glTexParameteriv(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_SWIZZLE_RGBA, opts->swizzle);
    glTexParameterfv(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_BORDER_COLOR, opts->border.v);
}

void pg_texture_buffer(struct pg_texture* tex)
{
    if(tex->type == PG_NULL) return;
    glActiveTexture(GL_TEXTURE0);
    if(tex->usage == PG_TEXTURE_IMAGE) {
        enum pg_data_type fulltype = tex->type + tex->channels - 1;
        const struct tex_format* fmt = &gl_tex_formats[fulltype];
        glBindTexture(GL_TEXTURE_2D_ARRAY, tex->handle);
        pg_texture_buffer_opts(&tex->opts);
        glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, fmt->iformat, tex->w, tex->h, tex->layers, 0,
                     fmt->pixformat, fmt->type, tex->data);
        if(tex->opts.filter_min != GL_LINEAR
        && tex->opts.filter_min != GL_NEAREST) {
            glGenerateMipmap(tex->handle);
        }
    } else if(tex->usage == PG_TEXTURE_DEPTH) {
        glBindTexture(GL_TEXTURE_2D_ARRAY, tex->handle);
        pg_texture_buffer_opts(&tex->opts);
        glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_DEPTH_COMPONENT32F, tex->w, tex->h,
                     tex->layers, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    } else if(tex->usage == PG_TEXTURE_DEPTH_STENCIL) {
        glBindTexture(GL_TEXTURE_2D_ARRAY, tex->handle);
        pg_texture_buffer_opts(&tex->opts);
        glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_DEPTH24_STENCIL8, tex->w, tex->h,
                     tex->layers, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
    }
}

/*  Texture frame (sub-rectangles) handling utilities   */
pg_tex_frame_t pg_tex_frame_px_to_uv_raw(struct pg_texture* tex, pg_tex_frame_t* in)
{
    vec2 px = vec2(tex->w, tex->h);
    return pg_tex_frame(
        vec2_div(in->frame[0], px), vec2_div(in->frame[1], px),
        in->layer);
}

pg_tex_frame_t pg_tex_frame_px_to_uv_pad(struct pg_texture* tex, pg_tex_frame_t* in)
{
    if(in->layer >= tex->layers) return *in;
    struct pg_texture_layer* layer = tex->layer_info + in->layer;
    return pg_tex_frame(
        vec2_div(in->frame[0], layer->px), vec2_div(in->frame[1], layer->px),
        in->layer);
}

pg_tex_frame_t pg_tex_frame_uv_raw_to_px(struct pg_texture* tex, pg_tex_frame_t* in)
{
    vec2 px = vec2(tex->w, tex->h);
    return pg_tex_frame(
        vec2_mul(in->frame[0], px), vec2_mul(in->frame[1], px),
        in->layer);
}

pg_tex_frame_t pg_tex_frame_uv_pad_to_px(struct pg_texture* tex, pg_tex_frame_t* in)
{
    if(in->layer >= tex->layers) return *in;
    struct pg_texture_layer* layer = tex->layer_info + in->layer;
    return pg_tex_frame(
        vec2_mul(in->frame[0], layer->px), vec2_mul(in->frame[1], layer->px),
        in->layer);
}


/*  Fix UV frame for a texture layer to correct/uncorrect for padding   */
pg_tex_frame_t pg_tex_frame_uv_pad_to_raw(struct pg_texture* tex, pg_tex_frame_t* in)
{
    if(in->layer >= tex->layers) return *in;
    struct pg_texture_layer* layer = tex->layer_info + in->layer;
    return pg_tex_frame(
        vec2_mul(in->frame[0], layer->uv), vec2_mul(in->frame[1], layer->uv),
        in->layer);
}

pg_tex_frame_t pg_tex_frame_uv_raw_to_pad(struct pg_texture* tex, pg_tex_frame_t* in)
{
    if(in->layer >= tex->layers) return *in;
    struct pg_texture_layer* layer = tex->layer_info + in->layer;
    return pg_tex_frame(
        vec2_div(in->frame[0], layer->uv), vec2_div(in->frame[1], layer->uv),
        in->layer);
}

/*  Flip a frame on given axes  */
pg_tex_frame_t pg_tex_frame_flip(pg_tex_frame_t* in, int x, int y)
{
    x = !!x;
    y = !!y;
    return pg_tex_frame(
        vec2(in->frame[x].x, in->frame[y].y),
        vec2(in->frame[1-x].x, in->frame[1-y].y),
        in->layer);
}

/*  Rotate a frame in quarter-turn increments   */
pg_tex_frame_t pg_tex_frame_turn(pg_tex_frame_t* in, int turns)
{
    turns %= 4;
    turns += 1;
    if(turns == 0) {
        return *in;
    } else if(turns == 1) {
        return pg_tex_frame(
            vec2(in->frame[0].x, in->frame[1].y),
            vec2(in->frame[1].x, in->frame[0].y),
            in->layer);
    } else if(turns == 2) {
        return pg_tex_frame(
            vec2(in->frame[1].x, in->frame[1].y),
            vec2(in->frame[0].x, in->frame[0].y),
            in->layer);
    } else if(turns == 3) {
        return pg_tex_frame(
            vec2(in->frame[1].x, in->frame[0].y),
            vec2(in->frame[0].x, in->frame[1].y),
            in->layer);
    } else return *in;
}

/*  Intuitively transform a texframe: (x,y) + offset, (w,h) * scale */
pg_tex_frame_t pg_tex_frame_tx(pg_tex_frame_t* in, vec2 const offset, vec2 const scale)
{
    vec2 diff = vec2_sub(in->frame[1], in->frame[0]);
    diff = vec2_mul(diff, scale);
    return pg_tex_frame(
        vec2_add(in->frame[0], offset),
        vec2_add(vec2_add(in->frame[1], diff), offset),
        in->layer);
}

void pg_texture_write_pixels(struct pg_texture* tex, void* data,
                             int layer, int x, int y, int w, int h)
{
    struct pg_texture_layer* dst_layer = tex->layer_info + layer;
    void* dst_data = tex->data + layer * tex->stride;
    size_t pix_sz = tex->pixel_stride;
    size_t line_sz = tex->pixel_stride * tex->w;
    if(x > dst_layer->px.x || y > dst_layer->px.y) return;
    int x_dst_start = LM_MAX(0, x);
    int x_src_start = -LM_MIN(0, x);
    int x_len = LM_MIN(dst_layer->px.x - x_dst_start, w);
    int y_dst_start = LM_MAX(0, y);
    int y_src_start = -LM_MIN(0, y);
    int y_len = LM_MIN(dst_layer->px.y - y_dst_start, h);
    if(x_len <= 0 || y_len <= 0) return;
    int i;
    for(i = 0; i < y_len; ++i) {
        void* dst_line = dst_data + (x_dst_start * pix_sz) + ((y_dst_start + i) * line_sz);
        void* src_line = data + (x_src_start * pix_sz) + ((y_src_start + i) * pix_sz * w);
        memcpy(dst_line, src_line, x_len * pix_sz);
    }
}
