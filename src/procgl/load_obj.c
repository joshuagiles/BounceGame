#include "procgl.h"

struct obj_vertex { vec3 pos; vec2 tex; };

static const struct pg_vertex_attrib obj_attribs[2] = {
    { "v_position", .type = GL_FLOAT, .elements = 3, .size = sizeof(vec3) },
    { "v_tex_coord", .type = GL_FLOAT, .elements = 2, .size = sizeof(vec2) },
};

void pg_vertex_buffer_load_obj(struct pg_vertex_buffer* verts, const char* filename)
{
    pg_vertex_buffer_init(verts, 2, obj_attribs);
    ARR_T(vec3) v_pos = {};
    ARR_T(vec2) v_tex = {};
    FILE* obj_file = fopen(filename, "r");
    if(!obj_file) {
        printf("procgame ERROR:\n"
               "    Failed to load model: %s\n", filename);
        return;
    }
    /*  First pass: Just count the number of each thing there is    */
    int n_pos = 0, n_tex = 0, n_face = 0;
    char line[64];
    while(fgets(line, 64, obj_file)) {
        if(strncmp(line, "v ", 2) == 0) ++n_pos;
        else if(strncmp(line, "vt ", 3) == 0) ++n_tex;
        else if(strncmp(line, "f ", 2) == 0) ++n_face;
    }
    if(n_pos == 0 || n_tex == 0 || n_face == 0) {
        printf("procgame ERROR:\n"
               "    Model file missing vertex, texture, or face data: %s\n",
               filename);
        fclose(obj_file);
        return;
    }
    printf("Model file %s\n"
           "    Vertex positions: %d\n"
           "    Vertex UVs: %d\n"
           "    Face definitions: %d\n", filename, n_pos, n_tex, n_face);
    /*  Second pass: Actually load model data   */
    fseek(obj_file, 0, SEEK_SET);
    pg_vertex_buffer_reserve_verts(verts, n_face * 3);
    ARR_RESERVE_CLEAR(v_pos, n_pos);
    ARR_RESERVE_CLEAR(v_tex, n_tex);
    while(fgets(line, 64, obj_file)) {
        if(strncmp(line, "v ", 2) == 0) {
            vec3 pos;
            sscanf(line, "v %f %f %f", &pos.x, &pos.y, &pos.z);
            ARR_PUSH(v_pos, pos);
        } else if(strncmp(line, "vt ", 3) == 0) {
            vec2 tex;
            sscanf(line, "vt %f %f", &tex.x, &tex.y);
            ARR_PUSH(v_tex, tex);
        } else if(strncmp(line, "f ", 2) == 0) {
            int pos[3], tex[3];
            sscanf(line, "f %d/%d %d/%d %d/%d",
                    &pos[0], &tex[0], &pos[1], &tex[1], &pos[2], &tex[2]);
            struct obj_vertex new_vert[3] = {
                { .pos = v_pos.data[pos[0] - 1],
                  .tex = v_tex.data[tex[0] - 1] },
                { .pos = v_pos.data[pos[1] - 1],
                  .tex = v_tex.data[tex[1] - 1] },
                { .pos = v_pos.data[pos[2] - 1],
                  .tex = v_tex.data[tex[2] - 1] } };
            pg_vertex_buffer_push_vertex(verts, &new_vert[0]);
            pg_vertex_buffer_push_vertex(verts, &new_vert[1]);
            pg_vertex_buffer_push_vertex(verts, &new_vert[2]);
        }
    }
    printf("Model now has %zu verts\n", verts->vertex_len);
    fclose(obj_file);
    pg_vertex_buffer_buffer(verts);
    return;
}

