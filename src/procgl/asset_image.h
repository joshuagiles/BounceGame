/*  Image Assets

    A way to manage images in a logical manner. This allows full image asset
    configuration at run-time using JSON. A thorough demonstration of the
    asset format is in the files `asset_image.json` and `asset_imgset.json`.
  
    Image assets have 'frames', which are named sub-rectangles of the image.
    They also have 'sequences', which are named sets of frames, with associated
    duration information. These form a database for producing animated image
    frame slide-shows out of data which can be reconfigured at runtime.

    Because string lookups for every frame would be inefficient, it is
    recommended to cache the values returned from the lookup functions whenever
    possible. For animations, a pg_asset_image_stepper structure is provided as
    a small interface to manage individual animated objects without requiring
    any lookups after initialization.

    Imgsets allow multiple images to be treated as one image with many layers.
    This means images smaller than the rest may end up in a layer with padding
    (generally 0x00000000) to fill the unused space. That image will also have
    all its frames' uv coords fixed to account for the padding, automatically.
    This is the difference between the `uv_pad` and `uv_raw` coordinate spaces;
    `uv_pad` refers to the UV space of the single image asset, and
    `uv_raw` refers to the UV space of the whole texture layer.
    So those frames were transformed from `uv_pad` space to `uv_raw` space.

    There are also grid cells, which is just a user-defined 2D factor for
    dividing an image so grid cells align to integer coordinates. It can be
    defined in terms of either `px` or `uv_pad` coordinates.

    This means images have four different coordinate systems to manage:
    * Pixel coordinates (`px`)
    * UV coordinates before accounting for padding  (`uv_pad`)
        * This is the space used in the JSON configuration files as "uv"
    * UV coordinates after accounting for padding (`uv_raw`)
    * Grid cell coordinates (`cell`)

    Here is an example of a frame's coordinates in those four coordinate spaces,
    Given an image with these dimensions:
        Image size (128,128), Texture size (256,256), Cell size (32,32)px
    `px`        (32,32) -> (64,64)
    `uv_pad`    (0.25,0.25) -> (0.5,0.5)
    `uv_raw`    (0.125,0.125) -> (0.25,0.25)
    `cell`      (1,1) -> (2,2)
*/

/*  A named sub-rectangle of the image. */
struct pg_asset_image_frame {
    char name[32];
    /*  Pixel coordinates, texture coordinates, cell coordinates [min,max]  */
    vec2 px[2], cell[2];
    /*  Raw UV texture frame    */
    pg_tex_frame_t uv_raw;
};

/*  Frame indices and start time of each frame, relative to the whole sequence  */
struct pg_asset_image_sequence {
    char name[32];
    int frame[32];
    float start[32];
    float duration[32];
    int n_frames;
    float full_duration;
};

/*  The minimal "instance" for tracking an animated object in a sequence    */
#define PG_IMAGE_STEPPER_NORESET    (1 << 0)
#define PG_IMAGE_STEPPER_SINGLE     (1 << 1)
#define PG_IMAGE_STEPPER_FLIP_X     (1 << 2)
#define PG_IMAGE_STEPPER_FLIP_Y     (1 << 3)
struct pg_asset_image_stepper {
    int img_idx;
    int seq_idx;
    int cur_step;
    uint32_t opts;
    float seq_progress;
    pg_tex_frame_t uv;
};

/*  Image assets. Just one image in a texture   */
struct pg_asset_image {
    /************************/
    /*  Configured data     */
    char cfg_filename[1024];
    char img_filename[1024];
    vec2 grid_px, grid_uv, grid_cells;
    ARR_T(struct pg_asset_image_frame) frames;
    HTABLE_T(int) frames_table;
    ARR_T(struct pg_asset_image_sequence) seqs;
    HTABLE_T(int) seqs_table;
    /************************/
    /*  Run-time data       */
    /*  img_size:   Dimensions of the pixel data
        uv_size:    UV dimensions of the pixel data within the texture (the
                        image may be smaller than texture dimensions)   */
    vec2 img_size, uv_size;
    int tex_layer;
    /*  If the asset owns the texture, then the texture pointer will be
        deinitialized and freed whenever the asset is deinitialized */
    int own_tex;
    struct pg_texture* tex;
};

/*  Imgset assets. A little wrapper that can keep images in texture layers. */
struct pg_asset_imgset {
    /*  Configured data (just a bunch of pg_asset_images, really)   */
    char cfg_filename[1024];
    ARR_T(struct pg_asset_image) imgs;
    HTABLE_T(int) imgs_table;
    /*  Run-time data   */
    int own_tex;
    struct pg_texture* tex;
};

/****************/
/*  Images      */
void pg_asset_image_load(struct pg_asset_image* asset, const char* filename,
                           struct pg_texture* tex, struct pg_texture_opts* opts);
void pg_asset_image_deinit(struct pg_asset_image* asset);
pg_tex_frame_t pg_asset_image_get_frame(struct pg_asset_image* asset, const char* frame_name);
pg_tex_frame_t pg_asset_image_get_cell(struct pg_asset_image* asset, int x, int y);
struct pg_asset_image_sequence* pg_asset_image_get_sequence(
        struct pg_asset_image* spr, const char* anim_name);

/****************/
/*  Imgsets     */
void pg_asset_imgset_load(struct pg_asset_imgset* asset, const char* filename,
                          struct pg_texture* tex, struct pg_texture_opts* opts);
void pg_asset_imgset_deinit(struct pg_asset_imgset* asset);
struct pg_asset_image* pg_asset_imgset_get_image(struct pg_asset_imgset* asset,
                                                 const char* image_name);
pg_tex_frame_t pg_asset_imgset_get_frame(struct pg_asset_imgset* asset,
                                         const char* image_name, const char* frame_name);
pg_tex_frame_t pg_asset_imgset_get_cell(struct pg_asset_imgset* asset,
                                        const char* image_name, int x, int y);
struct pg_asset_image_sequence* pg_asset_imgset_get_sequence(
        struct pg_asset_imgset* asset, const char* image_name, const char* seq_name);

/********************/
/*  Image steppers  */
/*  A "stepper" is a little tracker object for single animated items.
    It can be "stepped" through any sequence in discrete timesteps, and get
    a raw texframe from the sequence conveniently.
    Steppers that haven't been stepped (or which have, I suppose) can be cached
    and copied around later to avoid string look-ups if you want. A stepper is
    self-contained and has no dynamic allocations so copying requires no extra
    thought for memory management.  */
void pg_asset_image_set_stepper(struct pg_asset_image* asset,
                                  struct pg_asset_image_stepper* stepper,
                                  const char* seq_name, uint32_t opts);
void pg_asset_imgset_set_stepper(struct pg_asset_imgset* asset,
                                  struct pg_asset_image_stepper* stepper,
                                  const char* img_name,
                                  const char* seq_name, uint32_t opts);
void pg_asset_image_stepper_single(struct pg_asset_image* asset,
                                  struct pg_asset_image_stepper* stepper,
                                  const char* frame_name, uint32_t opts);
void pg_asset_imgset_stepper_single(struct pg_asset_imgset* asset,
                                  struct pg_asset_image_stepper* stepper,
                                  const char* img_name,
                                  const char* frame_name, uint32_t opts);
void pg_asset_image_step(struct pg_asset_image* asset,
                         struct pg_asset_image_stepper* stepper, float step);
void pg_asset_imgset_step(struct pg_asset_imgset* asset,
                          struct pg_asset_image_stepper* stepper, float step);
void pg_asset_image_stepper_set_time(struct pg_asset_image_stepper* stepper,
                                struct pg_asset_image* asset, float time);
void pg_asset_imgset_stepper_set_time(struct pg_asset_image_stepper* stepper,
                                 struct pg_asset_imgset* asset, float time);


