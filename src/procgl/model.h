/*  A built-in model format with rigging,
 *  and a renderpass interface for it.  */
/****************/
/*  Geometry    */
/****************/
struct pg_model {
    struct pg_vertex_buffer* vbuf;
    uint32_t vertex_range[2];
    uint32_t index_range[2];
    struct pg_armature* rig;
};

void pg_model_init_vertex_buffer(struct pg_vertex_buffer* verts);

void pg_model_empty(struct pg_model* model, struct pg_vertex_buffer* verts);
void pg_model_load(struct pg_model* model, struct pg_vertex_buffer* verts,
                   const char* filename, int tex_layer);

void pg_model_buffer_deduplicate(struct pg_vertex_buffer* vbuf);

/****************/
/*  Rendering   */
/****************/
/*  Static geometry drawing is a separate pass from rigged geometry */
void pg_ezpass_static_model(struct pg_renderpass* pass, struct pg_rendertarget* target,
                            struct pg_vertex_buffer* vbuf, struct pg_texture* tex);
void pg_model_draw_static(struct pg_renderpass* pass, struct pg_model* model, mat4 tx);
void pg_model_draw_static_to_vb(struct pg_vertex_buffer* vbuf_out,
                                struct pg_model* model, mat4 tx);
void pg_model_append_static(struct pg_model* dst, struct pg_model* src, mat4 tx);

/*  Rigged geometry drawing */
void pg_ezpass_rigged_model(struct pg_renderpass* pass, struct pg_rendertarget* target,
                            struct pg_vertex_buffer* vbuf, struct pg_texture* tex);
void pg_model_draw_rigged(struct pg_renderpass* pass, struct pg_model* model,
                          struct pg_armature_pose* pose, mat4 tx);

