/*  Basic rendering overview:
    A pg_renderer keeps track of shaders, global uniforms, and pg_renderpasses
    
    A pg_renderpass represents one high-level step in a render.
    Each pass has a defined set of inputs and outputs which are constant across
    the entire pass. So if a frame needs to have some models drawn with one
    pg_vertex_buffer and one pg_texture, to a given target, those are the
    inputs and outputs of the pass. If it is also needed to draw the same model
    but with a different pg_texture or to a different target, then a second
    renderpass must be used. If it is needed to draw the same vertices and the
    same texture to the same target, but with a different shader, another
    renderpass must be used then as well. A renderpass also takes as input a
    queue of *raw data* in a format the user specifies. The data is "parsed" at
    render time by the user-specified drawing function, which generates all the
    GL draw calls for that renderpass.  A renderpass may take one pg_rendertarget
    as both an input and an output.  This requires the rendertarget to have two
    attached renderbuffers, and the logic for which buffers are used as the
    input/output for each drawcall is determined by the `PG_RENDERPASS_SWAP...`
    option flags.

    This system allows virtual draw calls to be distributed among all the
    renderpasses before any GL functions will actually be called, so they can
    be added at any time and in any order, without having to modify GL state
    in every part of the user-side drawing code. That data can then be saved
    for reuse later, or sorted, analysed, culled, etc. if needed.

    At the end, when `pg_renderer_draw_frame()` is called, every renderpass in
    the list is executed, one after the other. All the renderpass options and
    required GL state are satisfied, and then the user-supplied draw function
    is called on the draw data array repeatedly until it reaches the end or
    stops advancing the data index for whatever reason.

    The last renderpass in the list will likely have the screen as its target
    (rendertarget or renderbuffer NULL always targets the screen), but this is
    not required. A renderer may just as well render solely to an off-screen
    buffer. Also, renderpasses at any point in the process may draw to textures
    not needed by later rendering stages so those intermediate data can be used
    for other purposes.
*/

/*  Renderpass options specify whole-pass rendering options, like whether
    to do depth tests/writes, blending modes, buffer swapping logic for double-
    buffered rendertargets, and other things    */
struct pg_renderpass_opts {
    /*  Option-specific data is only read of the relevant flag is set   */
    uint32_t flags;
    GLenum blend_func[2];
    GLenum depth_func;
    GLbitfield clear_buffers;
    vec4 clear_color;
    vec4 viewport;
    vec4 scissor;
};

#define PG_RENDERPASS_SWAP_BEFORE   (1 << 0)
#define PG_RENDERPASS_SWAP_PER_DRAW (1 << 1)
#define PG_RENDERPASS_SWAP_AFTER    (1 << 2)
#define PG_RENDERPASS_BUFFER_SWAP \
    ( PG_RENDERPASS_SWAP_BEFORE | PG_RENDERPASS_SWAP_PER_DRAW \
    | PG_RENDERPASS_SWAP_AFTER)
#define PG_RENDERPASS_BLENDING      (1 << 3)
#define PG_RENDERPASS_DEPTH_WRITE   (1 << 4)
#define PG_RENDERPASS_DEPTH_TEST    (1 << 5)
#define PG_RENDERPASS_CULL_FACES    (1 << 6)
#define PG_RENDERPASS_VIEWPORT      (1 << 7)
#define PG_RENDERPASS_SCISSOR       (1 << 8)

#define PG_RENDERPASS_OPTS(...) \
    (&(struct pg_renderpass_opts){ __VA_ARGS__ })

/*  Renderpass texture slots connect the various texture-storage pg structures
    into a renderpass to use as an input.  */
struct pg_renderpass_texslot {
    struct pg_texture_opts tex_opts;
    int use_opts;
    enum {
        PG_PASS_TEX_EMPTY,
        PG_PASS_TEX_IMAGE,
        PG_PASS_TEX_RENDERTARGET,
        PG_PASS_TEX_BUFFER
    } type;
    union {
        struct pg_texture* tex;
        struct pg_buffertex* buf;
        struct {
            struct pg_rendertarget* fb_tex;
            int idx;    /*  Attachment index to read from   */
        };
    };
};

/*  The draw-function is supplied externally and is the last step in the
    whole process: it reads draw data and emits actual GL calls.    */
typedef size_t (*pg_render_drawfunc_t)(
    const void*, const struct pg_shader*, const mat4*, const GLint*);

struct pg_renderpass {
    int enabled;
    /************/
    /*  INPUT   */
    /*  Shader (key in renderer's shader table)  */
    char shader[32];
    /*  Options for the renderpass as a whole (blending, depth testing, etc.)   */
    struct pg_renderpass_opts opts;
    /*  Whole-pass uniforms  */
    pg_shader_uniform_table_t uniforms;
    /*  Built-in matrices passed to shaders by default with matching names like
        pg_matrix_view pg_matrix_projection, pg_matrix_modelview, etc. */
    mat4 mats[PG_COMMON_MATRICES];
    /*  Generates projection, view, and projectionview matrices  */
    struct pg_viewer* viewer;
    /*  Eight texture slots.  Each slot can contain any kind of texture source,
        including a pg_rendertarget for post-processing, or a pg_buffertex. The
        specific requirements for which slot should have which type of texture
        is determined by each individual shader. Passed by default as uniforms
        `pg_texture[0-7]` (regardless of texture storage type). */
    struct pg_renderpass_texslot tex[8];
    /*  Vertex data     */
    struct pg_vertex_buffer* verts;
    /*  The uniforms (names and indices) which the drawfunc needs access to.
        They are set with `pg_renderpass_set_drawfunc()`, and their order in
        the array is the same as the order they were given in the format
        arguments.  */
    char draw_uniform[8][32];
    GLint draw_uniform_idx[8];
    /*  Raw data which is pushed to the renderpass ahead of the render   */
    arr_char_t drawdata;
    /*  Function pointer which is repeatedly called on the draw data array, and
        advances along the data emitting draw calls based on the data.  */
    pg_render_drawfunc_t per_draw;

    /************/
    /*  OUTPUT  */
    struct pg_rendertarget* target;
    vec2 resolution;
    float aspect_ratio;
};

/*  The top-level manager for the whole thing. Owns all loaded shaders and
    supplies global uniforms. Executes a list of renderpasses in order to
    generate each frame.    */
struct pg_renderer {
    /*  Used for shaders which don't require a vertex buffer    */
    GLuint dummy_vao;
    pg_shader_table_t shaders;
    /*  Uniforms accessible from all shader programs (may be overridden by
        renderpass uniforms)    */
    pg_shader_uniform_table_t common_uniforms;
    /*  List of renderpasses to execute in order to draw a frame.   */
    ARR_T(struct pg_renderpass*) passes;
};

/****************/
/*  Renderpass  */
/****************/
void pg_renderpass_init(struct pg_renderpass* pass, char* shader_name,
                        struct pg_renderpass_opts* opts);
void pg_renderpass_deinit(struct pg_renderpass* pass);
void pg_renderpass_enable(struct pg_renderpass* pass, int enabled);

/*  OPTIONS     */
void pg_renderpass_blending(struct pg_renderpass* pass, int use,
                            GLenum left, GLenum right);
void pg_renderpass_depth_test(struct pg_renderpass* pass, int use, GLenum func);
void pg_renderpass_depth_write(struct pg_renderpass* pass, int use);
void pg_renderpass_clear_color(struct pg_renderpass* pass, vec4 color);
void pg_renderpass_clear_bits(struct pg_renderpass* pass, GLbitfield buffers);
void pg_renderpass_viewport(struct pg_renderpass* pass, int use, struct pg_viewport* vp);
void pg_renderpass_scissor(struct pg_renderpass* pass, int use, struct pg_viewport* vp);
void pg_renderpass_swap(struct pg_renderpass* pass, uint32_t flags);

/*  INPUT   */
void pg_renderpass_uniform(struct pg_renderpass* pass, char* name,
                           enum pg_data_type type, struct pg_type* data);
void pg_renderpass_vertices(struct pg_renderpass* pass, struct pg_vertex_buffer* verts);
void pg_renderpass_texture(struct pg_renderpass* pass, int idx,
                           struct pg_texture* tex, struct pg_texture_opts* opts);
void pg_renderpass_fbtexture(struct pg_renderpass* pass, int idx,
                             struct pg_rendertarget* src, int src_idx);
void pg_renderpass_buftexture(struct pg_renderpass* pass, int idx,
                              struct pg_buffertex* tex);
void pg_renderpass_viewer(struct pg_renderpass* pass, struct pg_viewer* view);

/*  Draw data and draw function */
size_t pg_renderpass_add_drawdata(struct pg_renderpass* pass,
                                   size_t size, void* data);
void pg_renderpass_clear_drawdata(struct pg_renderpass* pass);
/*  Set the draw function and specify the uniforms it accesses (by names)   */
void pg_renderpass_set_drawfunc(struct pg_renderpass* pass,
                                pg_render_drawfunc_t drawfunc, int n_unis, ...);

/*  OUTPUT  */
void pg_renderpass_target(struct pg_renderpass* pass,
                          struct pg_rendertarget* target);

/****************/
/*  Renderer    */
/****************/
void pg_renderer_init(struct pg_renderer* rend);
void pg_renderer_deinit(struct pg_renderer* rend);
void pg_renderer_reset(struct pg_renderer* rend);
void pg_renderer_draw_frame(struct pg_renderer* rend);
void pg_renderer_vertex_spec(struct pg_renderer* rend, char* shader,
                             struct pg_vertex_buffer* verts);

/*  Make the static shaders option invisible to user code */
#ifdef PROCGL_STATIC_SHADERS
#define pg_renderer_load_shader(RENDERER, SHADER, VERT, FRAG) \
do { \
    struct pg_shader shader; \
    int load = pg_shader_load_static(&shader, SHADER, \
        VERT, VERT##_len, FRAG, FRAG##_len); \
    if(!load) printf("procgame ERROR: Failed to load shader: " SHADER "\n"); \
    else HTABLE_SET((RENDERER).shaders, SHADER, shader); \
} while(0)
#else
#define pg_renderer_load_shader(RENDERER, SHADER, VERT, FRAG) \
do { \
    struct pg_shader shader; \
    int load = pg_shader_load(&shader, SHADER, \
        SHADER_DIR #VERT ".glsl", SHADER_DIR #FRAG ".glsl"); \
    if(!load) printf("procgame ERROR: Failed to load shader: " SHADER "\n"); \
    else HTABLE_SET((RENDERER).shaders, SHADER, shader); \
} while(0)
#endif




