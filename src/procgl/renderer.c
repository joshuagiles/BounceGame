#include "procgl.h"

#ifdef PROCGL_STATIC_SHADERS
#include "shader_sources.glsl.h"
#else
#define SHADER_DIR "src/procgl/shaders/"
#endif

void pg_renderer_init(struct pg_renderer* rend)
{
    /*  Initialize memory management stuff  */
    ARR_INIT(rend->passes);
    HTABLE_INIT(rend->common_uniforms, 8);
    HTABLE_INIT(rend->shaders, 8);
    /*  Make a dummy VAO for shaders that don't need any vertex buffers */
    glGenVertexArrays(1, &rend->dummy_vao);
    /*  Load the procgame shaders   */
    pg_renderer_load_shader(*rend, "spritebatch", spritebatch_vert, spritebatch_frag);
    pg_renderer_load_shader(*rend, "boxbatch", boxbatch_vert, spritebatch_frag);
    pg_renderer_load_shader(*rend, "pgm_rigged", pgm_rigged_vert, pgm_frag);
    pg_renderer_load_shader(*rend, "pgm", pgm_vert, pgm_frag);
    pg_renderer_load_shader(*rend, "post_sine", screen_vert, post_sine);
    pg_renderer_load_shader(*rend, "post_blur", screen_vert, post_blur);
    pg_renderer_load_shader(*rend, "post_fog", screen_vert, post_fog);
    pg_renderer_load_shader(*rend, "passthru", screen_vert, post_screen);
}

void pg_renderer_deinit(struct pg_renderer* rend)
{
    HTABLE_DEINIT(rend->common_uniforms);
    ARR_DEINIT(rend->passes);
    /*  Deinit shaders  */
    struct pg_shader* shader;
    HTABLE_ITER iter;
    HTABLE_ITER_BEGIN(rend->shaders, iter);
    while(!HTABLE_ITER_END(rend->shaders, iter)) {
        HTABLE_ITER_NEXT_PTR(rend->shaders, iter, shader);
        pg_shader_deinit(shader);
    }
    HTABLE_DEINIT(rend->shaders);
    /*  Zero the renderer structure */
    *rend = (struct pg_renderer){};
}

void pg_renderer_vertex_spec(struct pg_renderer* rend, char* shader_name,
                              struct pg_vertex_buffer* verts)
{
    struct pg_shader* shader;
    HTABLE_GET(rend->shaders, shader_name, shader);
    pg_vertex_buffer_spec(verts, shader);
}

void pg_renderpass_init(struct pg_renderpass* pass, char* shader_name,
                        struct pg_renderpass_opts* opts)
{
    *pass = (struct pg_renderpass) { .enabled = 1,
        .opts = (opts ? *opts : *PG_RENDERPASS_OPTS()) };
    strncpy(pass->shader, shader_name, 31);
    int i;
    for(i = 0; i < PG_COMMON_MATRICES; ++i) pass->mats[i] = mat4_identity();
    HTABLE_INIT(pass->uniforms, 8);
}

void pg_renderpass_deinit(struct pg_renderpass* pass)
{
    HTABLE_DEINIT(pass->uniforms);
    ARR_DEINIT(pass->drawdata);
}

void pg_renderpass_clear_drawdata(struct pg_renderpass* pass)
{
    ARR_TRUNCATE_CLEAR(pass->drawdata, 0);
}

void pg_renderpass_uniform(struct pg_renderpass* pass, char* name,
                            enum pg_data_type type, struct pg_type* data)
{
    HTABLE_SET(pass->uniforms, name,
        (struct pg_shader_uniform){ .type = type, .data = *data });
}

void pg_renderpass_vertices(struct pg_renderpass* pass, struct pg_vertex_buffer* verts)
{
    pass->verts = verts;
}

void pg_renderpass_texture(struct pg_renderpass* pass, int idx,
                           struct pg_texture* tex, struct pg_texture_opts* opts)
{
    pass->tex[idx].type = PG_PASS_TEX_IMAGE;
    pass->tex[idx].tex = tex;
    if(opts) {
        pass->tex[idx].tex_opts = *opts;
        pass->tex[idx].use_opts = 1;
    } else {
        pass->tex[idx].use_opts = 0;
    }
}

void pg_renderpass_fbtexture(struct pg_renderpass* pass, int idx,
                             struct pg_rendertarget* src, int src_idx)
{
    pass->tex[idx].type = PG_PASS_TEX_RENDERTARGET;
    pass->tex[idx].fb_tex = src;
    pass->tex[idx].idx = src_idx;
}

void pg_renderpass_buftexture(struct pg_renderpass* pass, int idx,
                              struct pg_buffertex* tex)
{
    pass->tex[idx].type = PG_PASS_TEX_BUFFER;
    pass->tex[idx].buf = tex;
}

void pg_renderpass_set_drawfunc(struct pg_renderpass* pass,
                               pg_render_drawfunc_t per_draw, int n, ...)
{
    pass->per_draw = per_draw;
    va_list args;
    va_start(args, n);
    int i;
    for(i = 0; i < 8 && i < n; ++i) {
        char* u = va_arg(args, char*);
        strncpy(pass->draw_uniform[i], u, 32);
    }
    va_end(args);
}

size_t pg_renderpass_add_drawdata(struct pg_renderpass* pass, size_t size, void* data)
{
    ARR_RESERVE(pass->drawdata, pass->drawdata.len + size);
    size_t dst_offset = pass->drawdata.len;
    memcpy(pass->drawdata.data + pass->drawdata.len, data, size);
    pass->drawdata.len += size;
    return dst_offset;
}

void pg_renderer_reset(struct pg_renderer* rend)
{
    ARR_TRUNCATE_CLEAR(rend->passes, 0);
}

static inline void shader_base_mats(struct pg_shader* shader,
                                    struct pg_renderpass* pass)
{
    int i;
    for(i = 0; i < PG_COMMON_MATRICES; ++i) {
        if(shader->uni_mat[i] == -1) continue;
        glUniformMatrix4fv(shader->uni_mat[i], 1, GL_FALSE, pass->mats[i].v);
    }
}

static inline void shader_textures(struct pg_shader* shader, 
                                   struct pg_renderpass* pass)
{
    int i;
    for(i = 0; i < 8; ++i) {
        if(shader->uni_tex[i] == -1) continue;
        glActiveTexture(GL_TEXTURE0 + i);
        if(pass->tex[i].type == PG_PASS_TEX_IMAGE) {
            glBindTexture(GL_TEXTURE_2D_ARRAY, pass->tex[i].tex->handle);
            if(pass->tex[i].use_opts) pg_texture_buffer_opts(&pass->tex[i].tex_opts);
            else pg_texture_buffer_opts(&pass->tex[i].tex->opts);
            glUniform1i(shader->uni_tex[i], i);
        } else if(pass->tex[i].type == PG_PASS_TEX_RENDERTARGET
               && pass->tex[i].fb_tex != pass->target) {
            struct pg_renderbuffer* buf = pass->tex[i].fb_tex->buf[0];
            if(!buf || !buf->outputs[pass->tex[i].idx]) continue;
            glBindTexture(GL_TEXTURE_2D_ARRAY, buf->outputs[pass->tex[i].idx]->handle);
            glUniform1i(shader->uni_tex[i], i);
            if(shader->uni_tex_layer[i] != -1)
                glUniform1i(shader->uni_tex_layer[i], buf->layer[i]);
        } else if(pass->tex[i].type == PG_PASS_TEX_BUFFER) {
            glBindTexture(GL_TEXTURE_BUFFER, pass->tex[i].buf->tex_handle);
            glUniform1i(shader->uni_tex[i], i);
        }
    }
}

static inline void frame_swap_target(struct pg_shader* shader,
                                     struct pg_renderpass* pass)
{
    if(!pass->target) return;
    pg_rendertarget_swap(pass->target);
    pg_rendertarget_dst(pass->target);
    glClearColor(VEC_XYZW(pass->opts.clear_color));
    glClear(pass->opts.clear_buffers);
    int i;
    for(i = 0; i < 8; ++i) {
        if(shader->uni_tex[i] == -1 || pass->tex[i].type != PG_PASS_TEX_RENDERTARGET
        || pass->tex[i].fb_tex != pass->target) continue;
        struct pg_renderbuffer* buf = pass->tex[i].fb_tex->buf[1];
        if(!buf || !buf->outputs[pass->tex[i].idx]) continue;
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D_ARRAY, buf->outputs[pass->tex[i].idx]->handle);
        glUniform1i(shader->uni_tex[i], i);
        if(shader->uni_tex_layer[i] != -1)
            glUniform1i(shader->uni_tex_layer[i], buf->layer[i]);
    }

}

static void pass_opts(struct pg_renderpass* pass, struct pg_shader* shader)
{
    /*  Blending    */
    if(pass->opts.flags & PG_RENDERPASS_BLENDING) {
        glEnable(GL_BLEND);
        glBlendFunc(pass->opts.blend_func[0], pass->opts.blend_func[1]);
    } else glDisable(GL_BLEND);
    /*  Depth testing   */
    if(pass->opts.flags & PG_RENDERPASS_DEPTH_WRITE) glDepthMask(GL_TRUE);
    else glDepthMask(GL_FALSE);
    if(pass->opts.flags & PG_RENDERPASS_DEPTH_TEST) {
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(pass->opts.depth_func);
    } else glDisable(GL_DEPTH_TEST);
    /*  Face culling    */
    if(pass->opts.flags & PG_RENDERPASS_CULL_FACES) glEnable(GL_CULL_FACE);
    else glDisable(GL_CULL_FACE);
    if(!(pass->opts.flags & PG_RENDERPASS_BUFFER_SWAP)) {
        if(pass->target) pg_rendertarget_dst(pass->target);
        else pg_window_dst();
        glClear(pass->opts.clear_buffers);
    } else if(pass->opts.flags & PG_RENDERPASS_SWAP_BEFORE) {
        frame_swap_target(shader, pass);
    }
    /*  Viewport    */
    if(pass->opts.flags & PG_RENDERPASS_VIEWPORT) {
        glViewport( pass->resolution.x * pass->opts.viewport.x,
                    pass->resolution.y * pass->opts.viewport.y,
                    pass->resolution.x * pass->opts.viewport.z,
                    pass->resolution.y * pass->opts.viewport.w);
    }
    /*  Scissor */
    if(pass->opts.flags & PG_RENDERPASS_SCISSOR) {
        glScissor( pass->resolution.x * pass->opts.scissor.x,
                   pass->resolution.y * pass->opts.scissor.y,
                   pass->resolution.x * pass->opts.scissor.z,
                   pass->resolution.y * pass->opts.scissor.w );
        glEnable(GL_SCISSOR_TEST);
    } else glDisable(GL_SCISSOR_TEST);
}

void pg_renderer_draw_frame(struct pg_renderer* rend)
{
    struct pg_shader* lastshader = NULL;
    struct pg_shader* shader;
    struct pg_renderpass* pass = NULL;
    int i, j;
    ARR_FOREACH(rend->passes, pass, i) {
        if(!pass->enabled) continue;
        HTABLE_GET(rend->shaders, pass->shader, shader);
        if(!shader) continue;
        if(shader != lastshader) pg_shader_begin(shader);
        /*  Setup the model to draw, if there is one    */
        if(pass->verts) pg_vertex_buffer_begin(pass->verts, shader);
        else glBindVertexArray(rend->dummy_vao);
        /*  Get the viewer matrices */
        if(pass->viewer) {
            pass->mats[PG_PROJECTION_MATRIX] = pass->viewer->proj_matrix;
            pass->mats[PG_VIEW_MATRIX] = pass->viewer->view_matrix;
            pass->mats[PG_PROJECTIONVIEW_MATRIX] = pass->viewer->projview_matrix;
        }
        /*  Set the regular uniforms    */
        shader_base_mats(shader, pass);
        shader_textures(shader, pass);
        pg_shader_uniforms_from_table(shader, &pass->uniforms);
        pass_opts(pass, shader);
        /*  Set up the uniform indices which are passed to per_draw */
        GLint uni_idx[8];
        for(j = 0; j < 8; ++j) {
            struct pg_shader_uniform* sh_uni;
            HTABLE_GET(shader->uniforms, pass->draw_uniform[j], sh_uni);
            if(!sh_uni) {
                uni_idx[j] = -1;
                continue;
            } else uni_idx[j] = sh_uni->idx;
        }
        /*  The inner draw loop */
        j = 0;
        while(j < pass->drawdata.len) {
            /*  If this shader DOES swap targets with every draw,
                swap and set target here, and clear it on each draw,
                and also switch around the fbtextures uniforms  */
            if(pass->opts.flags & PG_RENDERPASS_SWAP_PER_DRAW)
                frame_swap_target(shader, pass);
            j += pass->per_draw(pass->drawdata.data + j, shader,
                                (const mat4*)pass->mats, uni_idx);
        }
        if(pass->opts.flags & PG_RENDERPASS_SWAP_AFTER) {
            frame_swap_target(shader, pass);
            glClear(pass->opts.clear_buffers);
        }
    }
}

void pg_renderpass_target(struct pg_renderpass* pass,
                          struct pg_rendertarget* target)
{
    pass->target = target;
    int w, h;
    if(target) {
        w = target->buf[0]->w;
        h = target->buf[0]->h;
    } else {
        vec2 win_sz = pg_window_size();
        w = win_sz.x;
        h = win_sz.y;
    }
    float ar = (float)w / h;
    pg_renderpass_uniform(pass, "pg_resolution", PG_VEC2, &PG_TYPE_FLOAT(w, h));
    pg_renderpass_uniform(pass, "pg_aspect_ratio", PG_FLOAT, &PG_TYPE_FLOAT(ar));
    pass->resolution = vec2(w, h);
    pass->aspect_ratio = ar;
}

void pg_renderpass_viewer(struct pg_renderpass* pass, struct pg_viewer* view)
{
    pass->viewer = view;
    if(!view) {
        pass->mats[PG_VIEW_MATRIX] = mat4_identity();
        pass->mats[PG_PROJECTION_MATRIX] = mat4_identity();
        pass->mats[PG_PROJECTIONVIEW_MATRIX] = mat4_identity();
    }
}

void pg_renderpass_enable(struct pg_renderpass* pass, int enabled)
{
    pass->enabled = enabled;
}

void pg_renderpass_blending(struct pg_renderpass* pass, int use,
                            GLenum left, GLenum right)
{
    if(!use) {
        pass->opts.flags &= ~PG_RENDERPASS_BLENDING;
        return;
    }
    pass->opts.flags |= PG_RENDERPASS_BLENDING;
    pass->opts.blend_func[0] = left;
    pass->opts.blend_func[1] = right;
}

void pg_renderpass_depth_write(struct pg_renderpass* pass, int use)
{
    if(use) pass->opts.flags |= PG_RENDERPASS_DEPTH_WRITE;
    else pass->opts.flags &= ~PG_RENDERPASS_DEPTH_WRITE;
}

void pg_renderpass_depth_test(struct pg_renderpass* pass, int use, GLenum func)
{
    if(!use) {
        pass->opts.flags &= ~PG_RENDERPASS_DEPTH_TEST;
        return;
    }
    pass->opts.flags |= PG_RENDERPASS_DEPTH_TEST;
    pass->opts.depth_func = func;
}

void pg_renderpass_clear_bits(struct pg_renderpass* pass, GLbitfield buffers)
{
    pass->opts.clear_buffers = buffers;
}

void pg_renderpass_viewport(struct pg_renderpass* pass, int use, struct pg_viewport* vp)
{
    if(!use) {
        pass->opts.flags &= ~PG_RENDERPASS_VIEWPORT;
        return;
    }
    pass->opts.flags |= PG_RENDERPASS_VIEWPORT;
    pass->opts.viewport = vec4( vp->pos.x * 0.5 + 0.5, vp->pos.y * 0.5 + 0.5,
                                vp->size.x * 0.5, vp->size.y * 0.5 );
}

void pg_renderpass_scissor(struct pg_renderpass* pass, int use, struct pg_viewport* vp)
{
    if(!use) {
        pass->opts.flags &= ~PG_RENDERPASS_SCISSOR;
        return;
    }
    pass->opts.flags |= PG_RENDERPASS_SCISSOR;
    pass->opts.scissor = vec4( vp->pos.x * 0.5 + 0.5, vp->pos.y * 0.5 + 0.5,
                               vp->size.x * 0.5, vp->size.y * 0.5 );
}

void pg_renderpass_swap(struct pg_renderpass* pass, uint32_t flags)
{
    uint32_t new_flags = pass->opts.flags & ~PG_RENDERPASS_BUFFER_SWAP;
    pass->opts.flags = new_flags | (flags & PG_RENDERPASS_BUFFER_SWAP);
}
