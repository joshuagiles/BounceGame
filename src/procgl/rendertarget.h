/********************/
/*  Rendertexture   */
/********************/

struct pg_renderbuffer {
    GLuint fbo;
    struct pg_texture* outputs[8];
    int layer[8];
    GLenum attachments[8];
    GLenum drawbufs[8];
    int dirty, w, h;
};

struct pg_rendertarget {
    struct pg_renderbuffer* buf[2];
};

void pg_renderbuffer_init(struct pg_renderbuffer* buffer);
void pg_renderbuffer_deinit(struct pg_renderbuffer* buffer);
void pg_renderbuffer_attach(struct pg_renderbuffer* buffer,
                             struct pg_texture* tex, int layer,
                             int idx, GLenum attachment);
void pg_renderbuffer_build(struct pg_renderbuffer* buffer);
void pg_renderbuffer_dst(struct pg_renderbuffer* buffer);

void pg_rendertarget_init(struct pg_rendertarget* target,
                          struct pg_renderbuffer* b0, struct pg_renderbuffer* b1);
void pg_rendertarget_dst(struct pg_rendertarget* target);
void pg_rendertarget_swap(struct pg_rendertarget* target);
vec2 pg_rendertarget_get_resolution(struct pg_rendertarget* target);
void pg_rendertarget_clear(struct pg_rendertarget* target, vec4 color);
