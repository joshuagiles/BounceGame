enum pg_data_type {
    PG_NULL,
    PG_BYTE, PG_BVEC2, PG_BVEC3, PG_BVEC4,
    PG_UBYTE, PG_UBVEC2, PG_UBVEC3, PG_UBVEC4,
    PG_INT, PG_IVEC2, PG_IVEC3, PG_IVEC4,
    PG_UINT, PG_UVEC2, PG_UVEC3, PG_UVEC4,
    PG_FLOAT, PG_VEC2, PG_VEC3, PG_VEC4,
    PG_MATRIX,
    PG_TEXTURE,
    PG_POINTER,
    PG_NUM_DATA_TYPES,
};

struct pg_type {
    union {
        char str[64];
        int i[16];
        unsigned u[16];
        float f[16];
        struct pg_texture* tex[8];
        void* ptr[8];
        mat4 m;
    };
};

struct pg_tagged_type_t {
    enum pg_data_type type;
    struct pg_type data;
};

typedef HTABLE_T(struct pg_tagged_type_t) pg_tagged_type_table_t;
typedef HTABLE_T(struct pg_type) pg_type_table_t;

#define PG_TYPE_FLOAT(...)   ((struct pg_type){ .f = { __VA_ARGS__ } })
#define PG_TYPE_INT(...)     ((struct pg_type){ .i = { __VA_ARGS__ } })
#define PG_TYPE_UINT(...)    ((struct pg_type){ .u = { __VA_ARGS__ } })
#define PG_TYPE_STRING(STR)  ((struct pg_type){ .str = (STR) })
#define PG_TYPE_TEXTURE(...) ((struct pg_type){ .tex = { __VA_ARGS__ } })
#define PG_TYPE_POINTER(...) ((struct pg_type){ .ptr = { __VA_ARGS__ } })
#define PG_TYPE_MATRIX(M)  ((struct pg_type){ .m = (M) })

static const GLenum pg_type_to_gl[] = {
    [PG_BYTE] = GL_BYTE,
    [PG_BVEC2] = GL_BYTE,
    [PG_BVEC3] = GL_BYTE,
    [PG_BVEC4] = GL_BYTE,
    [PG_UBYTE] = GL_UNSIGNED_BYTE,
    [PG_UBVEC2] = GL_UNSIGNED_BYTE,
    [PG_UBVEC3] = GL_UNSIGNED_BYTE,
    [PG_UBVEC4] = GL_UNSIGNED_BYTE,
    [PG_INT] = GL_INT,
    [PG_IVEC2] = GL_INT_VEC2,
    [PG_IVEC3] = GL_INT_VEC3,
    [PG_IVEC4] = GL_INT_VEC4,
    [PG_UINT] = GL_UNSIGNED_INT,
    [PG_UVEC2] = GL_UNSIGNED_INT_VEC2,
    [PG_UVEC3] = GL_UNSIGNED_INT_VEC3,
    [PG_UVEC4] = GL_UNSIGNED_INT_VEC4,
    [PG_FLOAT] = GL_FLOAT,
    [PG_VEC2] = GL_FLOAT_VEC2,
    [PG_VEC3] = GL_FLOAT_VEC3,
    [PG_VEC4] = GL_FLOAT_VEC4,
    [PG_MATRIX] = GL_FLOAT_MAT4,
    [PG_TEXTURE] = GL_SAMPLER_2D };

