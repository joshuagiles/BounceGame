
struct pg_vertex_attrib {
    char name[32];
    GLenum type;
    int as_integer, normalized, elements;
    size_t size, offset;
};

struct pg_vertex_buffer_face { uint32_t v[3]; };

#define PG_MODEL_VERTEX_MAX_FACES    16
struct pg_vertex_face_info {
    uint32_t faces[PG_MODEL_VERTEX_MAX_FACES];
    uint8_t num_faces;
};

struct pg_vertex_buffer {
    struct pg_vertex_attrib attribs[8];
    int num_attribs;
    void* vertex_data;
    uint32_t vertex_stride;
    uint32_t vertex_len;
    uint32_t vertex_cap;
    ARR_T(struct pg_vertex_buffer_face) faces;
    ARR_T(struct pg_vertex_face_info) vertex_info;
    /*  GL data */
    HTABLE_T(GLuint) vao_table;
    GLuint vbo;
    GLuint ebo;
};

void pg_vertex_buffer_init(struct pg_vertex_buffer* vbuf, int num_attribs,
                   const struct pg_vertex_attrib* attribs);
void pg_vertex_buffer_deinit(struct pg_vertex_buffer* vbuf);
void pg_vertex_buffer_spec(struct pg_vertex_buffer* vbuf, struct pg_shader* shader);
void pg_vertex_buffer_begin(struct pg_vertex_buffer* vbuf, struct pg_shader* shader);
void pg_vertex_buffer_buffer(struct pg_vertex_buffer* vbuf);
void pg_vertex_buffer_reserve_verts(struct pg_vertex_buffer* vbuf, uint32_t n);
void pg_vertex_buffer_reserve_faces(struct pg_vertex_buffer* vbuf, uint32_t n);
uint32_t pg_vertex_buffer_push_vertex(struct pg_vertex_buffer* vbuf, void* vertex);
void pg_vertex_buffer_set_vertex(struct pg_vertex_buffer* vbuf, int i, void* vertex);
void pg_vertex_buffer_add_face(struct pg_vertex_buffer* vbuf, uint32_t v0, uint32_t v1, uint32_t v2);
void pg_vertex_buffer_deduplicate(struct pg_vertex_buffer* vbuf,
        int (*vert_cmp)(const void*, const void*),
        void (*vert_merge)(void*, const void*));
void pg_vertex_buffer_clear(struct pg_vertex_buffer* vbuf);

void pg_vertex_buffer_load_obj(struct pg_vertex_buffer* vbuf, const char* filename);
