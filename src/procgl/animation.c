#include "procgl.h"

vec4 pg_anim_prop_value(struct pg_anim_property* prop, float t)
{
    if(!prop->anim) {
        return prop->base_value;
    } else if(prop->current_motion == 0 || prop->frame_time == -1) {
        const struct pg_anim_keyframe* frame = &prop->anim->keyframes[prop->current_frame];
        return frame->absolute ? frame->value : vec4_add(prop->base_value, frame->value);
    } else {
        int next_frame;
        if(prop->current_frame == prop->end_frame
        && prop->loop_mode == PG_ANIM_LOOP_CIRCULAR
        && prop->end_frame != prop->start_frame) {
            next_frame = prop->start_frame;
        } else {
            next_frame = LM_MOD(prop->current_frame + prop->current_motion,
                                prop->anim->num_frames);
        }
        const struct pg_anim_keyframe* f1 = &prop->anim->keyframes[prop->current_frame];
        const struct pg_anim_keyframe* f2 = &prop->anim->keyframes[next_frame];
        vec4 f1_val = f1->absolute ? f1->value : vec4_add(prop->base_value, f1->value);
        vec4 f2_val = f2->absolute ? f2->value : vec4_add(prop->base_value, f2->value);
        pg_easing_func_t func = prop->current_motion > 0 ? f1->ease_forward : f1->ease_back;
        if(func == NULL) func = pg_ease_lerp;
        float p = LM_SATURATE((t - prop->frame_time) / f1->dur_forward);
        return vec4_lerp(f1_val, f2_val, func(p));
    }
}

void pg_anim_prop_update(struct pg_anim_property* prop, float t)
{
    if(!prop->anim || prop->current_motion == 0) return;
    if(prop->frame_time == -1) prop->frame_time = t;
    float ft = t - prop->frame_time;
    const struct pg_anim_keyframe* f1 = &prop->anim->keyframes[prop->current_frame];
    float dur = (prop->current_motion > 0) ? f1->dur_forward : f1->dur_back;
    if(ft >= dur) {
        prop->frame_time = t;
        int next_frame = prop->current_frame + prop->current_motion;
        next_frame = LM_MOD(next_frame, prop->anim->num_frames);
        int loop_frame = next_frame;
        if(prop->loop_count != 0) {
            if(prop->current_motion == 1 && next_frame == prop->start_frame) {
                prop->current_frame = next_frame;
                return;
            } else if(prop->current_motion == -1 && next_frame == prop->end_frame) {
                prop->current_frame = next_frame;
                return;
            }
            if(prop->loop_mode == PG_ANIM_LOOP_CIRCULAR
            && prop->current_frame == prop->end_frame) loop_frame = prop->start_frame;
            if(prop->loop_mode == PG_ANIM_LOOP_REPEAT
            && (next_frame == prop->end_frame
            || next_frame == prop->start_frame)) prop->current_motion *= -1;
            if(loop_frame == prop->start_frame) {
                if(prop->loop_count > 0) --prop->loop_count;
                if(prop->loop_count == 0) prop->current_motion = 0;
            }
            prop->current_frame = loop_frame;
        } else if(next_frame == prop->end_frame) {
            prop->current_frame = next_frame;
            prop->current_motion = 0;
        }
    }
}

int pg_anim_status(struct pg_anim_property* prop)
{
    if(prop->current_motion == 0) return -1;
    return prop->current_frame;
}

void pg_animate(struct pg_anim_property* prop, const struct pg_animation* anim,
                int start_frame, int loop_start, int loop_end,
                int loop_count, int loop_mode)
{
    prop->anim = anim;
    if(!anim) return;
    prop->frame_time = -1;
    prop->current_frame = start_frame;
    prop->start_frame = loop_start;
    prop->end_frame = loop_end;
    prop->loop_count = loop_count;
    prop->loop_mode = loop_mode;
    if(loop_start < loop_end) {
        if(start_frame > loop_end) {
            prop->current_motion = -1;
        } else prop->current_motion = 1;
    } else if(loop_start > loop_end) {
        if(start_frame < loop_end) {
            prop->current_motion = 1;
        } else prop->current_motion = -1;
    } else {
        if(start_frame < loop_start) prop->current_motion = 1;
        else if(start_frame > loop_start) prop->current_motion = -1;
        else prop->current_motion = 0;
    }
}

vec4 pg_simple_anim_value(struct pg_simple_anim* anim, float t)
{
    if(!anim->duration) {
        return anim->start;
    } else if(anim->start_time < 0) {
        anim->start_time = t;
        return anim->start;
    }
    float p = LM_SATURATE((t - anim->start_time) / anim->duration);
    return vec4_lerp(anim->start, anim->end, anim->ease(p));
}

int pg_simple_anim_finished(struct pg_simple_anim* anim, float t)
{
    if(anim->start_time > 0 && t > anim->start_time + anim->duration) return 1;
    else return 0;
}
