/*  Start procgame; creates a window, GL context, initializes gamepad data,
    and initializes audio system  */
int pg_init(int w, int h, int fullscreen, const char* window_title);
void pg_deinit(void);

/*************************/
/*  Window management    */
/*************************/
void pg_window_resize(int w, int h, int fullscreen);
vec2 pg_screen_size(void);
vec2 pg_window_size(void);
float pg_window_aspect(void);
void pg_window_swap(void);
void pg_window_dst(void);

/****************************/
/*  Performance testing     */
/****************************/
double pg_time(void); // Time in seconds
void pg_calc_framerate(double new_time); // Call once per frame
float pg_framerate(void);   // Get framerate without recalculating it
/*  Granular performance testing using SDL2's performance counter   */
uint64_t pg_perf_time(void);
double pg_perf_time_diff(uint64_t start, uint64_t end);

/************************************************/
/*  Standardized direction vectors and things   */
/************************************************/
enum pg_direction {
    PG_NO_DIRECTION = 0,
    PG_FRONT = 1,
    PG_Y_POS = PG_FRONT,
    PG_BACK = 2,
    PG_Y_NEG = PG_BACK,
    PG_LEFT = 3,
    PG_X_POS = PG_LEFT,
    PG_RIGHT = 4,
    PG_X_NEG = PG_RIGHT,
    PG_UP = 5,
    PG_Z_POS = PG_UP,
    PG_TOP = PG_UP,
    PG_DOWN = 6,
    PG_Z_NEG = PG_DOWN,
    PG_BOTTOM = PG_DOWN,
};

static const int PG_DIR_OPPOSITE[7] = { 0, 2, 1, 4, 3, 6, 5 };
static const vec3 PG_DIR_VEC[7] = {
    [PG_NO_DIRECTION] = {{ 0, 0, 0 }},
    [PG_FRONT] = {{ 0, 1.0f, 0 }},
    [PG_BACK] = {{ 0, -1.0f, 0 }},
    [PG_LEFT] = {{ 1.0f, 0, 0 }},
    [PG_RIGHT] = {{ -1.0f, 0, 0 }},
    [PG_UP] = {{ 0, 0, 1.0f }},
    [PG_DOWN] = {{ 0, 0, -1.0f }}, };
static const vec3 PG_DIR_TAN[7] = {
    [PG_NO_DIRECTION] = {{ 0, 0, 0 }},
    [PG_FRONT] = {{ -1.0f, 0, 0 }},
    [PG_BACK] = {{ 1.0f, 0, 0 }},
    [PG_LEFT] = {{ 0, 1.0f, 0 }},
    [PG_RIGHT] = {{ 0, -1.0f, 0 }},
    [PG_UP] = {{ 1.0f, 0, 0 }},
    [PG_DOWN] = {{ -1.0f, 0, 0 }}, };
static const vec3 PG_DIR_BITAN[7] = {
    [PG_NO_DIRECTION] = {{ 0, 0, 0 }},
    [PG_FRONT] = {{ 0, 0, 1.0f }},
    [PG_BACK] = {{ 0, 0, -1.0f }},
    [PG_LEFT] = {{ 0, 0, 1.0f }},
    [PG_RIGHT] = {{ 0, 0, -1.0f }},
    [PG_UP] = {{ 0, 1.0f, 0 }},
    [PG_DOWN] = {{ 0, -1.0f, 0 }}, };
static const int PG_DIR_IDX[7][3] = {
    [PG_NO_DIRECTION] = { 0, 0, 0 },
    [PG_FRONT] = { 0, 1, 0 },
    [PG_BACK] = { 0, -1, 0 },
    [PG_LEFT] = { 1, 0, 0 },
    [PG_RIGHT] = { -1, 0, 0 },
    [PG_UP] = { 0, 0, 1 },
    [PG_DOWN] = { 0, 0, -1 }, };

enum pg_direction pg_get_direction(vec3 v);

