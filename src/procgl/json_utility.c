#include "procgl.h"

/*  Load a JSON object from a file, free everything but the parsed JSON  */
cJSON* load_json(const char* filename)
{
    /*  Read file, return default config and 0 if it can't be read. */
    FILE* file = fopen(filename, "r");
    if(!file) {
        printf("ERROR Failed to read asset_image from file: %s\n", filename);
        return NULL;
    }
    fseek(file, 0, SEEK_END);
    size_t len = ftell(file);
    fseek(file, 0, SEEK_SET);
    char* json_str = malloc(sizeof(*json_str) * len);
    fread(json_str, 1, len, file);
    fclose(file);
    /*  Now there is a string in memory, it must be parsed  */
    cJSON* json = cJSON_Parse(json_str);
    free(json_str);
    return json;
}

int json_get_child_vec2(const cJSON* json, char* child_name, vec2* out)
{
    cJSON* child = cJSON_GetObjectItemCaseSensitive(json, child_name);
    if(cJSON_IsArray(child) && cJSON_GetArraySize(child) >= 2) {
        child = child->child;
        out->x = child->valuedouble;
        child = child->next;
        out->y = child->valuedouble;
        return 1;
    }
    return 0;
}

int json_get_child_float(const cJSON* json, char* child_name, float* out)
{
    cJSON* child = cJSON_GetObjectItemCaseSensitive(json, child_name);
    if(cJSON_IsNumber(child)) {
        *out = child->valuedouble;
        return 1;
    }
    return 0;
}

int json_get_child_int(const cJSON* json, char* child_name, int* out)
{
    cJSON* child = cJSON_GetObjectItemCaseSensitive(json, child_name);
    if(cJSON_IsNumber(child)) {
        *out = child->valueint;
        return 1;
    }
    return 0;
}

int json_get_child_bool(const cJSON* json, char* child_name, int* out)
{
    cJSON* child = cJSON_GetObjectItemCaseSensitive(json, child_name);
    if(cJSON_IsTrue(child)) {
        *out = 1;
        return 1;
    } else if(cJSON_IsFalse(child)) {
        *out = 0;
        return 1;
    }
    *out = 0;
    return 0;
}

int json_get_child_string(const cJSON* json, char* child_name, char* out, int out_max)
{
    cJSON* child = cJSON_GetObjectItemCaseSensitive(json, child_name);
    if(cJSON_IsString(child)) {
        strncpy(out, child->valuestring, out_max);
        return 1;
    }
    return 0;
}

void json_get_vec2(const cJSON* json, vec2* out)
{
    out->x = json->child->valuedouble;
    out->y = json->child->next->valuedouble;
}

void json_get_float(const cJSON* json, float* out)
{
    *out = json->valuedouble;
}

void json_get_int(const cJSON* json, int* out)
{
    *out = json->valueint;
}

void json_get_bool(const cJSON* json, int* out)
{
    if(cJSON_IsTrue(json)) *out = 1;
    else if(cJSON_IsFalse(json)) *out = 0;
    else *out = 0;
}

void json_get_string(const cJSON* json, char* out, int out_max)
{
    strncpy(out, json->valuestring, out_max);
}

