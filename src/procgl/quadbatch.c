#include "procgl.h"

/****************************************/
/*  Unified sprite/text batching shader */
/****************************************/

static size_t drawfunc_batch(const void* draw, const struct pg_shader* shader,
                             const mat4* mats, const GLint* idx)
{
    const struct pg_type* unis = draw;
    if(unis[0].i[2]) {
        glStencilMask((GLuint)unis[0].i[3]);
        glStencilFunc(unis[0].i[4], unis[0].i[5], (GLuint)(unis[0].i[6] & 0xFF));
        glStencilOp(unis[0].i[7], unis[0].i[8], unis[0].i[9]);
        if(unis[0].i[10]) glEnable(GL_STENCIL_TEST);
        else glDisable(GL_STENCIL_TEST);
        return 1 * sizeof(struct pg_type);
    } else {
        mat4 mvp = mat4_mul(mats[PG_PROJECTIONVIEW_MATRIX], unis[1].m);
        glUniformMatrix4fv(idx[0], 1, GL_FALSE, mvp.v);
        glUniform1i(idx[1], unis[0].i[0] * 4);
        glDrawArrays(GL_TRIANGLES, 0, 6 * unis[0].i[1]);
        return 2 * sizeof(struct pg_type);
    }
}

void pg_quadbatch_init(struct pg_quadbatch* batch, int size)
{
    pg_buffertex_init(&batch->buf, PG_VEC4, 4, size);
    batch->cur_start = 0;
    batch->cur_idx = 0;
    batch->cur_transform = mat4_identity();
}

void pg_quadbatch_deinit(struct pg_quadbatch* batch)
{
    pg_buffertex_deinit(&batch->buf);
}

/*  Build a renderpass to use the batched data  */
void pg_quadbatch_init_pass(struct pg_quadbatch* batch,
                            struct pg_renderpass* pass, struct pg_rendertarget* target)
{
    pg_renderpass_init(pass, "spritebatch", PG_RENDERPASS_OPTS(
        PG_RENDERPASS_BLENDING,
        .blend_func = { GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA }));
    pg_renderpass_target(pass, target);
    pg_renderpass_set_drawfunc(pass, drawfunc_batch, 2,
                            "pg_matrix_mvp", "sprite_offset");
    pg_renderpass_buftexture(pass, 0, &batch->buf);
}

void pg_quadbatch_pass_texture(struct pg_renderpass* pass,
            struct pg_texture* tex0, struct pg_texture_opts* opts0,
            struct pg_texture* tex1, struct pg_texture_opts* opts1)
{
    pg_renderpass_texture(pass, 1, tex0, opts0);
    pg_renderpass_texture(pass, 2, tex1, opts1);
}

void pg_quadbatch_add_quad(struct pg_quadbatch* batch, struct pg_ezquad* quad)
{
    pg_buffertex_push(&batch->buf, quad, 1);
    batch->cur_idx += 1;
}

/*  Add sprites or text to a current batch group    */
void pg_quadbatch_add_sprite(struct pg_quadbatch* batch, struct pg_draw_2d* draw)
{

    uint32_t color_add = 0;
    color_add |= (uint32_t)(draw->color_add.x * 255) << 24;
    color_add |= (uint32_t)(draw->color_add.y * 255) << 16;
    color_add |= (uint32_t)(draw->color_add.z * 255) << 8;
    color_add |= (uint32_t)(draw->color_add.w * 255) << 0;
    uint32_t color_mod = 0;
    color_mod |= (uint32_t)(draw->color_mod.x * 255) << 24;
    color_mod |= (uint32_t)(draw->color_mod.y * 255) << 16;
    color_mod |= (uint32_t)(draw->color_mod.z * 255) << 8;
    color_mod |= (uint32_t)(draw->color_mod.w * 255) << 0;
    struct pg_ezquad q = {
        .pos = vec3( draw->pos.x, draw->pos.y, draw->pos.z ),
        .scale = draw->scale,
        .orientation = quat_rotation(vec3(0, 0, 1), -draw->rotation),
        .color_add = color_add, .color_mod = color_mod,
        .tex_layer = draw->frame.layer, .tex_select = 0,
        .tex_frame = vec4(VEC_XY(draw->frame.frame[0]), VEC_XY(draw->frame.frame[1])),
    };
    pg_buffertex_push(&batch->buf, &q, 1);
    batch->cur_idx += 1;
}

static inline void add_text_form(struct pg_quadbatch* batch, struct pg_text_form* form,
                                 struct pg_draw_text* draw)
{
    struct pg_ezquad q = { .tex_select = 1,
        .orientation = quat_rotation(vec3(0, 0, 1), -draw->rotation),
        .color_add = 0, .color_mod = VEC4_TO_UINT(draw->color),
    };
    uint32_t last_color = 0;
    uint32_t last_color_calc = 0;
    float rot_c = cos(-draw->rotation);
    float rot_s = sin(-draw->rotation);
    int i;
    for(i = 0; i < form->n_glyphs; ++i) {
        struct pg_text_form_glyph* fglyph = &form->glyphs[i];
        vec2 g_pos = vec2_mul(vec2_sub(fglyph->pos, draw->anchor), draw->scale);
        q.pos.x = g_pos.x*rot_c + g_pos.y*rot_s + draw->pos.x + draw->anchor.x;
        q.pos.y = g_pos.x*rot_s - g_pos.y*rot_c + draw->pos.y + draw->anchor.y;
        q.scale = vec2_mul(fglyph->scale, draw->scale);
        q.tex_frame = vec4(VEC_XY(fglyph->uv0), VEC_XY(fglyph->uv1));
        q.tex_layer = fglyph->tex_layer;
        if(fglyph->color != last_color) {
            last_color = fglyph->color;
            vec4 color = vec4((last_color>>24)&0xFF, (last_color>>16)&0xFF,
                              (last_color>>8)&0xFF, last_color&0xFF);
            color = vec4_scale(color, (1.0f / 255.0f));
            color = vec4_mul(color, draw->color);
            last_color_calc = VEC4_TO_UINT(color);
        }
        q.color_mod = last_color_calc;
        pg_buffertex_push(&batch->buf, &q, 1);
    }
    batch->cur_idx += i;
}

void pg_quadbatch_add_text(struct pg_quadbatch* batch, struct pg_draw_text* draw)
{
    if(draw->form) add_text_form(batch, draw->form, draw);
    else if(draw->formatter && draw->len) {
        struct pg_text_form_glyph f_glyphs[draw->len];
        struct pg_text_form form;
        pg_text_form_init_ptr(&form, f_glyphs, draw->len);
        if(draw->str) pg_text_format(&form, draw->formatter, draw->str, draw->len);
        else if(draw->str_c) pg_text_format_c(&form, draw->formatter, draw->str_c, draw->len);
        add_text_form(batch, &form, draw);
    }
}

void pg_quadbatch_stencil(struct pg_quadbatch* batch, struct pg_renderpass* pass,
                          struct pg_quadbatch_stencil* stencil)
{
    struct pg_type uni = PG_TYPE_INT(0,0,1,
            stencil->write_mask,
            stencil->func, stencil->ref, stencil->func_mask,
            stencil->sfail, stencil->dpfail, stencil->dppass,
            stencil->test);
    pg_renderpass_add_drawdata(pass, sizeof(uni), &uni);
}

/*  Emit a draw operation to a renderpass for the current batch group   */
void pg_quadbatch_draw(struct pg_quadbatch* batch, struct pg_renderpass* pass)
{
    if(batch->cur_idx == 0) return;
    struct pg_type unis[2] = {
        PG_TYPE_INT(batch->cur_start, batch->cur_idx),
        PG_TYPE_MATRIX(batch->cur_transform),
    };
    pg_renderpass_add_drawdata(pass, sizeof(unis), unis);
}

/*  Begin a new batch group with a given model matrix   */
void pg_quadbatch_next(struct pg_quadbatch* batch, mat4* transform)
{
    batch->cur_start += batch->cur_idx;
    batch->cur_idx = 0;
    batch->cur_transform = *transform;
}

/*  Upload all the batch groups to the GPU so it can be read at draw time   */
void pg_quadbatch_buffer(struct pg_quadbatch* batch)
{
    pg_buffertex_sub(&batch->buf, 0, batch->cur_start + batch->cur_idx);
}

/*  Restart batching    */
void pg_quadbatch_reset(struct pg_quadbatch* batch)
{
    pg_buffertex_seek(&batch->buf, 0);
    batch->cur_start = 0;
    batch->cur_idx = 0;
    batch->cur_transform = mat4_identity();
}
