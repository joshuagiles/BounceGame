#include "procgl.h"
#include "ext/lodepng.h"

void pg_renderbuffer_init(struct pg_renderbuffer* buffer)
{
    *buffer = (struct pg_renderbuffer){};
    glGenFramebuffers(1, &buffer->fbo);
}

void pg_renderbuffer_deinit(struct pg_renderbuffer* buffer)
{
    glDeleteFramebuffers(1, &buffer->fbo);
}

void pg_renderbuffer_attach(struct pg_renderbuffer* buffer,
                             struct pg_texture* tex, int layer,
                             int idx, GLenum attachment)
{
    buffer->outputs[idx] = tex;
    buffer->layer[idx] = layer;
    buffer->attachments[idx] = tex ? attachment : GL_NONE;
    buffer->dirty = 1;
}

void pg_renderbuffer_build(struct pg_renderbuffer* buffer)
{
    glBindFramebuffer(GL_FRAMEBUFFER, buffer->fbo);
    int w = -1, h = -1;
    buffer->dirty = 0;
    int i;
    for(i = 0; i < 8; ++i) {
        if(!buffer->outputs[i]) continue;
        if(w < 0) w = buffer->outputs[i]->w;
        else w = LM_MIN(w, buffer->outputs[i]->w);
        if(h < 0) h = buffer->outputs[i]->h;
        else h = LM_MIN(h, buffer->outputs[i]->h);
        glFramebufferTextureLayer(GL_FRAMEBUFFER, buffer->attachments[i],
                                  buffer->outputs[i]->handle, 0, buffer->layer[i]);
        if(buffer->attachments[i] != GL_DEPTH_ATTACHMENT
        && buffer->attachments[i] != GL_DEPTH_STENCIL_ATTACHMENT) {
            buffer->drawbufs[i] = buffer->attachments[i];
        } else {
            buffer->drawbufs[i] = GL_NONE;
        }
    }
    buffer->w = w;
    buffer->h = h;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void pg_renderbuffer_dst(struct pg_renderbuffer* buffer)
{
    if(buffer->dirty) {
        pg_renderbuffer_build(buffer);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, buffer->fbo);
    glDrawBuffers(8, buffer->drawbufs);
    glViewport(0, 0, buffer->w, buffer->h);
}

void pg_rendertarget_init(struct pg_rendertarget* target,
                          struct pg_renderbuffer* b0, struct pg_renderbuffer* b1)
{
    target->buf[0] = b0;
    target->buf[1] = b1;
    if(b1 && (b1->w != b0->w || b1->h != b0->h)) {
        printf("Procgl WARNING: pg_rendertarget buffer size mismatch\n");
    }
}

void pg_rendertarget_dst(struct pg_rendertarget* target)
{
    pg_renderbuffer_dst(target->buf[0]);
}

void pg_rendertarget_swap(struct pg_rendertarget* target)
{
    if(!target->buf[1]) return;
    struct pg_renderbuffer* tmp = target->buf[0];
    target->buf[0] = target->buf[1];
    target->buf[1] = tmp;
}

vec2 pg_rendertarget_get_resolution(struct pg_rendertarget* target)
{
    if(!target || !target->buf[0]) {
        return pg_window_size();
    }
    return vec2(target->buf[0]->w, target->buf[0]->h);
}

void pg_rendertarget_clear(struct pg_rendertarget* target, vec4 color)
{
    pg_rendertarget_dst(target);
    glDepthMask(GL_TRUE);
    glStencilMask(0xFF);
    glClearColor(VEC_XYZW(color));
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}
