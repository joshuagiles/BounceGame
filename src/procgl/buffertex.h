/********************/
/*  Buffer texture  */
/********************/

struct pg_buffertex {
    void* data;
    enum pg_data_type type;
    size_t stride;
    GLuint tex_handle;
    GLuint buf_handle;
    /*  Buffer capacity */
    uint32_t data_cap;
    /*  Index for the next write */
    uint32_t write_idx;
    uint32_t group_size;
    uint32_t group_stride;
};

void pg_buffertex_init(struct pg_buffertex* buf, enum pg_data_type type,
                       uint32_t group_size, uint32_t group_count);
void pg_buffertex_deinit(struct pg_buffertex* buf);
/*  Move write index to given value */
void pg_buffertex_seek(struct pg_buffertex* buf, int idx);
/*  Write formatted data (group_size per entry) */
void pg_buffertex_push(struct pg_buffertex* buf, void* data, int count);
/*  Upload data to GPU  */
void pg_buffertex_buffer(struct pg_buffertex* buf);
void pg_buffertex_sub(struct pg_buffertex* buf, uint32_t idx, uint32_t len);

