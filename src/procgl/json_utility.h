
cJSON* load_json(const char* filename);

int json_get_child_vec2(const cJSON* json, char* child_name, vec2* out);
int json_get_child_float(const cJSON* json, char* child_name, float* out);
int json_get_child_int(const cJSON* json, char* child_name, int* out);
int json_get_child_bool(const cJSON* json, char* child_name, int* out);
int json_get_child_string(const cJSON* json, char* child_name, char* out, int out_max);

void json_get_vec2(const cJSON* json, vec2* out);
void json_get_float(const cJSON* json, float* out);
void json_get_int(const cJSON* json, int* out);
void json_get_bool(const cJSON* json, int* out);
void json_get_string(const cJSON* json, char* out, int out_max);

