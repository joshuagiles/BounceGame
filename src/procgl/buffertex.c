#include "procgl.h"
#include "ext/lodepng.h"

void pg_buffertex_init(struct pg_buffertex* buf, enum pg_data_type type,
                       uint32_t group_size, uint32_t group_count)
{
    const struct tex_format* fmt = &gl_tex_formats[type];
    buf->type = type;
    buf->stride = fmt->size;
    buf->data_cap = buf->stride * group_size * group_count;
    buf->data = calloc(1, (buf->stride * buf->data_cap));
    buf->write_idx = 0;
    buf->group_size = group_size;
    buf->group_stride = group_size * buf->stride;
    glGenTextures(1, &buf->tex_handle);
    glGenBuffers(1, &buf->buf_handle);
    glBindBuffer(GL_TEXTURE_BUFFER, buf->buf_handle);
    glBufferData(GL_TEXTURE_BUFFER, buf->data_cap, buf->data, GL_DYNAMIC_DRAW);
    glBindTexture(GL_TEXTURE_BUFFER, buf->tex_handle);
    glTexBuffer(GL_TEXTURE_BUFFER, fmt->iformat, buf->buf_handle);
}

void pg_buffertex_deinit(struct pg_buffertex* buf)
{
    glDeleteTextures(1, &buf->tex_handle);
    glDeleteBuffers(1, &buf->buf_handle);
    free(buf->data);
    *buf = (struct pg_buffertex){};
}

/*  Move write index to given value */
void pg_buffertex_seek(struct pg_buffertex* buf, int idx)
{
    buf->write_idx = idx * buf->group_stride;
}

/*  Write formatted data (group_size per entry) */
void pg_buffertex_push(struct pg_buffertex* buf, void* data, int count)
{
    uint32_t len = buf->group_stride * count;
    if(buf->write_idx + len >= buf->data_cap) return;
    memcpy(buf->data + buf->write_idx, data, len);
    buf->write_idx += len;
}

/*  Upload data to GPU  */
void pg_buffertex_buffer(struct pg_buffertex* buf)
{
    const struct tex_format* fmt = &gl_tex_formats[buf->type];
    glBindBuffer(GL_TEXTURE_BUFFER, buf->buf_handle);
    glBufferData(GL_TEXTURE_BUFFER, buf->data_cap, buf->data, GL_DYNAMIC_DRAW);
    glBindTexture(GL_TEXTURE_BUFFER, buf->tex_handle);
    glTexBuffer(GL_TEXTURE_BUFFER, fmt->iformat, buf->buf_handle);
}

void pg_buffertex_sub(struct pg_buffertex* buf, uint32_t idx, uint32_t len)
{
    const struct tex_format* fmt = &gl_tex_formats[buf->type];
    glBindBuffer(GL_TEXTURE_BUFFER, buf->buf_handle);
    glBufferSubData(GL_TEXTURE_BUFFER,
                    idx * buf->group_stride,
                    len * buf->group_stride,
                    buf->data + idx * buf->group_stride);
    glBindTexture(GL_TEXTURE_BUFFER, buf->tex_handle);
    glTexBuffer(GL_TEXTURE_BUFFER, fmt->iformat, buf->buf_handle);
}

