
#define PG_ARMATURE_MAX_BONES   32
#define PG_ARMATURE_MAX_POSES   32

/****************************/
/*  Rigging and animation   */
/****************************/

struct pg_armature_bone_tx {
    quat rot;
    vec3 move;
    uint32_t enabled;
};

struct pg_armature_pose {
    struct pg_armature_bone_tx bones[PG_ARMATURE_MAX_BONES];
    int n_bones;
};

struct pg_armature_bone {
    char name[32];
    int8_t parent;
    int8_t children[8];
    int8_t n_children;
    vec3 local_origin;
    vec3 local_space[3];
    quat local_quat;
    float len;
};

struct pg_armature_sequence {
    int poses[16];
    int n_poses;
};

struct pg_armature {
    struct pg_armature_bone bones[PG_ARMATURE_MAX_BONES];
    struct pg_armature_pose poses[PG_ARMATURE_MAX_POSES];
    struct pg_armature_sequence seqs[32];
    int n_poses, n_seqs, n_bones;
    int_table_t bone_table;
    int_table_t pose_table;
    int_table_t sequence_table;
};

/************************/
/*  Armature functions  */
void pg_armature_load(struct pg_armature* rig, const char* filename);
/*  Get an animation sequence by name   */
struct pg_armature_sequence* pg_armature_get_sequence(struct pg_armature* rig, char* name);
/*  Get the index of a pose in an armature's pose library, by name  */
int pg_armature_pose_by_name(struct pg_armature* rig, char* name);
/*  Get the index of a bone in an armature, by name */
int pg_armature_bone_by_name(struct pg_armature* rig, char* name);
/*  Create a pose by lerping two poses from an armature's pose library  */
void pg_armature_get_lerp_pose(struct pg_armature* rig, struct pg_armature_pose* out,
                               int pose_0, int pose_1, float pose_lerp);

/********************/
/*  Pose functions  */
/*  Create a pose with every bone empty  */
void pg_armature_pose_empty(struct pg_armature_pose* pose);
/*  Create a pose by lerping any two other poses    */
void pg_armature_pose_lerp(struct pg_armature* rig, struct pg_armature_pose* out,
                           struct pg_armature_pose* p0, struct pg_armature_pose* p1,
                           float pose_lerp);
/*  Create a pose by applying one pose after another pose   */
void pg_armature_pose_apply(struct pg_armature* rig, struct pg_armature_pose* out,
                           struct pg_armature_pose* p0, struct pg_armature_pose* p1);
/*  Create a pose by propagating every bone's transform recursively */
void pg_armature_pose_propagate(struct pg_armature* rig, struct pg_armature_pose* out,
                                struct pg_armature_pose* pose);

/*  Apply a transformation to a bone in a pose  */
void pg_armature_pose_edit_bone(struct pg_armature* rig, struct pg_armature_pose* pose,
                                int bone_idx, quat rot, vec3 move);
/*  Get a bone transform from a pose    */
void pg_armature_pose_get_bone(struct pg_armature* rig, struct pg_armature_pose* pose,
                                  struct pg_armature_bone_tx* tx_out, int bone_idx);
/*  Get an attachment transform for a bone from a pose    */
void pg_armature_pose_get_attachment(struct pg_armature* rig, struct pg_armature_pose* pose,
                                  struct pg_armature_bone_tx* tx_out, int bone_idx,
                                  vec3 attach_point);

/*  Apply a transformation to a bone transform (for instance, to convert from
    object space - how the bones and poses are stored - to world space  */
void pg_armature_bone_transform(struct pg_armature_bone_tx* tx,
                                quat rot, vec3 move, float scale);
