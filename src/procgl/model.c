#include "procgl.h"

struct model_vertex { vec3 pos; vec3 tex; vec3 rig; };

static const struct pg_vertex_attrib model_attribs[3] = {
    { "v_position", .type = GL_FLOAT, .elements = 3, .size = sizeof(vec3) },
    { "v_tex_coord", .type = GL_FLOAT, .elements = 3, .size = sizeof(vec3) },
    { "v_rigging", .type = GL_FLOAT, .elements = 3, .size = sizeof(vec3) },
};

void pg_model_init_vertex_buffer(struct pg_vertex_buffer* verts)
{
    pg_vertex_buffer_init(verts, 3, model_attribs);
}

static int vert_cmp(const void* v0_, const void* v1_)
{
    const struct model_vertex* v0 = v0_, *v1 = v1_;
    if((vec3_dist2(v0->pos, v1->pos) < 0.0000001)
    && (vec3_dist2(v0->tex, v1->tex) < 0.00001)
    && (vec3_dist2(v0->rig, v1->rig) < 0.0000001)) return 1;
    return 0;

}

void pg_model_buffer_deduplicate(struct pg_vertex_buffer* vbuf)
{
    uint32_t start_len = vbuf->vertex_len;
    pg_vertex_buffer_deduplicate(vbuf, vert_cmp, NULL);
    uint32_t end_len = vbuf->vertex_len;
    printf("Deduplicated %u vertices\n", start_len - end_len);
}

void pg_model_load(struct pg_model* model, struct pg_vertex_buffer* verts,
                   const char* filename, int tex_layer)
{
    model->vbuf = verts;
    model->vertex_range[0] = verts->vertex_len;
    uint32_t first_vert = verts->vertex_len;
    
    FILE* file = fopen(filename, "r");
    if(!file) {
        printf("procgame ERROR: Failed to load model file: %s\n", filename);
        return;
    }
    
    /*  Read vertex count   */
    int n_verts;
    fscanf(file, " V %d\n", &n_verts);
    pg_vertex_buffer_reserve_verts(verts, verts->vertex_len + n_verts);
    pg_vertex_buffer_reserve_faces(verts, verts->faces.len + (n_verts / 3));
    model->vertex_range[1] = n_verts;

    /*  Read vertex data    */
    struct model_vertex vert;
    int i, j;
    for(i = 0; i < n_verts; ++i) {
        fscanf(file, " ( %f , %f , %f ) ( %f , %f ) ( %f , %f , %f )",
            &vert.pos.x, &vert.pos.y, &vert.pos.z,
            &vert.tex.x, &vert.tex.y,
            &vert.rig.x, &vert.rig.y, &vert.rig.z );
        vert.tex.y = 1.0f - vert.tex.y;
        vert.tex.z = tex_layer;
        pg_vertex_buffer_push_vertex(verts, &vert);
    }
    model->index_range[0] = verts->faces.len * 3;
    model->index_range[1] = n_verts;
    for(i = 0; i < n_verts; i += 3) {
        pg_vertex_buffer_add_face(verts,
            (i + first_vert), (i + first_vert + 1), (i + first_vert + 2));
    }

    printf("Model %s has %u verts, vertex buffer has %u verts.\n",
           filename, n_verts, verts->vertex_len);
    printf("Model now has %u verts\n", verts->vertex_len);

    fclose(file);
}

void pg_model_draw_static_to_vb(struct pg_vertex_buffer* vbuf_out,
                                struct pg_model* model, mat4 tx)
{
    int start_len = vbuf_out->vertex_len;
    int i;
    struct model_vertex* vert_data = model->vbuf->vertex_data;
    int vert_start = model->vertex_range[0];
    int vert_end = model->vertex_range[0] + model->vertex_range[1];
    for(i = vert_start; i < vert_end; ++i) {
        struct model_vertex new_vert = vert_data[i];
        vec4 v4 = vec4(VEC_XYZ(new_vert.pos), 1);
        v4 = mat4_mul_vec4(tx, v4);
        new_vert.pos = vec3(VEC_XYZ(v4));
        pg_vertex_buffer_push_vertex(vbuf_out, &new_vert);
    }
    int index_offset = start_len - model->vertex_range[0];
    struct pg_vertex_buffer_face* face_data = model->vbuf->faces.data;
    int face_start = model->index_range[0] / 3;
    int face_end = (model->index_range[0] /3)+( model->index_range[1] / 3);
    for(i = face_start; i < face_end; ++i) {
        struct pg_vertex_buffer_face face = face_data[i];
        pg_vertex_buffer_add_face(vbuf_out, face.v[0] + index_offset,
            face.v[1] + index_offset, face.v[2] + index_offset);
    }
}

void pg_model_empty(struct pg_model* model, struct pg_vertex_buffer* vbuf)
{
    *model = (struct pg_model){ .vbuf = vbuf };
}

/****************************************/
/*  Rendering                           */
/****************************************/
static size_t drawfunc_static_model(const void* draw, const struct pg_shader* shader,
                                    const mat4* mats, const GLint* idx)
{
    const mat4* tx = draw;
    uint32_t* index_range = (uint32_t*)((mat4*)draw + 1);
    mat4 mvp = mat4_mul(mats[PG_PROJECTIONVIEW_MATRIX], *tx);
    glUniformMatrix4fv(idx[0], 1, GL_FALSE, mvp.v);
    glDrawElements(GL_TRIANGLES, index_range[1], GL_UNSIGNED_INT,
        (GLvoid*)(index_range[0] * sizeof(uint32_t)));
    return sizeof(*tx) + sizeof(uint32_t) * 2;
}

static size_t drawfunc_rigged_model(const void* draw, const struct pg_shader* shader,
                                    const mat4* mats, const GLint* idx)
{
    const mat4* tx = draw;
    const uint32_t* index_range = (uint32_t*)((mat4*)draw + 1);
    const int n_bones = *(int*)(index_range + 2);
    const mat4* bone_mats = (mat4*)(index_range + 3);
    mat4 mvp = mat4_mul(mats[PG_PROJECTIONVIEW_MATRIX], *tx);
    glUniformMatrix4fv(idx[0], 1, GL_FALSE, mvp.v);
    glUniformMatrix4fv(idx[1], n_bones, GL_FALSE, (float*)bone_mats);
    glDrawElements(GL_TRIANGLES, index_range[1], GL_UNSIGNED_INT,
            (GLvoid*)(index_range[0] * sizeof(uint32_t)));
    return sizeof(*tx)
        + (sizeof(uint32_t) * 2)
        + (sizeof(int))
        + (sizeof(mat4) * n_bones);
}

void pg_model_append_static(struct pg_model* dst, struct pg_model* src, mat4 tx)
{
    uint32_t start_vertex_len = dst->vbuf->vertex_len;
    uint32_t start_faces_len = dst->vbuf->faces.len;
    pg_model_draw_static_to_vb(dst->vbuf, src, tx);
    uint32_t end_vertex_len = dst->vbuf->vertex_len;
    uint32_t end_faces_len = dst->vbuf->faces.len;
    dst->vertex_range[1] += (end_vertex_len - start_vertex_len);
    dst->index_range[1] += (end_faces_len - start_faces_len) * 3;
}

void pg_model_draw_static(struct pg_renderpass* pass, struct pg_model* model, mat4 tx)
{
    pg_renderpass_add_drawdata(pass, sizeof(tx), &tx);
    pg_renderpass_add_drawdata(pass, sizeof(model->index_range), model->index_range);
}

void pg_model_draw_rigged(struct pg_renderpass* pass, struct pg_model* model,
                          struct pg_armature_pose* pose, mat4 tx)
{
    pg_renderpass_add_drawdata(pass, sizeof(tx), &tx);
    pg_renderpass_add_drawdata(pass, sizeof(model->index_range), model->index_range);
    pg_renderpass_add_drawdata(pass, sizeof(int), &model->rig->n_bones);
    int i;
    for(i = 0; i < model->rig->n_bones; ++i) {
        mat4 bone_tx = mat4_translation(vec3_add(pose->bones[i].move, model->rig->bones[i].local_origin));
        bone_tx = mat4_mul_quat(bone_tx, pose->bones[i].rot);
        mat4 unbind = mat4_translation(vec3_negative(model->rig->bones[i].local_origin));
        bone_tx = mat4_mul(bone_tx, unbind);
        pg_renderpass_add_drawdata(pass, sizeof(bone_tx), &bone_tx);
    }
}

void pg_ezpass_static_model(struct pg_renderpass* pass, struct pg_rendertarget* target,
                            struct pg_vertex_buffer* vbuf, struct pg_texture* tex)
{
    pg_renderpass_init(pass, "pgm", PG_RENDERPASS_OPTS(
        PG_RENDERPASS_DEPTH_TEST | PG_RENDERPASS_DEPTH_WRITE,
        .depth_func = GL_LEQUAL ));
    pg_renderpass_target(pass, target);
    pg_renderpass_vertices(pass, vbuf);
    pg_renderpass_set_drawfunc(pass, drawfunc_static_model, 1, "pg_matrix_mvp");
    pg_renderpass_texture(pass, 0, tex, NULL);
}

void pg_ezpass_rigged_model(struct pg_renderpass* pass, struct pg_rendertarget* target,
                            struct pg_vertex_buffer* vbuf, struct pg_texture* tex)
{
    pg_renderpass_init(pass, "pgm_rigged", PG_RENDERPASS_OPTS(
        PG_RENDERPASS_DEPTH_TEST | PG_RENDERPASS_DEPTH_WRITE,
        .depth_func = GL_LEQUAL ));
    pg_renderpass_target(pass, target);
    pg_renderpass_vertices(pass, vbuf);
    pg_renderpass_set_drawfunc(pass, drawfunc_rigged_model, 2, "pg_matrix_mvp", "bone_mats");
    pg_renderpass_texture(pass, 0, tex, NULL);
}
