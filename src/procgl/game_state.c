#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "game_state.h"

void pg_game_state_init(struct pg_game_state* state, float start_time,
                        unsigned tick_rate, unsigned tick_max)
{
    state->ticks = 0;
    state->tick_rate = tick_rate;
    state->tick_max = tick_max;
    state->tick_secs = 1.0f / tick_rate;
    state->time_secs = 0;
    state->time_ticks_f = 0;
    state->time_secs_f = 0;
    state->tick_over = 0;
    state->secs_over = 0;
    state->running = 1;
    state->data = NULL;
    state->tick = NULL;
    state->draw = NULL;
}

void pg_game_state_deinit(struct pg_game_state* state)
{
    if(state->deinit) state->deinit(state->data);
}

void pg_game_state_update(struct pg_game_state* state, float new_time)
{
    float time_elapsed = new_time - state->time_secs;
    float time_consumed = 0;
    unsigned ticks_done = 0;
    while(state->running && time_consumed + state->tick_secs < time_elapsed) {
        /*  "Consume" the time in chunks of the intended tick-rate  */
        time_consumed += state->tick_secs;
        ++ticks_done;
        ++state->ticks;
        state->time_secs += state->tick_secs;
        /*  Do The Thing    */
        if(state->tick) state->tick(state);
        /*  Don't do too many ticks in a single call. If the tick takes longer
         *  than the allotted time, this gives the player a slideshow instead
         *  of nothing. Why not right?  */
        if(ticks_done >= state->tick_max) break;
    }
    state->secs_over = time_elapsed - time_consumed;
    state->tick_over = state->secs_over * state->tick_secs;
    state->time_secs_f = state->time_secs + state->secs_over;
    state->time_ticks_f = state->ticks + state->tick_over;
}

void pg_game_state_draw(struct pg_game_state* state)
{
    if(state->draw) state->draw(state);
}
