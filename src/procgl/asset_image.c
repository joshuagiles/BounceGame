#include "procgl/procgl.h"

/********************************/
/*  SINGLE-IMAGE ASSETS         */
/********************************/

/*  For fixing asset data when it is in a texture that requires padding */
static void asset_image_precalc_coords(struct pg_asset_image* asset)
{
    vec2 tex_sz = vec2(asset->tex->w, asset->tex->h);
    asset->uv_size = vec2_div(asset->img_size, tex_sz);
    /*  First do the grid cell dimensions   */
    if(!vec2_is_zero(asset->grid_px)) {
        /*  Grid defined in pixels  */
        asset->grid_uv = vec2_div(asset->grid_px, tex_sz);
        asset->grid_cells = vec2_div(asset->img_size, asset->grid_px);
    } else if(!vec2_is_zero(asset->grid_uv)) {
        /*  Grid defined in UV coordinates  */
        asset->grid_uv = vec2_mul(asset->grid_uv, asset->uv_size);
        asset->grid_px = vec2_mul(asset->img_size, asset->grid_uv);
        asset->grid_cells = vec2_div(vec2(1,1), asset->grid_uv);
    } else if(!vec2_is_zero(asset->grid_cells)) {
        /*  Grid defined by own dimensions  */
        asset->grid_uv = vec2_div(vec2(1,1), asset->grid_cells);
        asset->grid_px = vec2_div(asset->img_size, asset->grid_cells);
    } else {
        /*  Grid undefined  */
        asset->grid_uv = vec2(1,1);
        asset->grid_px = asset->img_size;
        asset->grid_cells = vec2(1,1);
    }
    /*  Next calculate the coordinates in all the frames    */
    int i;
    struct pg_asset_image_frame* f;
    ARR_FOREACH_PTR(asset->frames, f, i) {
        f->uv_raw.layer = asset->tex_layer;
        if(!vec2_is_zero(f->px[0]) || !vec2_is_zero(f->px[1])) {
            /*  Frame defined in pixels */
            f->uv_raw.frame[0] = vec2_div(f->px[0], tex_sz);
            f->uv_raw.frame[1] = vec2_div(f->px[1], tex_sz);
            f->cell[0] = vec2_div(f->px[0], asset->grid_px);
            f->cell[1] = vec2_div(f->px[0], asset->grid_px);
        } else if(!vec2_is_zero(f->uv_raw.frame[0]) || !vec2_is_zero(f->uv_raw.frame[1])) {
            /*  Frame defined as UV coordinates */
            f->uv_raw.frame[0] = vec2_mul(f->uv_raw.frame[0], asset->uv_size);
            f->uv_raw.frame[1] = vec2_mul(f->uv_raw.frame[1], asset->uv_size);
            f->px[0] = vec2_mul(f->uv_raw.frame[0], asset->img_size);
            f->px[1] = vec2_mul(f->uv_raw.frame[1], asset->img_size);
            f->cell[0] = vec2_mul(f->uv_raw.frame[0], asset->grid_uv);
            f->cell[1] = vec2_mul(f->uv_raw.frame[1], asset->grid_uv);
        } else if(!vec2_is_zero(f->cell[0]) || !vec2_is_zero(f->cell[1])) {
            /*  Frame defined in grid cells */
            f->uv_raw.frame[0] = vec2_mul(f->cell[0], asset->grid_uv);
            f->uv_raw.frame[1] = vec2_mul(f->cell[1], asset->grid_uv);
            f->px[0] = vec2_mul(f->cell[0], asset->grid_px);
            f->px[1] = vec2_mul(f->cell[1], asset->grid_px);
        } else {
            /*  Zero-size frame */
        }
    }
}

void pg_asset_image_init(struct pg_asset_image* asset)
{
    *asset = (struct pg_asset_image){};
    ARR_INIT(asset->frames);
    HTABLE_INIT(asset->frames_table, 8);
    ARR_INIT(asset->seqs);
    HTABLE_INIT(asset->seqs_table, 8);
}

/*  This is the function that traverses the JSON structure and fills out the asset */
void pg_asset_image_from_json(struct pg_asset_image* asset, cJSON* json)
{
    if(!cJSON_IsObject(json)) return;
    /********************************/
    /*  Read the basic image data   */
    json_get_child_string(json, "filename", asset->img_filename, 1024);
    cJSON* grid = cJSON_GetObjectItemCaseSensitive(json, "grid");
    if(grid && cJSON_IsObject(grid)) {
        cJSON* grid_coord = grid->child;
        const char* grid_base = grid_coord->string;
        /*  Unset grid values are zeroes; they'll be set later (after the
         *  texture is loaded)  */
        if(grid_base[0] == 'p' || grid_base[0] == 'i') {
            /*  'px', 'pix', 'pixel', 'img', 'image'    */
            json_get_vec2(grid_coord, &asset->grid_px);
        } else if(grid_base[0] == 't' || grid_base[1] == 'u') {
            /* 'tex', 'uv'  */
            json_get_vec2(grid_coord, &asset->grid_uv);
        } else if(grid_base[0] == 'c' || grid_base[1] == 'g') {
            /*  'cell', 'grid'  */
            json_get_vec2(grid_coord, &asset->grid_cells);
        }
    }
    /************************************/
    /*  Read all the frame definitions  */
    /*  Frame 0 is always the whole image, and has the name "image" */
    struct pg_asset_image_frame tmp_foobar, tmp_frame =
        { .name = "image",
          .uv_raw = pg_tex_frame(vec2(0,0), vec2(1,1), asset->tex_layer) };
    HTABLE_SET(asset->frames_table, "image", 0);
    ARR_PUSH(asset->frames, tmp_frame);
    /*  The rest of the frames are in the "frames" array, see above...  */
    cJSON* frames = cJSON_GetObjectItemCaseSensitive(json, "frames");
    if(frames) {
        cJSON* frame_item;
        cJSON_ArrayForEach(frame_item, frames) {
            if(!cJSON_IsObject(frame_item)) continue;
            /*  frame_item is `"frameA": { "px": ...` in `asset_image.json` */
            tmp_frame = (struct pg_asset_image_frame){};
            const char* frame_name = frame_item->string;
            /*  This is traversing the [[x0, y0], [x1, y1]] structure.
             *  coord_item is the first [, coord_item->child is the second,
             *  coord_item->child->child is x0,
             *  coord_item->child->next->child is x1    */
            cJSON* coord_item = frame_item->child;
            cJSON* arr_min = coord_item->child;
            cJSON* arr_max = coord_item->child->next;
            const char* frame_type = coord_item->string;
            /*  The unset values are zeroes; they will be calculated based on
             *  the non-zero values later (after the texture is loaded) */
            if(frame_type[0] == 'p' || frame_type[0] == 'i') {
                /*  'px', 'pix', 'pixel', 'img', 'image'    */
                json_get_vec2(arr_min, &tmp_frame.px[0]);
                json_get_vec2(arr_max, &tmp_frame.px[1]);
            } else if(frame_type[0] == 't' || frame_type[1] == 'u') {
                /* 'tex', 'uv'  */
                json_get_vec2(arr_min, &tmp_frame.uv_raw.frame[0]);
                json_get_vec2(arr_max, &tmp_frame.uv_raw.frame[1]);
            } else if(frame_type[0] == 'c' || frame_type[1] == 'g') {
                /*  'cell', 'grid'  */
                json_get_vec2(arr_min, &tmp_frame.cell[0]);
                json_get_vec2(arr_max, &tmp_frame.cell[1]);
            }
            /*  Add it to the array and to the hash table   */
            HTABLE_SET(asset->frames_table, frame_name, asset->frames.len);
            ARR_PUSH(asset->frames, tmp_frame);
        }
    }
    cJSON* foobar = cJSON_GetObjectItemCaseSensitive(json, "foobar");
    cJSON* foobar_item;
    cJSON_ArrayForEach(foobar_item, foobar) {
        cJSON* coord_item = foobar_item->child;
        cJSON* arr_min = coord_item->child;
        cJSON* arr_max = coord_item->child->next;
        const char* foobar_type = coord_item->string;
        /*  The unset values are zeroes; they will be calculated based on
         *  the non-zero values later (after the texture is loaded) */
        if(foobar_type[0] == 'p' || foobar_type[0] == 'i') {
            /*  'px', 'pix', 'pixel', 'img', 'image'    */
            json_get_vec2(arr_min, &tmp_foobar.px[0]);
            json_get_vec2(arr_max, &tmp_foobar.px[1]);
        } else if(foobar_type[0] == 't' || foobar_type[1] == 'u') {
            /* 'tex', 'uv'  */
            json_get_vec2(arr_min, &tmp_foobar.uv_raw.frame[0]);
            json_get_vec2(arr_max, &tmp_foobar.uv_raw.frame[1]);
        } else if(foobar_type[0] == 'c' || foobar_type[1] == 'g') {
            /*  'cell', 'grid'  */
            json_get_vec2(arr_min, &tmp_foobar.cell[0]);
            json_get_vec2(arr_max, &tmp_foobar.cell[1]);
        }
    }
    /****************************************/
    /*  Read all the sequence definitions   */
    cJSON* seqs = cJSON_GetObjectItemCaseSensitive(json, "sequences");
    if(seqs) {
        cJSON* seq_item;
        cJSON_ArrayForEach(seq_item, seqs) {
            if(!cJSON_IsObject(seq_item)) continue;
            /*  seq_item is `"loopA": { ...` in `asset_image.json`  */
            struct pg_asset_image_sequence tmp_seq = { .n_frames = 0 };
            float seq_duration = 0;
            strncpy(tmp_seq.name, seq_item->string, 32);
            const char* seq_name = seq_item->string;
            cJSON* seq_frame;
            cJSON_ArrayForEach(seq_frame, seq_item) {
                /*  seq_frame is `"frameA": 50,` in `asset_image.json`  */
                if(!cJSON_IsNumber(seq_frame)) continue;
                const char* frame_name = seq_frame->string;
                int frame_idx = 0;
                int frame_duration = seq_frame->valueint;
                tmp_seq.duration[tmp_seq.n_frames] = frame_duration;
                HTABLE_GET_V(asset->frames_table, frame_name, frame_idx);
                /*  frame_idx will still be 0 if the frame doesn't exist    */
                tmp_seq.frame[tmp_seq.n_frames] = frame_idx;
                /*  Set the frame start and update sequence duration based on
                 *  the frame duration in the config  */
                tmp_seq.start[tmp_seq.n_frames] = seq_duration;
                ++tmp_seq.n_frames;
                seq_duration += frame_duration;
            }
            tmp_seq.full_duration = seq_duration;
            /*  Add a full sequence to the array and table  */
            HTABLE_SET(asset->seqs_table, seq_name, asset->seqs.len);
            ARR_PUSH(asset->seqs, tmp_seq);
        }
    }
}

void pg_asset_image_from_yaml(struct pg_asset_image* asset, const char* filename)
{
    
}

/*  Initialize an asset, parse a JSON file, init the given texture object (or
 *  allocate one if tex==NULL) as a single-image texture for the asset. */
void pg_asset_image_load(struct pg_asset_image* asset, const char* filename,
                           struct pg_texture* tex, struct pg_texture_opts* opts)
{
    pg_asset_image_init(asset);
    strncpy(asset->cfg_filename, filename, 1024);
    cJSON* json = load_json(filename);
    if(json) pg_asset_image_from_json(asset, json);
    if(!tex) {
        /*  If no texture is given, we allocate one that will be freed with the asset   */
        asset->tex = malloc(sizeof(*asset->tex));
        asset->own_tex = 1;
        const char* filenames[1] = { asset->img_filename };
        pg_texture_from_files(asset->tex, 1, filenames, opts);
        asset_image_precalc_coords(asset);
    } else {
        /*  Assume the given texture will be initialized eventually (or already)  */
        asset->tex = tex;
        asset->own_tex = 0;
        if(opts) pg_texture_options(tex, opts);
    }
}

void pg_asset_image_deinit(struct pg_asset_image* asset)
{
    ARR_DEINIT(asset->frames);
    HTABLE_DEINIT(asset->frames_table);
    ARR_DEINIT(asset->seqs);
    HTABLE_DEINIT(asset->seqs_table);
    if(asset->own_tex) {
        pg_texture_deinit(asset->tex);
        free(asset->tex);
    }
}

/*  Get a raw texture frame by the name in the asset metadata   */
pg_tex_frame_t pg_asset_image_get_frame(struct pg_asset_image* asset, const char* frame_name)
{
    int frame_idx = 0;
    HTABLE_GET_V(asset->frames_table, frame_name, frame_idx);
    /*  frame_idx will still be 0 if frame does not exist   */
    struct pg_asset_image_frame* frame = &asset->frames.data[frame_idx];
    return frame->uv_raw;
}

/*  Get a raw texture frame for a grid cell based on asset metadata */
pg_tex_frame_t pg_asset_image_get_cell(struct pg_asset_image* asset, int x, int y)
{
    if(!asset) return pg_tex_frame(vec2(0,0), vec2(1,1), 0);
    vec2 cell_start = vec2_mul(vec2(x,y), asset->grid_uv);
    vec2 cell_end = vec2_add(cell_start, asset->grid_uv);
    return pg_tex_frame(cell_start, cell_end, asset->tex_layer);
}

/*  Get a sequence from the meta-data, to use with a pg_asset_imganim   */
struct pg_asset_image_sequence* pg_asset_image_get_sequence(
        struct pg_asset_image* asset, const char* seq_name)
{
    int seq_idx = 0;
    HTABLE_GET_V(asset->seqs_table, seq_name, seq_idx);
    /*  Returns sequence 0 on error */
    return &asset->seqs.data[seq_idx];
}

/****************************/
/*  IMAGE SETS              */
/****************************/

static void asset_imgset_precalc_coords(struct pg_asset_imgset* asset)
{
    int i;
    struct pg_asset_image* img_asset;
    ARR_FOREACH_PTR(asset->imgs, img_asset, i) {
        img_asset->tex = asset->tex;
        img_asset->tex_layer = i;
        asset_image_precalc_coords(img_asset);
    }
}

/*  This is the function that traverses the JSON structure and fills out the asset */
void pg_asset_imgset_from_json(struct pg_asset_imgset* asset, cJSON* json,
                               struct pg_texture* tex)
{
    /*  This is a lot simpler because it is just a pseudo-wrapper around the
     *  basic image asset   */
    if(!cJSON_IsObject(json)) return;
    int tex_layer = 0;
    cJSON* member_item;
    cJSON_ArrayForEach(member_item, json) {
        /*  member_item is `"exampleA": ...` in `asset_imgset.json` */
        struct pg_asset_image img_asset = {};
        if(cJSON_IsObject(member_item)) {
            /*  It is a full image asset in the image-set file  */
            pg_asset_image_init(&img_asset);
            pg_asset_image_from_json(&img_asset, member_item);
        } else if(cJSON_IsString(member_item)) {
            /*  It is a filename for image metadata in another file */
            pg_asset_image_load(&img_asset, member_item->valuestring, tex, NULL);
        } else {
            pg_asset_image_init(&img_asset);
        }
        HTABLE_SET(asset->imgs_table, member_item->string, asset->imgs.len);
        ARR_PUSH(asset->imgs, img_asset);
    }
}

/*  Initialize an asset, parse a JSON file, init the given texture object (or
 *  allocate one if tex==NULL) with all the images in the set as layers */
void pg_asset_imgset_load(struct pg_asset_imgset* asset, const char* filename,
                          struct pg_texture* tex, struct pg_texture_opts* opts)
{
    *asset = (struct pg_asset_imgset){};
    ARR_INIT(asset->imgs);
    HTABLE_INIT(asset->imgs_table, 4);
    strncpy(asset->cfg_filename, filename, 1024);
    cJSON* json = load_json(filename);
    if(!json) {
        printf("Error! Incorrect JSON in %s\n", filename);
        return;
    }
    pg_asset_imgset_from_json(asset, json, tex);
    if(!tex) {
        asset->tex = malloc(sizeof(*asset->tex));
        asset->own_tex = 1;
    } else {
        asset->tex = tex;
        asset->own_tex = 0;
    }
    /*  Get pointers to all the filenames and load all the files into one
     *  texture */
    const char* tex_files[asset->imgs.len];
    int i;
    for(i = 0; i < asset->imgs.len; ++i) {
        tex_files[i] = asset->imgs.data[i].img_filename;
    }
    pg_texture_from_files(asset->tex, asset->imgs.len, tex_files, opts);
    /*  Now that the images are loaded, image frames can all be calculated
     *  correctly.  */
    asset_imgset_precalc_coords(asset);
}

void pg_asset_imgset_deinit(struct pg_asset_imgset* asset)
{
    int i;
    struct pg_asset_image* img_asset;
    ARR_FOREACH_PTR(asset->imgs, img_asset, i) {
        pg_asset_image_deinit(img_asset);
    }
    ARR_DEINIT(asset->imgs);
    HTABLE_DEINIT(asset->imgs_table);
    if(asset->own_tex) {
        pg_texture_deinit(asset->tex);
        free(asset->tex);
    }
}

struct pg_asset_image* pg_asset_imgset_get_image(struct pg_asset_imgset* asset, const char* image_name)
{
    int img_idx = 0;
    HTABLE_GET_V(asset->imgs_table, image_name, img_idx);
    /*  img_idx will be 0 if the image name doesn't exist   */
    return &asset->imgs.data[img_idx];
}

pg_tex_frame_t pg_asset_imgset_get_frame(struct pg_asset_imgset* asset,
            const char* image_name, const char* frame_name)
{
    struct pg_asset_image* img_asset = pg_asset_imgset_get_image(asset, image_name);
    return pg_asset_image_get_frame(img_asset, frame_name);
}

struct pg_asset_image_sequence* pg_asset_imgset_get_sequence(
        struct pg_asset_imgset* asset, const char* image_name, const char* seq_name)
{
    struct pg_asset_image* img_asset = pg_asset_imgset_get_image(asset, image_name);
    return pg_asset_image_get_sequence(img_asset, seq_name);
}

pg_tex_frame_t pg_asset_imgset_get_cell(struct pg_asset_imgset* asset,
                                        const char* image_name, int x, int y)
{
    if(!asset) return pg_tex_frame(vec2(0,0), vec2(1,1), 0);
    struct pg_asset_image* img_asset = pg_asset_imgset_get_image(asset, image_name);
    return pg_asset_image_get_cell(img_asset, x, y);
}

/********************/
/*  Image steppers  */
void pg_asset_image_set_stepper(struct pg_asset_image* asset,
                                  struct pg_asset_image_stepper* stepper,
                                  const char* seq_name, uint32_t opts)
{
    stepper->img_idx = 0;
    int seq_idx = 0;
    if((opts & PG_IMAGE_STEPPER_SINGLE)) {
        HTABLE_GET_V(asset->frames_table, seq_name, seq_idx);
    } else {
        HTABLE_GET_V(asset->seqs_table, seq_name, seq_idx);
        if((opts & PG_IMAGE_STEPPER_NORESET)) {
            if((stepper->opts == (opts & (~PG_IMAGE_STEPPER_NORESET)))
            && seq_idx == stepper->seq_idx) return;
        }
    }
    stepper->seq_idx = seq_idx;
    stepper->cur_step = 0;
    stepper->seq_progress = 0;
    stepper->opts = opts & (~PG_IMAGE_STEPPER_NORESET);
}

void pg_asset_imgset_set_stepper(struct pg_asset_imgset* asset,
                                  struct pg_asset_image_stepper* stepper,
                                  const char* img_name,
                                  const char* seq_name, uint32_t opts)
{
    int img_idx = 0, seq_idx = 0;
    HTABLE_GET_V(asset->imgs_table, img_name, img_idx);
    struct pg_asset_image* img_asset = &asset->imgs.data[stepper->img_idx];
    if((opts & PG_IMAGE_STEPPER_SINGLE)) {
        HTABLE_GET_V(img_asset->frames_table, seq_name, seq_idx);
    } else {
        HTABLE_GET_V(img_asset->seqs_table, seq_name, seq_idx);
        if((opts & PG_IMAGE_STEPPER_NORESET)) {
            if((stepper->opts == (opts & (~PG_IMAGE_STEPPER_NORESET)))
            && seq_idx == stepper->seq_idx && img_idx == stepper->img_idx) return;
        }
    }
    stepper->seq_idx = seq_idx;
    stepper->img_idx = img_idx;
    stepper->cur_step = 0;
    stepper->seq_progress = 0;
    stepper->opts = opts & (~PG_IMAGE_STEPPER_NORESET);
}

void pg_asset_image_step(struct pg_asset_image* asset,
                         struct pg_asset_image_stepper* stepper, float step)
{
    int new_frame_idx = stepper->cur_step;
    if(stepper->opts & PG_IMAGE_STEPPER_SINGLE) {
        new_frame_idx = stepper->seq_idx;
    } else {
        struct pg_asset_image_sequence* seq;
        if(stepper->cur_step >= 32) return;
        if(stepper->seq_idx >= asset->seqs.len) {
            seq = &asset->seqs.data[0];
        } else {
            seq = &asset->seqs.data[stepper->seq_idx];
        }
        int time = stepper->seq_progress + step;
        if(time >= seq->full_duration) time = LM_FMOD(time, seq->full_duration);
        if(time < stepper->seq_progress) stepper->cur_step = 0;
        while(stepper->cur_step+1 < seq->n_frames && seq->start[stepper->cur_step+1] < time) {
            ++stepper->cur_step;
        }
        new_frame_idx = seq->frame[stepper->cur_step];
    }
    stepper->seq_progress += step;
    stepper->uv = asset->frames.data[new_frame_idx].uv_raw;
    stepper->uv = pg_tex_frame_flip(&stepper->uv,
        (stepper->opts & PG_IMAGE_STEPPER_FLIP_X),
        (stepper->opts & PG_IMAGE_STEPPER_FLIP_Y));
}

void pg_asset_imgset_step(struct pg_asset_imgset* asset,
                          struct pg_asset_image_stepper* stepper, float step)
{
    struct pg_asset_image* img_asset = &asset->imgs.data[stepper->img_idx];
    pg_asset_image_step(img_asset, stepper, step);
}

void pg_asset_image_stepper_set(struct pg_asset_image_stepper* stepper,
                                struct pg_asset_image* asset, float time)
{
    struct pg_asset_image_sequence* seq = &asset->seqs.data[stepper->seq_idx];
    if(time >= seq->full_duration) time = LM_FMOD(time, seq->full_duration);
    stepper->seq_progress = time;
    stepper->cur_step = 0;
    while(seq->start[stepper->cur_step+1] < time) ++stepper->cur_step;
    int frame_idx = seq->frame[stepper->cur_step];
    stepper->uv = asset->frames.data[frame_idx].uv_raw;
}

void pg_asset_imgset_stepper_set(struct pg_asset_image_stepper* stepper,
                                 struct pg_asset_imgset* asset, float time)
{
    struct pg_asset_image* img_asset = &asset->imgs.data[stepper->img_idx];
    pg_asset_image_stepper_set(stepper, img_asset, time);
}
