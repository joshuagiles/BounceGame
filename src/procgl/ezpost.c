#include "procgl.h"

/************************************/
/*  SINE EZPOST                     */
/************************************/

/*  Sine-wavy distortion    */
struct drawdata_sine {
    float frequency;
    float phase;
    float amplitude;
    vec2 axis;
};

static size_t drawfunc_sine(const void* draw, const struct pg_shader* shader,
                            const mat4* mats, const GLint* idx)
{
    const struct drawdata_sine* sine = draw;
    /*  Frequency, phase, amplitude */
    glUniform3f(idx[0], sine->frequency, sine->phase, sine->amplitude);
    /*  Axis    */
    glUniform2f(idx[1], VEC_XY(sine->axis));
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    return sizeof(struct drawdata_sine);
}

void pg_ezpost_sine(struct pg_renderpass* pass, struct pg_rendertarget* target)
{
    pg_renderpass_init(pass, "post_sine", PG_RENDERPASS_OPTS(
        .flags = PG_RENDERPASS_SWAP_PER_DRAW));
    pg_renderpass_target(pass, target);
    pg_renderpass_fbtexture(pass, 0, target, 0);
    pg_renderpass_set_drawfunc(pass, drawfunc_sine, 2, "wave", "axis");
}

void pg_ezpost_sine_add(struct pg_renderpass* pass, vec2 axis,
                        float frequency, float phase, float amplitude)
{
    struct drawdata_sine drawdata = {
        .frequency = frequency,
        .phase = phase,
        .amplitude = amplitude,
        .axis = axis
    };
    pg_renderpass_add_drawdata(pass, sizeof(drawdata), &drawdata);
}

/************************************/
/*  BLUR EZPOST                     */
/************************************/

static size_t drawfunc_blur(const void* drawdata, const struct pg_shader* shader,
                         const mat4* mats, const GLint* idx)
{
    const vec2* blur_dir = drawdata;
    glUniform2f(idx[0], VEC_XY(*blur_dir));
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    return sizeof(*blur_dir);
}

void pg_ezpost_blur(struct pg_renderpass* pass, struct pg_rendertarget* target)
{
    pg_renderpass_init(pass, "post_blur", PG_RENDERPASS_OPTS(
        .flags = PG_RENDERPASS_SWAP_PER_DRAW));
    pg_renderpass_target(pass, target);
    pg_renderpass_fbtexture(pass, 0, target, 0);
    pg_renderpass_set_drawfunc(pass, drawfunc_blur, 1, "blur_dir");
}

void pg_ezpost_blur_add(struct pg_renderpass* pass, vec2 axis0, vec2 axis1)
{
    if(!vec2_is_zero(axis0)) {
        pg_renderpass_add_drawdata(pass, sizeof(axis0), &axis0);
    }
    if(!vec2_is_zero(axis1)) {
        pg_renderpass_add_drawdata(pass, sizeof(axis1), &axis1);
    }
}

/************************************/
/*  FOG EZPOST                      */
/************************************/

struct drawdata_fog {
    vec3 color;
    float near, far;
};

static size_t drawfunc_fog(const void* drawdata, const struct pg_shader* shader,
                         const mat4* mats, const GLint* idx)
{
    const struct drawdata_fog* fog = drawdata;
    glUniform3f(idx[0], VEC_XYZ(fog->color));
    glUniform2f(idx[1], fog->near, fog->far);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    return sizeof(*fog);
}

void pg_ezpost_fog(struct pg_renderpass* pass, struct pg_rendertarget* target)
{
    pg_renderpass_init(pass, "post_fog", PG_RENDERPASS_OPTS(
        .flags = PG_RENDERPASS_SWAP_BEFORE));
    pg_renderpass_target(pass, target);
    pg_renderpass_fbtexture(pass, 0, target, 0);
    pg_renderpass_fbtexture(pass, 1, target, 1);
    pg_renderpass_set_drawfunc(pass, drawfunc_fog, 2, "fog_color", "fog_plane");
}

void pg_ezpost_fog_add(struct pg_renderpass* pass, vec3 color, float near, float far)
{
    struct drawdata_fog drawdata = {
        .color = color, .near = near, .far = far,
    };
    pg_renderpass_add_drawdata(pass, sizeof(drawdata), &drawdata);
}

/************************************/
/*  SCREEN EZPOST                   */
/************************************/

static size_t drawfunc_screen(const void* drawdata, const struct pg_shader* shader,
                           const mat4* mats, const GLint* idx)
{
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    return 1;
}

void pg_ezpost_screen(struct pg_renderpass* pass, struct pg_rendertarget* src)
{
    pg_renderpass_init(pass, "passthru", PG_RENDERPASS_OPTS());
    pg_renderpass_target(pass, NULL);
    pg_renderpass_fbtexture(pass, 0, src, 0);
    pg_renderpass_set_drawfunc(pass, drawfunc_screen, 0);
    char dummy = 0;
    pg_renderpass_add_drawdata(pass, 1, &dummy);
}


