/****************************************/
/*  Unified sprite/text batching shader */
/****************************************/

/*  Various draw structures, they all ultimately end up as pg_ezquad(s) though  */
struct pg_draw_2d {
    pg_tex_frame_t frame;
    vec3 pos;
    vec2 scale;
    float rotation;
    vec4 color_mod, color_add;
};

struct pg_draw_text {
    /*  Can take a font, formatter (optional), and string, to format the
        string on the fly   */
    struct pg_font* font;
    struct pg_text_formatter* formatter;
    wchar_t* str;
    char* str_c;
    int len;
    /*  Or take already-formatted text  */
    struct pg_text_form* form;
    /*  Transforms done after formatting    */
    vec4 color;
    vec2 pos, scale;
    float rotation;
    vec2 anchor;
};

struct pg_ezquad {
    vec3 pos;
    vec2 scale;
    uint32_t color_add;
    uint32_t color_mod;
    uint16_t tex_layer, tex_select;
    vec4 tex_frame;
    quat orientation;
};

struct pg_quadbatch_stencil {
    int test, write_mask;
    int func, ref, func_mask;
    int sfail, dpfail, dppass;
};

#define PG_EZDRAW_2D(...) \
    (&(struct pg_draw_2d){ \
        .frame = { { {{ 0, 0 }}, {{ 1, 1 }} }, 0 }, \
        .pos = {{ 0, 0, 0 }}, .scale = {{ 1, 1 }}, .rotation = 0, \
        .color_mod = {{ 1, 1, 1, 1 }}, .color_add = {{ 0, 0, 0, 0 }}, \
        __VA_ARGS__ })

#define PG_EZDRAW_2D_RECT(color, ...) \
    (&(struct pg_draw_2d){ \
        .frame = { { {{ 0, 0 }}, {{ 1, 1 }} }, 0 }, \
        .pos = {{ 0, 0 }}, .scale = {{ 1, 1 }}, .rotation = 0, \
        .color_mod = {{ 0, 0, 0, 0 }}, .color_add = color, \
        __VA_ARGS__ })

#define PG_EZDRAW_TEXT(...) \
    (&(struct pg_draw_text){ \
        .pos = {{ 0, 0 }}, .scale = {{ 1, 1 }}, .rotation = 0, \
        .color = {{ 1, 1, 1, 1 }}, __VA_ARGS__ })

#define PG_QUADBATCH_STENCIL(...) &(struct pg_quadbatch_stencil){ \
        .test = 0, .write_mask = 0x00, \
        .func = GL_NEVER, .ref = 0, .func_mask = 0xFF, \
        .sfail = GL_KEEP, .dpfail = GL_KEEP, .dppass = GL_KEEP, \
        __VA_ARGS__ }

#define PG_EZQUAD(...) \
    (&(struct pg_ezquad){ \
        .scale = {{ 1, 1 }}, .orientation = {{ 0, 0, 0, 1 }}, \
        .color_mod = 0xFFFFFFFF, __VA_ARGS__ })

struct pg_quadbatch {
    struct pg_buffertex buf;
    mat4 cur_transform;
    uint32_t cur_start;
    uint32_t cur_idx;
};

void pg_quadbatch_init(struct pg_quadbatch* batch, int size);
/*  Build a renderpass to use the batched data  */
void pg_quadbatch_init_pass(struct pg_quadbatch* batch,
                            struct pg_renderpass* pass, struct pg_rendertarget* target);
void pg_quadbatch_pass_texture(struct pg_renderpass* pass,
            struct pg_texture* tex0, struct pg_texture_opts* opts0,
            struct pg_texture* tex1, struct pg_texture_opts* opts1);
void pg_quadbatch_deinit(struct pg_quadbatch* batch);
/*  Add sprites or text to a current batch group    */
void pg_quadbatch_add_quad(struct pg_quadbatch* batch, struct pg_ezquad* quad);
void pg_quadbatch_add_sprite(struct pg_quadbatch* batch, struct pg_draw_2d* draw);
void pg_quadbatch_add_text(struct pg_quadbatch* batch, struct pg_draw_text* draw);
/*  Emit a draw operation to a renderpass for the current batch group   */
void pg_quadbatch_stencil(struct pg_quadbatch* batch, struct pg_renderpass* pass,
                          struct pg_quadbatch_stencil* stencil);
void pg_quadbatch_draw(struct pg_quadbatch* batch, struct pg_renderpass* pass);
/*  Begin a new batch group with a given model matrix   */
void pg_quadbatch_next(struct pg_quadbatch* batch, mat4* transform);
/*  Upload all the batch groups to the GPU so it can be read at draw time   */
void pg_quadbatch_buffer(struct pg_quadbatch* batch);
/*  Restart batching    */
void pg_quadbatch_reset(struct pg_quadbatch* batch);

