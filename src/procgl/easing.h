/*  Adapted from AHeasing, originally written by Warren Moore   */

#ifndef AH_EASING_H
#define AH_EASING_H

typedef float (*pg_easing_func_t)(float);

// Linear interpolation (no easing)
#define pg_ease_in_lerp pg_ease_lerp
#define pg_ease_out_lerp pg_ease_lerp
#define pg_ease_inout_lerp pg_ease_lerp
float pg_ease_lerp(float p);

// Instant stepping
float pg_ease_in_step(float p);
float pg_ease_out_step(float p);
float pg_ease_inout_step(float p);

// Quadratic easing; p^2
float pg_ease_in_quadratic(float p);
float pg_ease_out_quadratic(float p);
float pg_ease_inout_quadratic(float p);

// Cubic easing; p^3
float pg_ease_in_cubic(float p);
float pg_ease_out_cubic(float p);
float pg_ease_inout_cubic(float p);

// Quartic easing; p^4
float pg_ease_in_quartic(float p);
float pg_ease_out_quartic(float p);
float pg_ease_inout_quartic(float p);

// Quintic easing; p^5
float pg_ease_in_quintic(float p);
float pg_ease_out_quintic(float p);
float pg_ease_inout_quintic(float p);

// Sine wave easing; sin(p * PI/2)
float pg_ease_in_sine(float p);
float pg_ease_out_sine(float p);
float pg_ease_inout_sine(float p);

// Circular easing; sqrt(1 - p^2)
float pg_ease_in_circular(float p);
float pg_ease_out_circular(float p);
float pg_ease_inout_circular(float p);

// Exponential easing, base 2
float pg_ease_in_exponential(float p);
float pg_ease_out_exponential(float p);
float pg_ease_inout_exponential(float p);

// Exponentially-damped sine wave easing
float pg_ease_in_elastic(float p);
float pg_ease_out_elastic(float p);
float pg_ease_inout_elastic(float p);

// Overshooting cubic easing; 
float pg_ease_in_back(float p);
float pg_ease_out_back(float p);
float pg_ease_inout_back(float p);

// Exponentially-decaying bounce easing
float pg_ease_in_bounce(float p);
float pg_ease_out_bounce(float p);
float pg_ease_inout_bounce(float p);

#endif

