#include <stdio.h>
#include <time.h>
#include "procgl/procgl.h"
#include "game/bouncegame.h"
#include "handle_crash.h"

//#include <fenv.h>

int main(int argc, char *argv[])
{
    //feenableexcept(FE_ALL_EXCEPT & ~FE_INEXACT);
    program_name = argv[0];
    set_signal_handler();
    /*  Read the options file   */
    struct bg_user_options config;
    bg_user_options_read_or_create(&config, "./bouncegame.conf");
    /*  Init procgame   */
    pg_log_open("./bouncegame.log");
    pg_log(PG_LOG_DEBUG, "Fuck the %s!", "police");
    if(config.fullscreen) {
        pg_init(config.fullscreen_res.x, config.fullscreen_res.y, 1, "procgame");
    } else {
        pg_init(config.windowed_res.x, config.windowed_res.y, 0, "procgame");
    }
    glEnable(GL_CULL_FACE);
    srand(time(0));
    /*  Init game   */
    struct pg_game_state game;
    bouncegame_start(&game);
    /*  Main loop   */
    while(game.running) {
        float time = pg_time();
        pg_calc_framerate(time);
        pg_game_state_update(&game, time);
        pg_game_state_draw(&game);
        pg_window_swap();
    }
    /*  Clean it all up */
    pg_game_state_deinit(&game);
    pg_deinit();
    pg_log_close();
    return 0;
}
