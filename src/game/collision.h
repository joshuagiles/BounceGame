struct bg_impulse {
    vec2 push;
    vec2 push_norm;
    float push_len;
};

struct bg_collision {
    int did_collide;
    float sep_len;
    vec2 sep_axis;
    vec2 sep_norm;
    vec2 surf_norm;
    float impulse[2];
    int debug;
};

#define BG_COLLIDER_STATIC      (1 << 0)
#define BG_COLLIDER_WEAK        (1 << 1)
#define BG_COLLIDER_DEBUGGED    (1 << 2)
#define BG_COLLIDER_ON_SURFACE  (1 << 3)
#define BG_COLLIDER_PROCESSED   (1 << 4)

#define BG_COLLIDER_PRIM_NONE \
    (struct bg_collider_prim){ .type = BG_NO_COLLIDER } 
#define BG_COLLIDER_PRIM_CIRCLE(c, r) \
    (struct bg_collider_prim){ .type = BG_COLLIDER_CIRCLE, .ctr = (c), .radius = (r) }
#define BG_COLLIDER_PRIM_AABB(a, b) \
    (struct bg_collider_prim){ .type = BG_COLLIDER_AABB, .box0 = (a), .box1 = (b) }
#define BG_COLLIDER_PRIM_TILEGRID(f, p) \
    (struct bg_collider_prim){ .type = BG_COLLIDER_TILEGRID, \
        .tilegrid_fn = (f), .tilegrid_ptr = (p) }

#define BG_COLLIDER_NONE \
    (struct bg_collider){ .collider = { .type = BG_NO_COLLIDER } } 
#define BG_COLLIDER_CIRCLE(c, r) \
    (struct bg_collider){ .collider = { .type = BG_COLLIDER_CIRCLE, \
        .ctr = (c), .radius = (r) } }
#define BG_COLLIDER_AABB(a, b) \
    (struct bg_collider){ .collider = { .type = BG_COLLIDER_AABB, \
        .box0 = (a), .box1 = (b) } }
#define BG_COLLIDER_TILEGRID(f, p) \
    (struct bg_collider){ .collider = { .type = BG_COLLIDER_TILEGRID, \
        .tilegrid_fn = (f), .tilegrid_ptr = (p) } }

struct bg_collider_prim {
    enum bg_collider_type {
        BG_NO_COLLIDER,
        BG_COLLIDER_CIRCLE,
        BG_COLLIDER_AABB,
        BG_COLLIDER_TILEGRID,
        BG_NUM_COLLIDER_TYPES,
    } type;
    union {
        struct { vec2 ctr; float radius; };
        struct { vec2 box0, box1; };
        struct {
            void (*tilegrid_fn)(struct bg_collider_prim*, int, int, void*);
            void* tilegrid_ptr;
        };
    };
};

struct bg_collider {
    uint32_t flags;
    float mass;
    vec2 pos, vel, vel_norm;
    struct bg_impulse imp;
    struct bg_collider_prim prim;       /*  Local-space primitive   */
    struct bg_collider_prim prim_ws;    /*  World-space primitive   */
};

const char* BG_COLLIDER_NAME[BG_NUM_COLLIDER_TYPES];

/********************************/
/*  Core collision functions    */
/*  These take primitives in world-space!   */
typedef void (*bg_collide_func_t)(struct bg_collision*,
                    struct bg_collider_prim*, struct bg_collider_prim*);
void bg_collide_circle_circle(struct bg_collision* out,
                struct bg_collider_prim* prim0, struct bg_collider_prim* prim1);
void bg_collide_circle_aabb(struct bg_collision* out,
                struct bg_collider_prim* prim0, struct bg_collider_prim* prim1);
void bg_collide_aabb_aabb(struct bg_collision* out,
                struct bg_collider_prim* prim0, struct bg_collider_prim* prim1);

/****************************/
/*  Basic impulse functions */
void bg_impulse_add(struct bg_impulse* out,
                    struct bg_impulse* imp0, struct bg_impulse* imp1);
int bg_impulse_cmp(struct bg_impulse* imp0, struct bg_impulse* imp1);
vec2 bg_impulse_response(struct bg_impulse* imp, vec2 vel, float absorb);

/****************/
/*  Colliders   */
/*  Basic collider functions    */
void bg_collider_init(struct bg_collider* c0);
void bg_collider_set_pos(struct bg_collider* c0, vec2 pos);
void bg_collider_set_vel(struct bg_collider* c0, vec2 vel);
void bg_collider_set(struct bg_collider* c0, vec2 pos, vec2 vel);
void bg_collider_set_prim(struct bg_collider* c0, struct bg_collider_prim* prim);
void bg_collider_prim_tx(struct bg_collider_prim* out,
                         struct bg_collider_prim* prim, vec2 move);
/*  Collider impulses   */
void bg_collider_do_impulse(struct bg_collider* c0, float absorb);
void bg_collider_set_impulse(struct bg_collider* c0, struct bg_impulse* imp);
void bg_collider_set_impulse_gt(struct bg_collider* c0, struct bg_impulse* imp);
void bg_collider_add_impulse(struct bg_collider* c0, struct bg_impulse* imp);
/*  Collider responses  */
vec2 bg_collider_response(struct bg_collider* c0, float absorb);

/****************/
/*  Collisions  */
void bg_collision_prims(struct bg_collision* out,
                        struct bg_collider_prim* prim0, struct bg_collider_prim* prim1);
void bg_collision_check(struct bg_collision* out,
                        struct bg_collider* c0, struct bg_collider* c1);
/*  Generating impulses from a collision    */
void bg_collision_impulse(struct bg_collision* coll,
                          struct bg_impulse* imp0, struct bg_impulse* imp1);
void bg_collision_impulse_gt(struct bg_collision* coll,
                             struct bg_impulse* imp0, struct bg_impulse* imp1);
void bg_collision_impulse_add(struct bg_collision* coll,
                              struct bg_impulse* imp0, struct bg_impulse* imp1);

/*  Debugging collisions    */
void bg_debug_print_primitive(struct bg_collider_prim* prim0, char* name);
void bg_debug_print_collider(struct bg_collider* c0, char* name0);
void bg_debug_print_collision(struct bg_collision* coll,
                              struct bg_collider* c0, char* name0,
                              struct bg_collider* c1, char* name1);
void bg_debug_print_prim_collision(struct bg_collision* coll,
                                   struct bg_collider_prim* c0, char* name0,
                                   struct bg_collider_prim* c1, char* name1);
