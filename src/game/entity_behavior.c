#include "procgl/procgl.h"
#include "bouncegame.h"

/*  Basic entity "behaviors" should go here. Things like... being affected by
    gravity, being on fire, having other status effects, etc. Should be called
    once per tick per entity with each behavior.    */

void bg_entity_behavior_velocity(struct bg_gameplay_state* g, struct bg_entity* ent, vec2 slowdown)
{
    int ballistic = (ent->flags & BG_ENTITY_FLAG_BALLISTIC);
    int ground = (ent->flags & BG_ENTITY_FLAG_GROUND);
    if(ballistic && ground && ent->vel.y <= 0) ent->flags &= ~BG_ENTITY_FLAG_BALLISTIC;
    if(vec2_len2(ent->vel) >= 0.5*0.5) {
        ent->flags |= BG_ENTITY_FLAG_BALLISTIC;
        ballistic = 1;
    }
    if(!(ent->flags & BG_ENTITY_FLAG_BALLISTIC)) {
        ent->vel = vec2_mul(ent->vel, vec2(0.5, 1));
    }
    ent->pos = vec2_add(ent->pos, ent->vel);
}

void bg_entity_behavior_gravity(struct bg_gameplay_state* g, struct bg_entity* ent)
{
    ent->vel.y -= 0.015;
    ent->vel = vec2_vclamp(ent->vel, -0.5, 0.5);
}

void bg_entity_behavior_collide_level(struct bg_gameplay_state* g, struct bg_entity* ent)
{
    float bounce_factor = 1.25f;
    int on_surface = 0;
    int ballistic = (ent->flags & BG_ENTITY_FLAG_BALLISTIC);
    struct bg_impulse imp = {};
    bg_entity_sync_to_collider(ent);
    struct bg_collision coll = { .debug = !!(ent->flags & BG_ENTITY_FLAG_DEBUG) };
    bg_collision_check(&coll, &ent->collider, &g->cur_level.collider);
    if(coll.did_collide && (ent->flags & BG_ENTITY_FLAG_DEBUG)) {
        bg_debug_print_collision(&coll, &ent->collider, "Player", &g->cur_level.collider, "Level");
    }
    bg_collision_impulse(&coll, &ent->collider.imp, NULL);
    imp = ent->collider.imp;
    if(ent->collider.imp.push_norm.y > 0.5) on_surface = 1;
    bg_collider_do_impulse(&ent->collider, 0);

    bg_collision_check(&coll, &ent->collider, &g->cur_level.collider);
    if(coll.did_collide && (ent->flags & BG_ENTITY_FLAG_DEBUG)) {
        bg_debug_print_collision(&coll, &ent->collider, "Player", &g->cur_level.collider, "Level");
    }
    bg_collision_impulse(&coll, &ent->collider.imp, NULL);
    bg_impulse_add(&imp, &imp, &ent->collider.imp);
    if(ent->collider.imp.push_norm.y > 0.5) on_surface = 1;
    bg_collider_do_impulse(&ent->collider, 0);

    float vel_len = vec2_len(ent->vel);
    bg_entity_impulse(ent, &imp, (ballistic && vel_len > 0.2) ? 1.5 : 1.0);
    bg_entity_sync_to_collider(ent);

    if(on_surface) {
        ent->flags |= BG_ENTITY_FLAG_GROUND;
    }
}
