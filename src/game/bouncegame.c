#include <stdlib.h>
#include <limits.h>
#include "procgl/procgl.h"
#include "bouncegame.h"

static void bouncegame_update(struct pg_game_state*);
static void bouncegame_tick(struct pg_game_state*);
static void bouncegame_draw(struct pg_game_state*);
static void bouncegame_deinit(void*);

void bouncegame_start(struct pg_game_state* state)
{
    /*  Create a game state structure (contains actual game data)   */
    struct bouncegame_core* bg = malloc(sizeof(*bg));
    *bg = (struct bouncegame_core){};

    /*  Create the procgame state object (handles timing, calls the game interface) */
    pg_game_state_init(state, pg_time(), 60, 3);
    state->data = bg;
    state->tick = bouncegame_tick;
    //state->draw = bouncegame_draw;
    state->deinit = bouncegame_deinit;

    /*  Setup a render target   */
    vec2 win_sz = vec2(384,384);
    bg->ar = win_sz.x / win_sz.y;
    bg->res = vec2_scale(win_sz, 1);
    bg->halfres = vec2_scale(bg->res, 0.5);
    pg_viewer_ortho(&bg->view, vec2(), vec2(0.25,0.25), 0, bg->res);

    /*  Load the image assets   */
    pg_asset_imgset_load(&bg->imgs, "res/images.json", NULL,
        PG_TEXTURE_OPTS(.filter_min = GL_NEAREST, .filter_mag = GL_NEAREST,
                        .wrap_x = GL_CLAMP_TO_EDGE, .wrap_y = GL_CLAMP_TO_EDGE));

    /*  Deciding what proportion of the screen each font should be rendered
     *  (to the font texture buffer). Regular text font can be smallish, but
     *  "headline" fonts might need to be bigger because they will be drawn
     *  larger in-game. */
    char font_tex_size[16];
    snprintf(font_tex_size, 16, ":%d,%d", (int)win_sz.y, (int)win_sz.y);
    const char* tex_files[3] = { font_tex_size, font_tex_size, font_tex_size };
    pg_texture_from_files(&bg->font_tex, 3, tex_files,
        PG_TEXTURE_OPTS(.filter_min = GL_NEAREST, .filter_mag = GL_NEAREST,
                        .wrap_x = GL_CLAMP_TO_EDGE, .wrap_y = GL_CLAMP_TO_EDGE));
    int font_pixels = win_sz.y;
    if(win_sz.y < 400) {
        pg_font_init(&bg->bgfont[0], "res/Orbitron-Bold.ttf", &bg->font_tex, 0, win_sz.y*0.0625, 8);
    } else {
        pg_font_init(&bg->bgfont[0], "res/Orbitron-Regular.ttf", &bg->font_tex, 0, win_sz.y*0.0625, 8);
    }
    pg_font_init(&bg->bgfont[1], "res/NASDAQER.ttf", &bg->font_tex, 1, win_sz.y*0.125, 4);
    pg_font_init(&bg->bgfont[2], "res/Caveat-Bold.ttf", &bg->font_tex, 2, win_sz.y*0.125, 4);
    bg->bgfont[1].line_move *= 0.55;
    pg_texture_buffer(&bg->font_tex);

    /*  Initialize procgame renderer  */
    pg_renderer_init(&bg->rend);
    
    /*  Make the render textures    */
    pg_texture_init(&bg->pptex[0], PG_UBVEC4, win_sz.x, win_sz.y, 1, PG_TEXTURE_OPTS(
        .filter_mag = GL_NEAREST,
        .wrap_x = GL_CLAMP_TO_EDGE, .wrap_y = GL_CLAMP_TO_EDGE));
    pg_texture_init(&bg->pptex[1], PG_UBVEC4, win_sz.x, win_sz.y, 1, PG_TEXTURE_OPTS(
        .filter_mag = GL_NEAREST,
        .wrap_x = GL_CLAMP_TO_EDGE, .wrap_y = GL_CLAMP_TO_EDGE));
    /*  Make renderbuffers with the textures (seems pointless but only because
        we are just using basic color attachments; with depth buffers,
        G-buffer, etc. this system is more obviously useful)    */
    pg_renderbuffer_init(&bg->ppbuf[0]);
    pg_renderbuffer_init(&bg->ppbuf[1]);
    pg_renderbuffer_attach(&bg->ppbuf[0], &bg->pptex[0], 0, 0, GL_COLOR_ATTACHMENT0);
    pg_renderbuffer_attach(&bg->ppbuf[1], &bg->pptex[1], 0, 0, GL_COLOR_ATTACHMENT0);
    pg_renderbuffer_build(&bg->ppbuf[0]);
    pg_renderbuffer_build(&bg->ppbuf[1]);
    /*  Finally, make a full rendertarget, ready to be targeted by renderpasses */
    pg_rendertarget_init(&bg->target, &bg->ppbuf[0], &bg->ppbuf[1]);

    /*  Initialize quadbatch and create the basic renderpasses  */
    pg_quadbatch_init(&bg->quadbatch, 16384);

    pg_quadbatch_init_pass(&bg->quadbatch, &bg->sprites_pass, &bg->target);
    pg_quadbatch_pass_texture(&bg->sprites_pass, bg->imgs.tex, NULL, &bg->font_tex, NULL);
    pg_renderpass_viewer(&bg->sprites_pass, &bg->view);
    pg_renderpass_depth_write(&bg->sprites_pass, 1);
    pg_renderpass_depth_test(&bg->sprites_pass, 1, GL_LEQUAL);
    pg_renderpass_uniform(&bg->sprites_pass, "alpha_cutoff", PG_FLOAT,
        &PG_TYPE_FLOAT(0));

    pg_ezpost_screen(&bg->copy_pass, &bg->target);

    /*  Make a UI context, give the root a variable pointing to the game structure  */
    pg_ui_init(&bg->ui, &bg->target,
        bg->imgs.tex, &bg->font_tex,
        &PG_TEXT_FORMATTER(
            .fonts = PG_FONTS(3, &bg->bgfont[0], &bg->bgfont[1], &bg->bgfont[2]),
            .size = vec2(1,1), .htab = 1 ));
    *pg_ui_variable(&bg->ui, bg->ui.root, "bouncegame") = PG_TYPE_POINTER(bg);

    /*  Push the renderpasses into the renderer in their draw order */
    ARR_PUSH(bg->rend.passes, &bg->sprites_pass);
    ARR_PUSH(bg->rend.passes, &bg->ui.rendpass);
    ARR_PUSH(bg->rend.passes, &bg->copy_pass);

    /*  Start the game  */
    bg->state = BG_GAMEPLAY;
    bg_start_gameplay(bg);
    pg_input_mouse_mode(PG_MOUSE_FREE);
}

static void bouncegame_tick(struct pg_game_state* state)
{
    pg_input_poll();
    struct bouncegame_core* bg = state->data;
    if(pg_input_user_exit()) state->running = 0;
    if(bg->state == BG_GAMEPLAY) bg_gameplay_tick(bg);
    pg_ui_update(&bg->ui, (float)state->ticks / 60.0f);
    bouncegame_draw(state);
    pg_input_flush();
}

static void bouncegame_draw(struct pg_game_state* state)
{
    struct bouncegame_core* bg = state->data;
    float dt = state->tick_over;
    dt = 0;
    pg_rendertarget_clear(&bg->target, vec4(1,1,1,1));
    pg_ui_draw(&bg->ui, dt / 60.0f);
    pg_renderpass_clear_drawdata(&bg->sprites_pass);
    if(bg->state == BG_GAMEPLAY) bg_gameplay_draw(bg, dt);
    pg_renderer_draw_frame(&bg->rend);
}

void bouncegame_deinit(void* data)
{
    struct bouncegame_core* bg = data;
    pg_asset_imgset_deinit(&bg->imgs);
    pg_texture_deinit(&bg->font_tex);
    pg_texture_deinit(&bg->pptex[0]);
    pg_texture_deinit(&bg->pptex[1]);
    pg_font_deinit(&bg->bgfont[0]);
    pg_font_deinit(&bg->bgfont[1]);
    pg_font_deinit(&bg->bgfont[2]);
    pg_renderbuffer_deinit(&bg->ppbuf[0]);
    pg_renderbuffer_deinit(&bg->ppbuf[1]);
    pg_renderer_deinit(&bg->rend);
    pg_renderpass_deinit(&bg->sprites_pass);
    pg_renderpass_deinit(&bg->copy_pass);
    pg_quadbatch_deinit(&bg->quadbatch);
    pg_ui_deinit(&bg->ui);
    free(bg);
}
