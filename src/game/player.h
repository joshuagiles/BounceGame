BG_ENTITY_SUBTYPE_DATA(bg_player_data,
    vec4 color;
    int facing;
    int jump_ticks;
    int jump_released;
    enum {
        BG_PLAYER_IDLE,
        BG_PLAYER_WALK,
        BG_PLAYER_AIR_CONTROLLED,
        BG_PLAYER_AIR_BALLISTIC,
        BG_PLAYER_FLIGHT,
    } action_state;
);

BG_ENTITY_TICK(bg_entity_tick_player);
BG_ENTITY_DRAW(bg_entity_draw_player);

bg_entpool_id_t bg_player_alloc(struct bg_entity** out_ptr);
void bg_player_set_color(struct bg_entity* ent, vec4 color);
