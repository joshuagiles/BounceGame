/********************************/
/*  Full gameplay state         */
/********************************/
struct bg_gameplay_state {
    struct bouncegame_core* bg;
    /*  UI  */
    struct pg_ui_context* ui_context;
    pg_ui_t ui;
    /*  Rendering   */
    struct pg_viewer worldview;
    struct pg_simple_anim cam_lookat_anim;
    struct pg_simple_anim cam_angle_anim;
    vec2 view_pos;
    vec2 view_want;
    vec2 view_speed;
    /*  Game state  */
    int tick;
    struct bg_level_bounce bounces[4];
    struct bg_level cur_level;
    bg_entpool_id_t plr_id;
    bg_entity_arr_t game_entities;
};

/********************************/
/*  Base functions (gameplay.c) */
/********************************/
void bg_start_gameplay(struct bouncegame_core* bg);
void bg_end_gameplay(struct bouncegame_core* bg);
void bg_gameplay_update(struct bouncegame_core* bg);
void bg_gameplay_tick(struct bouncegame_core* bg);
void bg_gameplay_draw(struct bouncegame_core* bg, float dt);
