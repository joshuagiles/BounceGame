#include "procgl/procgl.h"
#include "bouncegame.h"

static const struct bg_entity_base player_base = {
    .name = "Player",
    //.flags = BG_ENTITY_FLAG_DEBUG,
    .bound = { {{ -8, -8 }}, {{ 8, 8 }} },
    .tick = bg_entity_tick_player,
    .draw = bg_entity_draw_player,
    //.collider = { .type = BG_COLLIDER_CIRCLE, .radius = 0.35f },
    .collider = { .type = BG_COLLIDER_AABB, .box0 = {{ -0.4, -0.5 }}, .box1 = {{ 0.4, 0.2 }} },
};

bg_entpool_id_t bg_player_alloc(struct bg_entity** out_ptr)
{
    struct bg_entity* new_ent;
    bg_entpool_id_t new_id = bg_entpool_alloc(1, &new_ent);
    bg_entity_init_from_base(new_ent, &player_base);
    new_ent->pos = vec2(16,2);
    bg_entity_sync_to_collider(new_ent);
    struct bg_player_data* plr_data = bg_player_data(new_ent);
    bg_player_set_color(new_ent, vec4(1,1,1,0));
    if(out_ptr) *out_ptr = new_ent;
    return new_id;
}

static void bg_player_input(struct bg_entity* plr, struct bg_gameplay_state* g)
{
    struct bg_player_data* plr_data = bg_player_data(plr);
    float move_speed = 0.125;
    if((plr->flags & BG_ENTITY_FLAG_BALLISTIC)) move_speed = 0.0025;
    if(pg_input_kb_check(SDL_SCANCODE_A, PG_CONTROL_HELD)) {
        plr->vel.x -= move_speed;
        plr_data->facing = 1;

    }
    if(pg_input_kb_check(SDL_SCANCODE_D, PG_CONTROL_HELD)) {
        plr->vel.x += move_speed;
        plr_data->facing = 0;
    }
    if(pg_input_kb_check(SDL_SCANCODE_W, PG_CONTROL_HELD)) {
        if(plr_data->jump_ticks > 0 && !plr_data->jump_released) {
            plr->vel.y += (plr_data->jump_ticks / 30.0f) * 0.015;
            plr_data->jump_ticks -= 1;
        } else if((plr->flags & BG_ENTITY_FLAG_GROUND) && plr_data->jump_released) {
            plr->vel.y = 0.2f;
            plr_data->jump_ticks = 30;
            plr_data->jump_released = 0;
        }
    } else {
        plr_data->jump_ticks = 0;
        plr_data->jump_released = 1;
    }
    /*
    if(pg_input_kb_check(SDL_SCANCODE_S, PG_CONTROL_HELD)) {
        plr->vel.y -= move_speed;
    }*/
    if(pg_input_kb_check(SDL_SCANCODE_E, PG_CONTROL_HIT)) {
        bg_player_set_color(plr, vec4(RANDF(1), RANDF(1), RANDF(1), 0));
    }
    /*  Calculate action state based on velocity, on surface, etc.    */
    if(plr_data->action_state != BG_PLAYER_FLIGHT) {
        if(plr->flags & BG_ENTITY_FLAG_GROUND) {
            if(fabs(plr->vel.x) > 0.01) {
                plr_data->action_state = BG_PLAYER_WALK;
            } else {
                plr_data->action_state = BG_PLAYER_IDLE;
            }
        } else if(plr->flags & BG_ENTITY_FLAG_BALLISTIC) {
            plr_data->action_state = BG_PLAYER_AIR_BALLISTIC;
        } else {
            plr_data->action_state = BG_PLAYER_AIR_CONTROLLED;
        }
    }
    /*  Convert mouse position to world coordinates */
    vec2 mouse = pg_input_mouse_pos();
    vec2 mouse_screen = vec2_mul(mouse, g->bg->halfres);
    vec2 mouse_world = vec2_add(mouse_screen, g->view_pos);
    vec2 mouse_from_plr = vec2_sub(mouse_world, vec2_scale(plr->pos, 8));
    /*  If the player isn't bouncing around, show the first four bounces of
        the path if they were to click. If they click, then go in flight    */
    if(pg_input_kb_check(PG_LEFT_MOUSE, PG_CONTROL_HIT)) {
        if(plr_data->action_state == BG_PLAYER_FLIGHT) {
            //plr_data->action_state = BG_PLAYER_AIR_BALLISTIC;
        } else {
            plr->flags &= ~BG_ENTITY_FLAG_GROUND;
            plr_data->action_state = BG_PLAYER_FLIGHT;
            plr->flags |= BG_ENTITY_FLAG_BALLISTIC;
            g->bounces[0].start = plr->pos;
            g->bounces[0].dir = vec2_norm(mouse_from_plr);
            bg_level_calc_trajectory(&g->cur_level, g->bounces, 4);
        }
    } else if(plr_data->action_state != BG_PLAYER_FLIGHT) {
        g->bounces[0].start = plr->pos;
        g->bounces[0].dir = vec2_norm(mouse_screen);
        bg_level_calc_trajectory(&g->cur_level, g->bounces, 4);
    }
}

BG_ENTITY_TICK(bg_entity_tick_player)
{
    struct bouncegame_core* bg = g->bg;
    bg_player_input(ent, g);
    float plr_size = 0.45f;
    struct bg_player_data* plr_data = bg_player_data(ent);
    if(plr_data->action_state == BG_PLAYER_FLIGHT) {
        vec2 toward_bounce = vec2_sub(g->bounces[0].end, ent->pos);
        if(vec2_len2(toward_bounce) < (0.25*0.25)) {
            printf("\n\n");
            int bounce_i;
            for(bounce_i = 0; bounce_i < 4; ++bounce_i) {
                printf("Bounce %d {\n", bounce_i);
                printf("  start (%f, %f)\n", VEC_XY(g->bounces[bounce_i].start));
                printf("  dir (%f, %f)\n", VEC_XY(g->bounces[bounce_i].dir));
                printf("  end (%f, %f)\n", VEC_XY(g->bounces[bounce_i].end));
                printf("  bounce (%f, %f)\n", VEC_XY(g->bounces[bounce_i].bounce));
                printf("}\n");
            }
            ent->flags |= BG_ENTITY_FLAG_BALLISTIC;
            ent->flags &= ~BG_ENTITY_FLAG_GROUND;
            ent->vel = vec2_scale(g->bounces[0].bounce, 0.3);
            ent->pos = vec2_add(g->bounces[0].end, vec2_scale(g->bounces[0].bounce, 0.5));
            plr_data->action_state = BG_PLAYER_AIR_BALLISTIC;
            /*
            g->bounces[0] = g->bounces[1];
            g->bounces[1] = g->bounces[2];
            g->bounces[2] = g->bounces[3];
            g->bounces[3].start = g->bounces[2].end;
            g->bounces[3].dir = g->bounces[2].bounce;
            bg_level_calc_trajectory(&g->cur_level, &g->bounces[3], 1);*/
        } else {
            ent->pos = vec2_add(ent->pos, vec2_tolen(toward_bounce, 0.25));
        }
        if(vec2_is_zero(g->bounces[0].bounce)) {
            plr_data->action_state = BG_PLAYER_AIR_BALLISTIC;
        }
        return;
    }
    /*  Handle setting the sprite, with animation, reverse for facing, etc. */
    if(plr_data->action_state == BG_PLAYER_IDLE) {
        pg_asset_imgset_set_stepper(&bg->imgs, &ent->anim, "player", "Idle",
            PG_IMAGE_STEPPER_NORESET |
            (plr_data->facing ? PG_IMAGE_STEPPER_FLIP_X : 0));
    } else if(plr_data->action_state == BG_PLAYER_WALK) {
        pg_asset_imgset_set_stepper(&bg->imgs, &ent->anim, "player", "Walk",
            PG_IMAGE_STEPPER_NORESET |
            (plr_data->facing ? PG_IMAGE_STEPPER_FLIP_X : 0));
    } else if(plr_data->action_state == BG_PLAYER_AIR_CONTROLLED
           || plr_data->action_state == BG_PLAYER_AIR_BALLISTIC) {
        if(ent->vel.y < 0) {
            pg_asset_imgset_set_stepper(&bg->imgs, &ent->anim, "player", "JumpDown",
                PG_IMAGE_STEPPER_SINGLE |
                (plr_data->facing ? PG_IMAGE_STEPPER_FLIP_X : 0));
        } else {
            pg_asset_imgset_set_stepper(&bg->imgs, &ent->anim, "player", "JumpUp",
                PG_IMAGE_STEPPER_SINGLE |
                (plr_data->facing ? PG_IMAGE_STEPPER_FLIP_X : 0));
        }
    } else if(plr_data->action_state == BG_PLAYER_FLIGHT) {
        pg_asset_imgset_set_stepper(&bg->imgs, &ent->anim, "player", "RocketBase",
            PG_IMAGE_STEPPER_SINGLE |
            (plr_data->facing ? PG_IMAGE_STEPPER_FLIP_X : 0));
    }
    pg_asset_imgset_step(&bg->imgs, &ent->anim, 10);
    //bg_debug_print_collider(&ent->collider, "Player");
    bg_entity_behavior_gravity(g, ent);
    bg_entity_behavior_velocity(g, ent, vec2(1,1));
    ent->flags &= ~BG_ENTITY_FLAG_GROUND;
    bg_entity_behavior_collide_level(g, ent);
    #if 1
    //vec2 collide_push = bg_level_collide_circle(&g->cur_level, ent->pos, plr_size);
    /*
    ent->pos = vec2_add(ent->pos, vec2_scale(collide_push, 1));
    collide_push = bg_level_collide_circle(&g->cur_level, ent->pos, plr_size);
    ent->pos = vec2_add(ent->pos, vec2_scale(collide_push, 1));*/
    #else
    vec2 b0 = vec2( ent->pos.x - plr_size, ent->pos.y - plr_size );
    vec2 b1 = vec2( ent->pos.x + plr_size, ent->pos.y + plr_size );
    vec2 collide_push = bg_level_collide_aabb(&g->cur_level, b0, b1);
    ent->pos = vec2_add(ent->pos, vec2_scale(collide_push, 1));
    b0 = vec2( ent->pos.x - plr_size, ent->pos.y - plr_size );
    b1 = vec2( ent->pos.x + plr_size, ent->pos.y + plr_size );
    collide_push = bg_level_collide_aabb(&g->cur_level, b0, b1);
    ent->pos = vec2_add(ent->pos, vec2_scale(collide_push, 1));
    #endif
}

BG_ENTITY_DRAW(bg_entity_draw_player)
{
    struct bouncegame_core* bg = g->bg;
    struct bg_player_data* plr_data = bg_player_data(ent);
    vec2 draw_pos = vec2_add(ent->pos, vec2_scale(ent->vel, dt));
    draw_pos =(vec2_scale(draw_pos, 8));
    pg_tex_frame_t O_frame = ent->anim.uv;
    float rot = 0;
    if(plr_data->action_state == BG_PLAYER_FLIGHT) {
        vec2 toward_bounce = vec2_sub(g->bounces[0].end, ent->pos);
        rot = atan2f(toward_bounce.x, toward_bounce.y);
    } else if(plr_data->action_state == BG_PLAYER_AIR_BALLISTIC) {
        rot = atan2f(ent->vel.x, ent->vel.y);
    }
    pg_quadbatch_add_sprite(&bg->quadbatch, PG_EZDRAW_2D(
        .frame = O_frame, .pos = vec3(VEC_XY(draw_pos)),
        .rotation = rot,
        .scale = vec2(8,8), .color_mod = vec4(1,1,1,1),
        .color_add = vec4(0) ));
}

void bg_player_set_color(struct bg_entity* ent, vec4 color)
{
    struct bg_player_data* plr_data = bg_player_data(ent);
    plr_data->color = color;
}
