#include "procgl/procgl.h"
#include "bouncegame.h"

/********************************/
/*  Core collision functions    */
/********************************/
/*  These take primitives in world-space!   */
/*  (primitive, primitive) -> collision     */
/*  'bg_collide_a_b()' argument types should be (a, b) */

#define RETURN_NO_COLLISION(out) \
    do { \
        *out = (struct bg_collision){ .did_collide = 0, .debug = out->debug }; \
        return; \
    } while(0) \

void bg_collide_circle_circle(struct bg_collision* out,
                struct bg_collider_prim* prim0, struct bg_collider_prim* prim1)
{
    vec2 toward = vec2_sub(prim0->ctr, prim1->ctr);
    float dist = vec2_len(toward);
    float overlap = dist - (prim0->radius + prim1->radius);
    if(overlap <= 0) RETURN_NO_COLLISION(out);
    vec2 sep_norm = vec2_tdiv(toward, dist);
    *out = (struct bg_collision) { .did_collide = 1,
        .sep_axis = toward,
        .sep_norm = sep_norm,
        .sep_len = overlap,
        .surf_norm = sep_norm,
        .debug = out->debug,
    };
}

void bg_collide_aabb_aabb(struct bg_collision* out,
                struct bg_collider_prim* prim0, struct bg_collider_prim* prim1)
{
    float x_dists[2] = {
        prim1->box0.x - prim0->box1.x, prim1->box1.x - prim0->box0.x };
    float y_dists[2] = {
        prim1->box0.y - prim0->box1.y, prim1->box1.y - prim0->box0.y };
    vec2 min_push = vec2(0,0);
    if(x_dists[0] < 0 || x_dists[1] > 0) {
        if(fabs(x_dists[0]) > x_dists[1]) min_push.x = x_dists[1];
        else min_push.x = x_dists[0];
    }
    if(y_dists[0] < 0 || y_dists[1] > 0) {
        if(fabs(y_dists[0]) > y_dists[1]) min_push.y = y_dists[1];
        else min_push.y = y_dists[0];
    }
    float dist = 0;
    if(fabs(min_push.y) < fabs(min_push.x)) {
        min_push.x = 0;
        dist = fabs(min_push.y);
    } else {
        min_push.y = 0;
        dist = fabs(min_push.x);
    }
    vec2 sep_norm = vec2_tdiv(min_push, dist);
    *out = (struct bg_collision) {
        .did_collide = (dist != 0),
        .sep_axis = min_push,
        .sep_norm = sep_norm,
        .sep_len = dist,
        .surf_norm = sep_norm,
        .debug = out->debug,
    };
}

void bg_collide_circle_aabb(struct bg_collision* out,
                struct bg_collider_prim* prim0, struct bg_collider_prim* prim1)
{
    vec2 closest_point = vec2_min(vec2_max(prim0->ctr, prim1->box0), prim1->box1);
    vec2 sep_axis = vec2_sub(prim0->ctr, closest_point);
    if(vec2_is_zero(sep_axis)) {
        float dists[2][2] = {
            { prim1->box0.x - prim0->ctr.x, prim1->box1.x - prim0->ctr.x },
            { prim1->box0.y - prim0->ctr.y, prim1->box1.y - prim0->ctr.y } };
        sep_axis = vec2(LM_MAX(dists[0][0], dists[0][1]),
                        LM_MAX(dists[1][0], dists[1][1]));
        if(fabs(sep_axis.y) < fabs(sep_axis.x)) {
            float axis_sign = LM_SGN(sep_axis.y);
            out->sep_axis = vec2(0, sep_axis.y + axis_sign * prim0->radius);
            out->sep_norm = vec2(0, axis_sign);
            out->sep_len = fabs(sep_axis.y + axis_sign * prim0->radius);
        } else {
            float axis_sign = LM_SGN(sep_axis.x);
            out->sep_axis = vec2(sep_axis.x + axis_sign * prim0->radius, 0);
            out->sep_norm = vec2(LM_SGN(out->sep_axis.x), 0);
            out->sep_len = fabs(sep_axis.x + axis_sign * prim0->radius);
        }
        out->did_collide = !(out->sep_len <= 0);
        return;
    }
    float dist = vec2_len(sep_axis);
    float overlap = prim0->radius - dist;
    if(overlap <= 0) {
        *out = (struct bg_collision){ .did_collide = 0, .debug = out->debug };
        return;
    }
    float sep_len = overlap;
    vec2 sep_norm = vec2_norm(sep_axis);
    *out = (struct bg_collision) {
        .did_collide = (overlap > 0.0001),
        .sep_axis = vec2_scale(sep_norm, sep_len),
        .sep_norm = vec2_scale(sep_norm, 1),
        .sep_len = sep_len,
        .surf_norm = sep_norm,
        .debug = out->debug,
    };
    if(out->did_collide && out->sep_len < 0.00001) {
        printf("WHAT THE FUCK\n");
        printf("Overlap %f\n", overlap);
    }
}

void bg_collide_circle_grid(struct bg_collision* out,
                struct bg_collider_prim* prim0, struct bg_collider_prim* prim1)
{
    struct bg_collision coll_tmp = { .debug = out->debug };
    vec2 bound[2] = { vec2(prim0->ctr.x - prim0->radius, prim0->ctr.y - prim0->radius),
                      vec2(prim0->ctr.x + prim0->radius, prim0->ctr.y + prim0->radius) };
    vec2 push = vec2(0,0);
    float push_len = 0;
    int i, j;
    *out = coll_tmp;
    for(i = (int)bound[0].x; i <= (int)bound[1].x; ++i)
    for(j = (int)bound[0].y; j <= (int)bound[1].y; ++j) {
        struct bg_collider_prim tile_prim;
        prim1->tilegrid_fn(&tile_prim, i, j, prim1->tilegrid_ptr);
        if(!tile_prim.type) continue;
        bg_collision_prims(&coll_tmp, prim0, &tile_prim);
        if(out->debug) {
            printf("Tilegrid sub-collision {\n");
            printf("did_collide = %d, sep_len = %f\n", coll_tmp.did_collide, coll_tmp.sep_len);
            bg_debug_print_prim_collision(&coll_tmp, prim0, "Prim0", &tile_prim, "Tile Prim");
            printf("}\n");
        }
        if(coll_tmp.did_collide && coll_tmp.sep_len > out->sep_len) {
            *out = coll_tmp;
        }
        coll_tmp.did_collide = 0;
    }
}

void bg_collide_aabb_grid(struct bg_collision* out,
                struct bg_collider_prim* prim0, struct bg_collider_prim* prim1)
{
    struct bg_collision coll_tmp = { .debug = out->debug };
    vec2 bound[2] = { prim0->box0, prim0->box1 };
    vec2 push = vec2(0,0);
    float push_len;
    int i, j;
    *out = coll_tmp;
    for(i = (int)bound[0].x; i <= (int)bound[1].x; ++i)
    for(j = (int)bound[0].y; j <= (int)bound[1].y; ++j) {
        struct bg_collider_prim tile_prim;
        prim1->tilegrid_fn(&tile_prim, i, j, prim1->tilegrid_ptr);
        if(!tile_prim.type) continue;
        bg_collision_prims(&coll_tmp, prim0, &tile_prim);
        if(coll_tmp.did_collide && coll_tmp.sep_len > out->sep_len) {
            *out = coll_tmp;
        }
    }
    if(!out->did_collide) return;
    out->did_collide = (out->sep_len > 0.00001);
}

/*  Function dispatch table     */
static const struct bg_collide_func_dispatch {
    bg_collide_func_t func; int swap;
} COLLIDE_DISPATCH[BG_NUM_COLLIDER_TYPES][BG_NUM_COLLIDER_TYPES] = {
    [BG_COLLIDER_CIRCLE] = {
        [BG_COLLIDER_CIRCLE] = {    bg_collide_circle_circle,  0 },
        [BG_COLLIDER_AABB] = {      bg_collide_circle_aabb,    0 },
        [BG_COLLIDER_TILEGRID] = {  bg_collide_circle_grid,    0 } },
    [BG_COLLIDER_AABB] = {
        [BG_COLLIDER_CIRCLE] = {    bg_collide_circle_aabb,    1, },
        [BG_COLLIDER_AABB] = {      bg_collide_aabb_aabb,      0 },
        [BG_COLLIDER_TILEGRID] = {  bg_collide_aabb_grid,      0 } },
    [BG_COLLIDER_TILEGRID] = {
        [BG_COLLIDER_CIRCLE] = {    bg_collide_circle_grid,    1 },
        [BG_COLLIDER_AABB] = {      bg_collide_aabb_grid,      1 },
        [BG_COLLIDER_TILEGRID] = {  NULL,                      0} },
};

const char* BG_COLLIDER_NAME[BG_NUM_COLLIDER_TYPES] = {
    [BG_NO_COLLIDER] = "None",
    [BG_COLLIDER_CIRCLE] = "Circle",
    [BG_COLLIDER_AABB] = "AABB",
    [BG_COLLIDER_TILEGRID] = "Grid",
};

/****************************/
/*  Basic impulse functions */
/****************************/
void bg_impulse_add(struct bg_impulse* out,
                    struct bg_impulse* imp0, struct bg_impulse* imp1)
{
    out->push = vec2_add(imp0->push, imp1->push);
    out->push_len = vec2_len(out->push);
    out->push_norm = out->push_len ? vec2_tdiv(imp0->push, imp0->push_len) : vec2();
}

int bg_impulse_cmp(struct bg_impulse* imp0, struct bg_impulse* imp1)
{
    return imp0->push_len - imp1->push_len;
}

vec2 bg_impulse_response(struct bg_impulse* imp, vec2 vel, float absorb)
{
    return vec2_dot(vel, imp->push_norm) > 0 ? vec2(0,0) :
                vec2_scale(imp->push_norm, absorb * -vec2_dot(vel, imp->push_norm));
}

/********************************************/
/*  Collision detection and response code   */
/********************************************/

/*  Resolve a collision between primitives (no mass or other game logic,
    just pure collision checking    */
void bg_collision_prims(struct bg_collision* out,
                        struct bg_collider_prim* prim0, struct bg_collider_prim* prim1)
{
    /*  Dispatch to the correct function with correct argument order    */
    *out = (struct bg_collision){ .did_collide = 0, .debug = out->debug };
    const struct bg_collide_func_dispatch* func = &COLLIDE_DISPATCH[prim0->type][prim1->type];
    if(!func || !prim0->type || !prim1->type) return;
    struct bg_collider_prim* arg_fixed_order[2] = { prim0, prim1 };
    struct bg_collider_prim* prim[2] = { arg_fixed_order[func->swap], arg_fixed_order[1 - func->swap] };
    func->func(out, prim[0], prim[1]);
    float impulse[2] = { 0.5, -0.5 };
    if(func->swap) {
        out->impulse[0] = impulse[1];
        out->impulse[1] = impulse[0];
    } else {
        out->impulse[0] = impulse[0];
        out->impulse[1] = impulse[1];
    }
}

/*  Resolve a full collision for any two colliders. */
void bg_collision_check(struct bg_collision* out,
                        struct bg_collider* c0, struct bg_collider* c1)
{
    *out = (struct bg_collision){ .did_collide = 0, .debug = out->debug };
    if((c0->flags & c1->flags & BG_COLLIDER_STATIC)
    || !c0->prim.type || !c1->prim.type) return;
    /*  Dispatch to the correct function with correct argument order    */
    const struct bg_collide_func_dispatch* func = &COLLIDE_DISPATCH[c0->prim.type][c1->prim.type];
    if(!func) return;
    struct bg_collider* arg_fixed_order[2] = { c0, c1 };
    struct bg_collider* coll[2] = { arg_fixed_order[func->swap], arg_fixed_order[1 - func->swap] };
    func->func(out, &coll[0]->prim_ws, &coll[1]->prim_ws);
    if(!out->did_collide) return;
    /*  Calculate mass differential; static = infinite mass */
    float impulse[2];
    if(c0->flags & BG_COLLIDER_STATIC) {
        impulse[0] = 0.0f;
        impulse[1] = -1.0f;
    } else if(c1->flags & BG_COLLIDER_STATIC) {
        impulse[0] = 1.0f;
        impulse[1] = 0.0f;
    } else {
        float mass[2] = { coll[0]->mass, coll[1]->mass };
        float total_mass = mass[0] + mass[1];
        impulse[0] = mass[1] / total_mass;
        impulse[1] = -(mass[0] / total_mass);
    }
    /*  Output un-swapped (if necessary) impulses   */
    if(func->swap) {
        out->impulse[0] = impulse[1];
        out->impulse[1] = impulse[0];
    } else {
        out->impulse[0] = impulse[0];
        out->impulse[1] = impulse[1];
    }
}

void bg_collision_impulse(struct bg_collision* coll,
                          struct bg_impulse* imp0, struct bg_impulse* imp1)
{
    if(!coll->did_collide) return;
    if(imp0) {
        imp0->push = vec2_scale(coll->sep_axis, coll->impulse[0]);
        imp0->push_len = coll->sep_len * fabs(coll->impulse[0]);
        imp0->push_norm = vec2_scale(coll->sep_norm, LM_SGN(coll->impulse[0]));
    }
    if(imp1) {
        imp1->push = vec2_scale(coll->sep_axis, coll->impulse[1]);
        imp1->push_len = coll->sep_len * fabs(coll->impulse[1]);
        imp1->push_norm = vec2_scale(coll->sep_norm, LM_SGN(coll->impulse[1]));
    }
}

void bg_collision_impulse_gt(struct bg_collision* coll,
                             struct bg_impulse* imp0, struct bg_impulse* imp1)
{
    if(!coll->did_collide) return;
    if(imp0 && coll->impulse[0] * coll->sep_len > imp0->push_len) {
        imp0->push = vec2_scale(coll->sep_axis, coll->impulse[0]);
        imp0->push_len = coll->sep_len * coll->impulse[0];
        imp0->push_norm = vec2_scale(coll->sep_norm, LM_SGN(coll->impulse[0]));
    }
    if(imp1 && coll->impulse[1] * coll->sep_len > imp1->push_len) {
        imp1->push = vec2_scale(coll->sep_axis, coll->impulse[1]);
        imp1->push_len = coll->sep_len * coll->impulse[1];
        imp1->push_norm = vec2_scale(coll->sep_norm, LM_SGN(coll->impulse[1]));
    }
}

void bg_collision_impulse_add(struct bg_collision* coll,
                              struct bg_impulse* imp0, struct bg_impulse* imp1)
{
    if(!coll->did_collide) return;
    if(imp0) {
        imp0->push = vec2_add(imp0->push, vec2_scale(coll->sep_axis, coll->impulse[0]));
        imp0->push_len = vec2_len(imp0->push);
        imp0->push_norm = imp0->push_len ? vec2_norm(imp0->push) : vec2();
    }
    if(imp1) {
        imp1->push = vec2_add(imp1->push, vec2_scale(coll->sep_axis, coll->impulse[0]));
        imp1->push_len = vec2_len(imp1->push);
        imp1->push_norm = imp1->push_len ? vec2_norm(imp1->push) : vec2();
    }
}

/*  Basic collider functions  */
void bg_collider_init(struct bg_collider* c0)
{
    *c0 = (struct bg_collider){ .mass = 1.0f };
}

void bg_collider_set_pos(struct bg_collider* c0, vec2 pos)
{
    c0->pos = pos;
    bg_collider_prim_tx(&c0->prim_ws, &c0->prim, pos);
}

void bg_collider_set_vel(struct bg_collider* c0, vec2 vel)
{
    c0->vel = vel;
    c0->vel_norm = vec2_norm(vel);
}

void bg_collider_set(struct bg_collider* c0, vec2 pos, vec2 vel)
{
    bg_collider_set_pos(c0, pos);
    bg_collider_set_vel(c0, vel);
}

void bg_collider_set_prim(struct bg_collider* c0, struct bg_collider_prim* prim)
{
    c0->prim = *prim;
    bg_collider_prim_tx(&c0->prim_ws, prim, c0->pos);
}

void bg_collider_prim_tx(struct bg_collider_prim* out,
                         struct bg_collider_prim* prim, vec2 move)
{
    switch(prim->type) {
    case BG_COLLIDER_CIRCLE:
        out->radius = prim->radius;
        out->ctr = vec2_add(move, prim->ctr);
        break;
    case BG_COLLIDER_AABB:
        out->box0 = vec2_add(move, prim->box0);
        out->box1 = vec2_add(move, prim->box1);
        break;
    default: break;
    }
    out->type = prim->type;
}

/*  Collider impulses   */
void bg_collider_do_impulse(struct bg_collider* c0, float absorb)
{
    if(c0->imp.push_len <= 0) return;
    bg_collider_response(c0, absorb);
    bg_collider_set_pos(c0, vec2_add(c0->pos, vec2_scale(c0->imp.push, 1)));
    c0->imp = (struct bg_impulse){{}};
}

void bg_collider_set_impulse(struct bg_collider* c0, struct bg_impulse* imp)
{
    c0->imp = imp ? *imp : (struct bg_impulse){{}};
}

void bg_collider_set_impulse_gt(struct bg_collider* c0, struct bg_impulse* imp)
{
    if(imp && bg_impulse_cmp(imp, &c0->imp) > 0) c0->imp = *imp;
}

void bg_collider_add_impulse(struct bg_collider* c0, struct bg_impulse* imp)
{
    if(imp) bg_impulse_add(&c0->imp, &c0->imp, imp);
}

/************************/
/*  Collider responses  */
/*  absorb = 2      bounce
    absorb = 1      slide
    absorb = 0      pass through
*/
vec2 bg_collider_response(struct bg_collider* c0, float absorb)
{
    if(c0->imp.push_len == 0) return vec2(0);
    vec2 slide = bg_impulse_response(&c0->imp, c0->vel, absorb);
    c0->vel = vec2_add(c0->vel, slide);
    return slide;
}

/********************************/
/*  Debug printing functions    */
/********************************/
void bg_debug_print_primitive(struct bg_collider_prim* prim0, char* name)
{
    printf("%s Primitive %s {\n", BG_COLLIDER_NAME[prim0->type], name);
    switch(prim0->type) {
    case BG_NO_COLLIDER: break;
    case BG_COLLIDER_CIRCLE:
        printf("  c = (%f, %f), r = %f}\n", VEC_XY(prim0->ctr), prim0->radius);
        break;
    case BG_COLLIDER_AABB:
        printf("  min: (%f, %f), max: (%f, %f)\n", VEC_XY(prim0->box0), VEC_XY(prim0->box1));
        break;
    case BG_COLLIDER_TILEGRID: break;
    }
    printf("}\n");
}

void bg_debug_print_collider(struct bg_collider* c0, char* name0)
{
    printf("Collider %s {\n", name0);
    printf("  flags %x, mass %f\n", c0->flags, c0->mass);
    printf("  pos (%f, %f), vel (%f, %f)\n", VEC_XY(c0->pos), VEC_XY(c0->vel));
    printf("  impulse (%f, %f)\n", VEC_XY(c0->imp.push));
    bg_debug_print_primitive(&c0->prim, "Local");
    bg_debug_print_primitive(&c0->prim_ws, "World");
}

void bg_debug_print_collision(struct bg_collision* coll,
                              struct bg_collider* c0, char* name0,
                              struct bg_collider* c1, char* name1)
{
    struct bg_impulse imp0, imp1;
    bg_collision_impulse(coll, &imp0, &imp1);
    vec2 slide0 = vec2(0,0), slide1 = vec2(0,0);
    if(coll->impulse[0]) slide0 = bg_impulse_response(&imp0, c0->vel, 1.0f);
    if(coll->impulse[1]) slide1 = bg_impulse_response(&imp1, c1->vel, 1.0f);
    printf("Collision {\n");
    printf("  did collide = %s\n", coll->did_collide ? "Yes" : "No");
    printf("  separation axis (%f, %f)\n", VEC_XY(coll->sep_axis));
    printf("  separation norm (%f, %f)\n", VEC_XY(coll->sep_norm));
    printf("  separation length %f\n", coll->sep_len);
    printf("  impulses (%f, %f)\n", coll->impulse[0], coll->impulse[1]);
    printf("Collider %s {\n", name0);
    printf("  flags %x\n", c0->flags);
    printf("  pos (%f, %f), vel (%f, %f)\n", VEC_XY(c0->pos), VEC_XY(c0->vel));
    printf("  push (%f, %f), slide (%f, %f)\n", VEC_XY(imp0.push), VEC_XY(slide0));
    bg_debug_print_primitive(&c0->prim, "Local");
    bg_debug_print_primitive(&c0->prim_ws, "World");
    printf("}\n");
    printf("Collider %s {\n", name1);
    printf("  flags %x\n", c1->flags);
    printf("  pos (%f, %f), vel (%f, %f)\n", VEC_XY(c1->pos), VEC_XY(c1->vel));
    printf("  push (%f, %f), slide (%f, %f)\n", VEC_XY(imp1.push), VEC_XY(slide1));
    bg_debug_print_primitive(&c1->prim, "Local");
    bg_debug_print_primitive(&c1->prim_ws, "World");
    printf("}\n}\n");
}

void bg_debug_print_prim_collision(struct bg_collision* coll,
                                   struct bg_collider_prim* c0, char* name0,
                                   struct bg_collider_prim* c1, char* name1)
{
    struct bg_impulse imp0, imp1;
    bg_collision_impulse(coll, &imp0, &imp1);
    printf("Primitive Collision {\n");
    printf("  did collide = %s\n", coll->did_collide ? "Yes" : "No");
    printf("  separation axis (%f, %f)\n", VEC_XY(coll->sep_axis));
    printf("  separation normal (%f, %f)\n", VEC_XY(coll->sep_norm));
    printf("  separation length %f\n", coll->sep_len);
    printf("  impulses %f, %f\n", coll->impulse[0], coll->impulse[1]);
    printf("  impulse push 0 (%f, %f)\n", VEC_XY(imp0.push));
    printf("  impulse push 1 (%f, %f)\n", VEC_XY(imp1.push));
    bg_debug_print_primitive(c0, name0);
    bg_debug_print_primitive(c1, name1);
    printf("}\n");
}
