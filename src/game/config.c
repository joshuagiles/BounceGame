#include "procgl/procgl.h"
#include "bouncegame.h"

void bg_user_options_default(struct bg_user_options* cfg)
{
    *cfg = (struct bg_user_options) {
        .windowed_res = vec2(960,600),
        .fullscreen_res = vec2(960,600),
        .fullscreen = 0,
        .mouse_sensitivity = 1.0f,
        .gamma = 1.0f,
    };
}

void bg_user_options_read_or_create(struct bg_user_options* cfg, const char* filename)
{
    /*  Read file, return default config and 0 if it can't be read. */
    FILE* file = fopen(filename, "r");
    bg_user_options_default(cfg);
    if(!file) {
        printf("Creating config file \"%s\"!\n", filename);
        bg_user_options_write(cfg, filename);
        return;
    }
    fseek(file, 0, SEEK_END);
    size_t len = ftell(file);
    fseek(file, 0, SEEK_SET);
    char* config_str = malloc(sizeof(*config_str) * len);
    fread(config_str, 1, len, file);
    fclose(file);
    /*  Now there is a string in memory, it must be parsed  */
    cJSON* config_json = cJSON_Parse(config_str);
    if(!config_json) {
        printf("ERROR Failed to parse JSON file %s\n", filename);
        return;
    }
    json_get_child_vec2(config_json, "windowed_res", &cfg->windowed_res);
    json_get_child_vec2(config_json, "fullscreen_res", &cfg->fullscreen_res);
    json_get_child_bool(config_json, "fullscreen", &cfg->fullscreen);
    json_get_child_float(config_json, "mouse_sensitivity", &cfg->mouse_sensitivity);
    json_get_child_float(config_json, "gamma", &cfg->gamma);
    cJSON_Delete(config_json);
}

int bg_user_options_write(struct bg_user_options* cfg, const char* filename)
{
    cJSON* config_json = cJSON_CreateObject();
    cJSON_AddItemToObject(config_json, "windowed_res",
        cJSON_CreateFloatArray(cfg->windowed_res.v, 2));
    cJSON_AddItemToObject(config_json, "fullscreen_res",
        cJSON_CreateFloatArray(cfg->fullscreen_res.v, 2));
    cJSON_AddItemToObject(config_json, "fullscreen",
        cJSON_CreateBool(cfg->fullscreen));
    cJSON_AddItemToObject(config_json, "mouse_sensitivity",
        cJSON_CreateNumber(cfg->mouse_sensitivity));
    cJSON_AddItemToObject(config_json, "gamma",
        cJSON_CreateNumber(cfg->gamma));
    char* config_text = cJSON_Print(config_json);
    size_t len = strlen(config_text);
    cJSON_Delete(config_json);
    FILE* file = fopen(filename, "w");
    if(!file) {
        printf("ERROR Failed to open file for writing: %s\n", filename);
        return 0;
    }
    fwrite(config_text, 1, len, file);
    fclose(file);
    return 1;
}

void bg_user_options_apply(struct bg_user_options* cfg, struct bouncegame_core* core)
{
}
