struct bg_user_options {
    /*  Window management stuff */
    vec2 windowed_res;
    vec2 fullscreen_res;
    int fullscreen;
    /*  Input   */
    float mouse_sensitivity;
    /*  Video/graphics options  */
    float gamma;
};

struct bg_user_options bg_default_user_options;

void bg_user_options_default(struct bg_user_options* cfg);
void bg_user_options_read_or_create(struct bg_user_options* cfg, const char* filename);
int bg_user_options_write(struct bg_user_options* cfg, const char* filename);
void bg_user_options_apply(struct bg_user_options* cfg, struct bouncegame_core* core);

