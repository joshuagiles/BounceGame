struct bouncegame_core;
struct bg_gameplay_state;
struct bg_entity_base;
struct bg_entity;
struct bg_level;

struct bg_impulse;
struct bg_collider;
struct bg_collider_prim;
struct bg_collision;

#include "config.h"
#include "collision.h"
#include "entity.h"
#include "player.h"
#include "level.h"
#include "gameplay.h"

struct bouncegame_core {
    /************/
    /*  Assets  */
    /*  All the graphics are in this imgset asset   */
    struct pg_asset_imgset imgs;
    /*  All the fonts in a separate texture together    */
    struct pg_texture font_tex;
    struct pg_font bgfont[3];
    /****************/
    /*  Rendering   */
    /*  Convenience data about the game window, for our own use */
    vec2 halfres;
    vec2 res;
    float ar;
    /*  procgame rendering structure    */
    struct pg_renderer rend;            // Renderer manages shaders and renderpasses
    struct pg_renderpass sprites_pass;  // Renderpasses manage GL state when drawing the frame
    struct pg_renderpass copy_pass;     // Copy pass just draws the final image on the screen
    struct pg_texture pptex[2];         // Actual textures to render to
    struct pg_renderbuffer ppbuf[2];    // Renderbuffers contain target textures
    struct pg_rendertarget target;      // Rendertarget manages renderbuffer switching
    struct pg_quadbatch quadbatch;      // Buffer where sprite draw data is accumulated each frame
    struct pg_viewer view;              // Viewer is used to create projection matrices for drawing
    /******************/
    /*  Input data    */
    vec2 mouse_motion;
    vec2 mouse_pos;
    /****************/
    /*  UI Context  */
    struct pg_ui_context ui;
    /****************/
    /*  Game states */
    enum {
        BG_GAMEPLAY,
    } state;
    struct bg_gameplay_state gameplay;
};

void bouncegame_start(struct pg_game_state* state);
