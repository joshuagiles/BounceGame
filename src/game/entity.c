#include "procgl/procgl.h"
#include "bouncegame.h"

PG_MEMPOOL_DEFINE_GLOBAL(struct bg_entity, bg_entpool, alloc);

void bg_entity_init_from_base(struct bg_entity* ent, const struct bg_entity_base* base)
{
    ent->flags = base->flags;
    ent->bound[0] = base->bound[0];
    ent->bound[1] = base->bound[1];
    strncpy(ent->name, base->name, 32);
    ent->tick = base->tick;
    ent->draw = base->draw;
    bg_collider_init(&ent->collider);
    bg_collider_set_prim(&ent->collider, &base->collider);
    ent->collider.flags = base->collider_flags;
}

void bg_entity_sync_from_collider(struct bg_entity* ent)
{
    ent->pos = ent->collider.pos;
    ent->vel = ent->collider.vel;
}

void bg_entity_sync_to_collider(struct bg_entity* ent)
{
    bg_collider_set(&ent->collider, ent->pos, ent->vel);
}

void bg_entities_tick(struct bg_gameplay_state* g)
{
    int i;
    bg_entpool_id_t ent_id;
    struct bg_entity* ent;
    ARR_FOREACH_REV(g->game_entities, ent_id, i) {
        if(!(ent = bg_entpool_get(ent_id))) {
            ARR_SWAPSPLICE(g->game_entities, i, 1);
        }
        if(ent->flags & BG_ENTITY_FLAG_DEBUG) {
            printf("%s Tick {\n", ent->name);
            if(ent->tick) ent->tick(g, ent);
            printf("}\n");
        } else if(ent->tick) ent->tick(g, ent);
    }
}

void bg_entities_draw(struct bg_gameplay_state* g, float dt)
{
    int i;
    bg_entpool_id_t ent_id;
    struct bg_entity* ent;
    ARR_FOREACH_REV(g->game_entities, ent_id, i) {
        if(!(ent = bg_entpool_get(ent_id))) continue;
        if(ent->draw) ent->draw(g, ent, dt);
    }
}

void bg_gameplay_add_entity(struct bg_gameplay_state* g, bg_entpool_id_t ent_id)
{
    ARR_PUSH(g->game_entities, ent_id);
}

void bg_entity_impulse(struct bg_entity* ent, struct bg_impulse* imp, float absorb)
{
    vec2 slide = bg_impulse_response(imp, ent->vel, absorb);
    ent->vel = vec2_add(ent->vel, slide);
    ent->pos = vec2_add(ent->pos, vec2_scale(imp->push, 1));
}

/*  Just a n^2 collision solver */
void bg_entities_resolve_collisions(struct bg_gameplay_state* g)
{
    return;
    struct bg_collision coll;
    int i, j;
    bg_entpool_id_t ent0_id, ent1_id;
    struct bg_entity* ent0, * ent1;
    ARR_FOREACH(g->game_entities, ent0_id, i) {
        if(!(ent0 = bg_entpool_get(ent0_id))) continue;
        if(!ent0->collider.prim.type) continue;
        for(j = i; j < g->game_entities.len; ++j) {
            ent1_id = g->game_entities.data[j];
            if(!(ent1 = bg_entpool_get(ent1_id))) continue;
            if(!ent1->collider.prim.type) continue;
            coll.did_collide = 0;
            bg_collision_check(&coll, &ent0->collider, &ent1->collider);
            if(!coll.did_collide) continue;
            bg_collision_impulse_gt(&coll, &ent0->collider.imp, &ent1->collider.imp);
        }
        bg_collider_do_impulse(&ent0->collider, 1.0f);
        ent0->pos = ent0->collider.pos;
        ent0->vel = ent0->collider.vel;
    }
}
