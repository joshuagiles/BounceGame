/****************************/
/*  BEHAVIOR / BASE TYPES   */
/****************************/

#define BG_ENTITY_FLAG_DEBUG        (1 << 0)
#define BG_ENTITY_FLAG_GROUND       (1 << 1)
#define BG_ENTITY_FLAG_BALLISTIC    (1 << 2)

/*  Entity behavior is encapsulated in a standard set of function pointers  */
typedef void (*bg_entity_tick_fn_t)(struct bg_gameplay_state*, struct bg_entity*);
typedef void (*bg_entity_draw_fn_t)(struct bg_gameplay_state*, struct bg_entity*, float);

/*  Those along with a few auxiliary bits of data form a "base" for any
    entity type */
struct bg_entity_base {
    /*  Base stats  */
    uint64_t flags;
    char name[32];
    vec2 bound[2];
    struct bg_collider_prim collider;
    uint64_t collider_flags;
    /*  Behavior    */
    bg_entity_tick_fn_t tick;
    bg_entity_draw_fn_t draw;
};

/************************************/
/*  Defining entity subtype data    */
/************************************/

#define BG_ENTITY_TYPE_DATA_MAX     (128)

#define BG_ENTITY_TICK(F) void F(struct bg_gameplay_state* g, struct bg_entity* ent)
#define BG_ENTITY_DRAW(F) void F(struct bg_gameplay_state* g, struct bg_entity* ent, float dt)

#define BG_ENTITY_SUBTYPE_DATA(NAME, ...) \
struct NAME { __VA_ARGS__ }; \
_Static_assert(sizeof(struct NAME) <= BG_ENTITY_TYPE_DATA_MAX, \
    "Entity subtype (" #NAME ") data size exceeds maximum size"); \
static inline struct NAME* NAME(struct bg_entity* ent) \
    { return (struct NAME*)(ent->type_data); }

/****************************/
/*  ACTIVE DATA             */
/****************************/

#define MG_ENTFLAG_CONTAINED    (1 << 0)

struct bg_entity_anim_state {
    enum {
        MG_ENTITY_NO_ANIM,
        MG_ENTITY_ANIM_LOOP,
        MG_ENTITY_ANIM_ONCE,
        MG_ENTITY_ANIM_HOLD,
    } mode;
    int sequence[16];
    int seq_len;
    /*  Index where the loop starts, and the length of the loop (in keyframes)  */
    int loop_start, loop_len;
    /*  When the current animation was entered, speed to play it
        (ticks per full loop)  */
    double start_time;
    double speed;
};

/*  Entities are stored in a global allocation pool */
PG_MEMPOOL_DECLARE_GLOBAL(struct bg_entity, bg_entpool);
typedef ARR_T(bg_entpool_id_t) bg_entity_arr_t;

/*  Running data for each entity instance   */
struct bg_entity {
    /********************/
    /*  Book-keeping    */
    int alloc, dead, last_tick;
    /********************************/
    /*  Query data                  */
    vec2 bound[2], cur_bound[2];
    /****************/
    /*  Draw data   */
    struct pg_asset_image_stepper anim;
    /********************/
    /*  Physics data    */
    vec2 move[2];
    vec2 push[2];
    vec2 pos, vel;
    struct bg_collider collider;
    /********************************/
    /*  Game logic                  */
    /*  (ie. common entity data )   */
    uint64_t flags;
    char name[32];
    int HP, max_HP;
    /****************/
    /*  Type data   */
    int type;
    char type_data[BG_ENTITY_TYPE_DATA_MAX];
    /****************/
    /*  Behavior    */
    bg_entity_tick_fn_t tick;
    bg_entity_draw_fn_t draw;
};

void bg_entity_init_from_base(struct bg_entity* ent, const struct bg_entity_base* base);
void bg_entity_sync_from_collider(struct bg_entity* ent);
void bg_entity_sync_to_collider(struct bg_entity* ent);
void bg_entity_impulse(struct bg_entity* ent, struct bg_impulse* imp, float absorb);

void bg_entities_tick(struct bg_gameplay_state* g);
void bg_entities_draw(struct bg_gameplay_state* g, float dt);

void bg_gameplay_add_entity(struct bg_gameplay_state* g, bg_entpool_id_t ent_id);

/*  Basic entity behaviors  */
void bg_entity_behavior_velocity(struct bg_gameplay_state* g, struct bg_entity* ent, vec2 slowdown);
void bg_entity_behavior_gravity(struct bg_gameplay_state* g, struct bg_entity* ent);
void bg_entity_behavior_collide_level(struct bg_gameplay_state* g, struct bg_entity* ent);
