#include "procgl/procgl.h"
#include "bouncegame.h"

/************/
/* I <3 GCC */
/************/
static const struct {
    char* string;
    int flip_x, flip_y;
    int turn_steps;
} tile_set_lookup[256] = {
    [0b00000000 ... 0b00001111] = { .string = "0000_XXXX" },
    [0b00010000 ... 0b00011111] = { .string = "0001_XXXX", .turn_steps = 0 },
    [0b10000000 ... 0b10001111] = { .string = "0001_XXXX", .turn_steps = 1 },
    [0b01000000 ... 0b01001111] = { .string = "0001_XXXX", .turn_steps = 2 },
    [0b00100000 ... 0b00101111] = { .string = "0001_XXXX", .turn_steps = 3 },

    [0b00110000] = { .string = "0011_XX0X", .turn_steps = 0 },
    [0b00110001] = { .string = "0011_XX0X", .turn_steps = 0 },
    [0b00110010] = { .string = "0011_XX1X", .turn_steps = 0 },
    [0b00110011] = { .string = "0011_XX1X", .turn_steps = 0 },
    [0b00110100] = { .string = "0011_XX0X", .turn_steps = 0 },
    [0b00110101] = { .string = "0011_XX0X", .turn_steps = 0 },
    [0b00110110] = { .string = "0011_XX1X", .turn_steps = 0 },
    [0b00110111] = { .string = "0011_XX1X", .turn_steps = 0 },
    [0b00111000] = { .string = "0011_XX0X", .turn_steps = 0 },
    [0b00111001] = { .string = "0011_XX0X", .turn_steps = 0 },
    [0b00111010] = { .string = "0011_XX1X", .turn_steps = 0 },
    [0b00111011] = { .string = "0011_XX1X", .turn_steps = 0 },
    [0b00111100] = { .string = "0011_XX0X", .turn_steps = 0 },
    [0b00111101] = { .string = "0011_XX0X", .turn_steps = 0 },
    [0b00111110] = { .string = "0011_XX1X", .turn_steps = 0 },
    [0b00111111] = { .string = "0011_XX1X", .turn_steps = 0 },

    [0b10010000] = { .string = "0011_XX0X", .turn_steps = 1 },
    [0b10011000] = { .string = "0011_XX0X", .turn_steps = 1 },
    [0b10010001] = { .string = "0011_XX1X", .turn_steps = 1 },
    [0b10011001] = { .string = "0011_XX1X", .turn_steps = 1 },
    [0b10010010] = { .string = "0011_XX0X", .turn_steps = 1 },
    [0b10011010] = { .string = "0011_XX0X", .turn_steps = 1 },
    [0b10010011] = { .string = "0011_XX1X", .turn_steps = 1 },
    [0b10011011] = { .string = "0011_XX1X", .turn_steps = 1 },
    [0b10010100] = { .string = "0011_XX0X", .turn_steps = 1 },
    [0b10011100] = { .string = "0011_XX0X", .turn_steps = 1 },
    [0b10010101] = { .string = "0011_XX1X", .turn_steps = 1 },
    [0b10011101] = { .string = "0011_XX1X", .turn_steps = 1 },
    [0b10010110] = { .string = "0011_XX0X", .turn_steps = 1 },
    [0b10011110] = { .string = "0011_XX0X", .turn_steps = 1 },
    [0b10010111] = { .string = "0011_XX1X", .turn_steps = 1 },
    [0b10011111] = { .string = "0011_XX1X", .turn_steps = 1 },

    [0b11000000] = { .string = "0011_XX0X", .turn_steps = 2 },
    [0b11000100] = { .string = "0011_XX0X", .turn_steps = 2 },
    [0b11001000] = { .string = "0011_XX1X", .turn_steps = 2 },
    [0b11001100] = { .string = "0011_XX1X", .turn_steps = 2 },
    [0b11000001] = { .string = "0011_XX0X", .turn_steps = 2 },
    [0b11000101] = { .string = "0011_XX0X", .turn_steps = 2 },
    [0b11001001] = { .string = "0011_XX1X", .turn_steps = 2 },
    [0b11001101] = { .string = "0011_XX1X", .turn_steps = 2 },
    [0b11000010] = { .string = "0011_XX0X", .turn_steps = 2 },
    [0b11000110] = { .string = "0011_XX0X", .turn_steps = 2 },
    [0b11001010] = { .string = "0011_XX1X", .turn_steps = 2 },
    [0b11001110] = { .string = "0011_XX1X", .turn_steps = 2 },
    [0b11000011] = { .string = "0011_XX0X", .turn_steps = 2 },
    [0b11000111] = { .string = "0011_XX0X", .turn_steps = 2 },
    [0b11001011] = { .string = "0011_XX1X", .turn_steps = 2 },
    [0b11001111] = { .string = "0011_XX1X", .turn_steps = 2 },

    [0b01100000] = { .string = "0011_XX0X", .turn_steps = 3 },
    [0b01100010] = { .string = "0011_XX0X", .turn_steps = 3 },
    [0b01100100] = { .string = "0011_XX1X", .turn_steps = 3 },
    [0b01100110] = { .string = "0011_XX1X", .turn_steps = 3 },
    [0b01101000] = { .string = "0011_XX0X", .turn_steps = 3 },
    [0b01101010] = { .string = "0011_XX0X", .turn_steps = 3 },
    [0b01101100] = { .string = "0011_XX1X", .turn_steps = 3 },
    [0b01101110] = { .string = "0011_XX1X", .turn_steps = 3 },
    [0b01100001] = { .string = "0011_XX0X", .turn_steps = 3 },
    [0b01100011] = { .string = "0011_XX0X", .turn_steps = 3 },
    [0b01100101] = { .string = "0011_XX1X", .turn_steps = 3 },
    [0b01100111] = { .string = "0011_XX1X", .turn_steps = 3 },
    [0b01101001] = { .string = "0011_XX0X", .turn_steps = 3 },
    [0b01101011] = { .string = "0011_XX0X", .turn_steps = 3 },
    [0b01101101] = { .string = "0011_XX1X", .turn_steps = 3 },
    [0b01101111] = { .string = "0011_XX1X", .turn_steps = 3 },

    [0b01010000 ... 0b01011111] = { .string = "0101_XXXX", .turn_steps = 0 },
    [0b10100000 ... 0b10101111] = { .string = "0101_XXXX", .turn_steps = 1 },

    [0b01110000] = { .string = "0111_X00X", .turn_steps = 0 },
    [0b01110001] = { .string = "0111_X00X", .turn_steps = 0 },
    [0b01110010] = { .string = "0111_X01X", .turn_steps = 0 },
    [0b01110011] = { .string = "0111_X01X", .turn_steps = 0 },
    [0b01110100] = { .string = "0111_X10X", .turn_steps = 0 },
    [0b01110101] = { .string = "0111_X10X", .turn_steps = 0 },
    [0b01110110] = { .string = "0111_X11X", .turn_steps = 0 },
    [0b01110111] = { .string = "0111_X11X", .turn_steps = 0 },
    [0b01111000] = { .string = "0111_X00X", .turn_steps = 0 },
    [0b01111001] = { .string = "0111_X00X", .turn_steps = 0 },
    [0b01111010] = { .string = "0111_X01X", .turn_steps = 0 },
    [0b01111011] = { .string = "0111_X01X", .turn_steps = 0 },
    [0b01111100] = { .string = "0111_X10X", .turn_steps = 0 },
    [0b01111101] = { .string = "0111_X10X", .turn_steps = 0 },
    [0b01111110] = { .string = "0111_X11X", .turn_steps = 0 },
    [0b01111111] = { .string = "0111_X11X", .turn_steps = 0 },


    [0b10110000] = { .string = "0111_X00X", .turn_steps = 1 },
    [0b10111000] = { .string = "0111_X00X", .turn_steps = 1 },
    [0b10110001] = { .string = "0111_X01X", .turn_steps = 1 },
    [0b10111001] = { .string = "0111_X01X", .turn_steps = 1 },
    [0b10110010] = { .string = "0111_X10X", .turn_steps = 1 },
    [0b10111010] = { .string = "0111_X10X", .turn_steps = 1 },
    [0b10110011] = { .string = "0111_X11X", .turn_steps = 1 },
    [0b10111011] = { .string = "0111_X11X", .turn_steps = 1 },
    [0b10110100] = { .string = "0111_X00X", .turn_steps = 1 },
    [0b10111100] = { .string = "0111_X00X", .turn_steps = 1 },
    [0b10110101] = { .string = "0111_X01X", .turn_steps = 1 },
    [0b10111101] = { .string = "0111_X01X", .turn_steps = 1 },
    [0b10110110] = { .string = "0111_X10X", .turn_steps = 1 },
    [0b10111110] = { .string = "0111_X10X", .turn_steps = 1 },
    [0b10110111] = { .string = "0111_X11X", .turn_steps = 1 },
    [0b10111111] = { .string = "0111_X11X", .turn_steps = 1 },

    [0b11010000] = { .string = "0111_X00X", .turn_steps = 2 },
    [0b11010100] = { .string = "0111_X00X", .turn_steps = 2 },
    [0b11011000] = { .string = "0111_X01X", .turn_steps = 2 },
    [0b11011100] = { .string = "0111_X01X", .turn_steps = 2 },
    [0b11010001] = { .string = "0111_X10X", .turn_steps = 2 },
    [0b11010101] = { .string = "0111_X10X", .turn_steps = 2 },
    [0b11011001] = { .string = "0111_X11X", .turn_steps = 2 },
    [0b11011101] = { .string = "0111_X11X", .turn_steps = 2 },
    [0b11010010] = { .string = "0111_X00X", .turn_steps = 2 },
    [0b11010110] = { .string = "0111_X00X", .turn_steps = 2 },
    [0b11011010] = { .string = "0111_X01X", .turn_steps = 2 },
    [0b11011110] = { .string = "0111_X01X", .turn_steps = 2 },
    [0b11010011] = { .string = "0111_X10X", .turn_steps = 2 },
    [0b11010111] = { .string = "0111_X10X", .turn_steps = 2 },
    [0b11011011] = { .string = "0111_X11X", .turn_steps = 2 },
    [0b11011111] = { .string = "0111_X11X", .turn_steps = 2 },

    [0b11100000] = { .string = "0111_X00X", .turn_steps = 3 },
    [0b11100010] = { .string = "0111_X00X", .turn_steps = 3 },
    [0b11100100] = { .string = "0111_X01X", .turn_steps = 3 },
    [0b11100110] = { .string = "0111_X01X", .turn_steps = 3 },
    [0b11101000] = { .string = "0111_X10X", .turn_steps = 3 },
    [0b11101010] = { .string = "0111_X10X", .turn_steps = 3 },
    [0b11101100] = { .string = "0111_X11X", .turn_steps = 3 },
    [0b11101110] = { .string = "0111_X11X", .turn_steps = 3 },
    [0b11100001] = { .string = "0111_X00X", .turn_steps = 3 },
    [0b11100011] = { .string = "0111_X00X", .turn_steps = 3 },
    [0b11100101] = { .string = "0111_X01X", .turn_steps = 3 },
    [0b11100111] = { .string = "0111_X01X", .turn_steps = 3 },
    [0b11101001] = { .string = "0111_X10X", .turn_steps = 3 },
    [0b11101011] = { .string = "0111_X10X", .turn_steps = 3 },
    [0b11101101] = { .string = "0111_X11X", .turn_steps = 3 },
    [0b11101111] = { .string = "0111_X11X", .turn_steps = 3 },

    [0b11110000] = { .string = "1111_0000", .turn_steps = 0 },

    [0b11110001] = { .string = "1111_0001", .turn_steps = 0 },
    [0b11111000] = { .string = "1111_0001", .turn_steps = 1 },
    [0b11110100] = { .string = "1111_0001", .turn_steps = 2 },
    [0b11110010] = { .string = "1111_0001", .turn_steps = 3 },

    [0b11110011] = { .string = "1111_0011", .turn_steps = 0 },
    [0b11111001] = { .string = "1111_0011", .turn_steps = 1 },
    [0b11111100] = { .string = "1111_0011", .turn_steps = 2 },
    [0b11110110] = { .string = "1111_0011", .turn_steps = 3 },

    [0b11110101] = { .string = "1111_0101", .turn_steps = 0 },
    [0b11111010] = { .string = "1111_0101", .turn_steps = 1 },

    [0b11110111] = { .string = "1111_0111", .turn_steps = 0 },
    [0b11111011] = { .string = "1111_0111", .turn_steps = 1 },
    [0b11111101] = { .string = "1111_0111", .turn_steps = 2 },
    [0b11111110] = { .string = "1111_0111", .turn_steps = 3 },

    [0b11111111] = { .string = "1111_1111", .turn_steps = 0 },
};




void bg_level_tilegrid_fn(struct bg_collider_prim* out, int x, int y, void* lv_)
{
    struct bg_level* lv = lv_;
    if(x < 0 || x >= 64 || y < 0 || y >= 64) {
        *out = BG_COLLIDER_PRIM_NONE;
    } else {
        struct bg_tile* tile = &lv->tiles[x][y];
        bg_collider_prim_tx(out, &lv->tile_types[tile->type].collider, vec2(x, y));
    }
}

static int is_tile_same(struct bg_level* lv, int x, int y, int type)
{
    if(x < 0 || x > 63 || y < 0 || y > 63) return 1;
    else if(lv->tiles[x][y].type == type) return 1;
    else return 0;
}

static void bg_level_calc_frames(struct bouncegame_core* bg, struct bg_level* lv)
{
    pg_tex_frame_t background_frames[4] = {
    pg_asset_imgset_get_frame(&bg->imgs, "EnvLabs", "WhiteTile"),
    pg_asset_imgset_get_frame(&bg->imgs, "EnvLabs", "RedTile"),
    pg_asset_imgset_get_frame(&bg->imgs, "EnvLabs", "YellowTile"),
    pg_asset_imgset_get_frame(&bg->imgs, "EnvLabs", "BlackTile"), };
    int i, j;
    for(i = 0; i < 64; ++i) for(j = 0; j < 64; ++j) {
        int tile_type = lv->tiles[i][j].type;
        if(tile_type == 1) {
            int surr[8] = {
                is_tile_same(lv, i,   j+1, tile_type),
                is_tile_same(lv, i+1, j, tile_type),
                is_tile_same(lv, i,   j-1, tile_type),
                is_tile_same(lv, i-1, j, tile_type),

                is_tile_same(lv, i+1, j+1, tile_type),
                is_tile_same(lv, i+1, j-1, tile_type),
                is_tile_same(lv, i-1, j-1, tile_type),
                is_tile_same(lv, i-1, j+1, tile_type),
            };
            uint8_t surr_bits = 
                (surr[0] << 7) |
                (surr[1] << 6) |
                (surr[2] << 5) |
                (surr[3] << 4) |
                (surr[4] << 3) |
                (surr[5] << 2) |
                (surr[6] << 1) |
                (surr[7] << 0);
            char string[32] = "Wall_";
            char* frame_string = tile_set_lookup[surr_bits].string;
            strncpy(string+5, frame_string, 32-5);
            pg_tex_frame_t frame = pg_asset_imgset_get_frame(&bg->imgs, "EnvLabs", string);
            //frame = pg_tex_frame_turn(&frame, tile_set_lookup[surr_bits].turn_steps);
            lv->tiles[i][j].turn = tile_set_lookup[surr_bits].turn_steps;
            lv->tiles[i][j].frame = frame;
        } else {
            lv->tiles[i][j].frame = background_frames[j/4 % 4];
        }
    };
}

void bg_level_init(struct bouncegame_core* bg, struct bg_level* lv)
{
    *lv = (struct bg_level) {
        .tile_types = {
            { "Empty", .collider = BG_COLLIDER_PRIM_NONE, .effect = NULL },
            { "Wall", .collider = BG_COLLIDER_PRIM_AABB(vec2(0,0), vec2(1,1)), .effect = NULL },
        },
    };
    lv->collider = (struct bg_collider) {
        .flags = BG_COLLIDER_STATIC,
        .pos = vec2(0,0),
        .prim = BG_COLLIDER_PRIM_TILEGRID(bg_level_tilegrid_fn, lv),
    };
    lv->collider.prim_ws = lv->collider.prim;
    int i, j;
    for(i = 0; i < 64; ++i) for(j = 0; j < 64; ++j) {
        if(i == 0 || j == 0 || i == 63 || j == 63) {
            lv->tiles[i][j] = (struct bg_tile){ .type = 1 };
        } else if(j % 6 < 2) {
            if((j % 8 == 0 && i % 5 == 2) || ((i+5)%16) <= 3) {
                lv->tiles[i][j] = (struct bg_tile){ .type = 0 };
            } else {
                lv->tiles[i][j] = (struct bg_tile){ .type = 1 };
            }
        } else if(RANDI(16) == 0) {
            lv->tiles[i][j] = (struct bg_tile){ .type = 1 };
        } else {
            lv->tiles[i][j] = (struct bg_tile){ .type = 0 };
        }
    }
    bg_level_calc_frames(bg, lv);
}

void bg_level_draw(struct bouncegame_core* bg, struct bg_level* lv, vec2 offset)
{
    pg_tex_frame_t tile_frame = pg_asset_imgset_get_cell(&bg->imgs, "font8x8", 4, 4);
    offset = vec2_floor(offset);
    int i, j;
    for(i = 0; i < 64; ++i) for(j = 0; j < 64; ++j) {
        if(lv->tiles[i][j].type == 0) continue;
        pg_quadbatch_add_sprite(&bg->quadbatch, PG_EZDRAW_2D(
            .frame = lv->tiles[i][j].frame, .pos = vec3(i*8 + 4 + offset.x, j*8 + 4 + offset.y, 0),
            .rotation = (lv->tiles[i][j].turn) * LM_PI_2,
            .scale = vec2(8,8) ));
    }
}

/************************/
/*  Gameplay effects    */
/************************/
void bg_level_effect(struct bg_gameplay_state* g, struct bg_level* lv, bg_entpool_id_t ent_id)
{
    struct bg_entity* ent = bg_entpool_get(ent_id);
    if(!ent) return;
    int tile_x = LM_CLAMP(ent->pos.x, 0, 63);
    int tile_y = LM_CLAMP(ent->pos.y, 0, 63);
    struct bg_tile* tile = &lv->tiles[tile_x][tile_y];
    bg_tile_effect_fn tile_fn = lv->tile_types[tile->type].effect;
    if(tile_fn) tile_fn(g, tile, ent);
}

/****************************/
/*  Collision resolution    */
/****************************/

void bg_level_calc_trajectory(struct bg_level* lv,
                              struct bg_level_bounce* bounce, int n_bounces)
{
    struct bg_collision coll = {0};
    struct bg_collider_prim ray = BG_COLLIDER_PRIM_CIRCLE(bounce->start, 0.25);
    vec2 move_norm = vec2_norm(bounce->dir);
    vec2 move = vec2_scale(move_norm, 1.0f / 16.0f);
    int step = 0;
    int bounce_i = 0;
    struct bg_level_bounce* bounce_ptr = bounce;
    while(bounce_i < n_bounces) {
        coll.did_collide = 0;
        while(!coll.did_collide && ++step < 1000) {
            bg_collision_prims(&coll, &ray, &lv->collider.prim);
            bg_collider_prim_tx(&ray, &ray, move);
        }
        if(coll.did_collide) {
            //bounce->end = ray.ctr;
            struct bg_impulse imp;
            bg_collision_impulse(&coll, &imp, NULL);
            if(imp.push_len <= 0) {
                bg_collider_prim_tx(&ray, &ray, move);
                continue;
            }
            //vec2 response = bg_impulse_response(&imp, move_norm, 2.0);
            vec2 reflect = vec2_norm(vec2_reflect(bounce_ptr->dir, imp.push_norm));
            if(vec2_dot(bounce_ptr->dir, imp.push_norm) > 0) {
                bg_collider_prim_tx(&ray, &ray, move);
                continue;
            }
            bounce_ptr->bounce = vec2_norm(vec2_reflect(bounce_ptr->dir, imp.push_norm));
            bounce_ptr->end = vec2_add(ray.ctr, vec2_scale(imp.push, 1.0f));
            ray.ctr = bounce_ptr->end;
        } else {
            bounce_ptr->end = bounce->start;
            bounce_ptr->bounce = vec2(0,0);
        }
        if(bounce_i < n_bounces - 1) {
            bounce_ptr[1].start = bounce_ptr->end;
            bounce_ptr[1].dir = bounce_ptr->bounce;
        }
        move = vec2_scale(bounce_ptr->bounce, 1.0f/16.0f);
        bg_collider_prim_tx(&ray, &ray, move);
        ++bounce_i;
        bounce_ptr = bounce + bounce_i;
    }
}
