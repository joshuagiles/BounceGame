
struct bg_tile {
    int type;
    pg_tex_frame_t frame;
    int turn;
};

typedef void (*bg_tile_effect_fn)(struct bg_gameplay_state*, struct bg_tile*, struct bg_entity*);
struct bg_tile_type {
    const char* name;
    struct bg_collider_prim collider;
    bg_tile_effect_fn effect;
};

struct bg_level {
    struct bg_tile tiles[64][64];
    struct bg_tile_type tile_types[64];
    struct bg_collider collider;
};

struct bg_level_bounce {
    vec2 start, dir, end, bounce;
};

void bg_level_init(struct bouncegame_core* bg, struct bg_level* lv);
void bg_level_draw(struct bouncegame_core* bg, struct bg_level* lv, vec2 offset);

void bg_level_effect(struct bg_gameplay_state* g, struct bg_level* lv, bg_entpool_id_t ent_id);

vec2 bg_level_collide_circle(struct bg_level* lv, vec2 pos, float r);
vec2 bg_level_collide_aabb(struct bg_level* lv, vec2 b0, vec2 b1);

void bg_level_calc_trajectory(struct bg_level* lv,
                              struct bg_level_bounce* bounce, int n_bounces);
