#include "procgl/procgl.h"
#include "bouncegame.h"

void bg_start_gameplay(struct bouncegame_core* bg)
{
    struct bg_gameplay_state* g = &bg->gameplay;
    g->bg = bg;
    ARR_INIT(g->game_entities);
    struct bg_entity* plr;
    bg_entpool_id_t plr_id = bg_player_alloc(&plr);
    pg_asset_imgset_set_stepper(&bg->imgs, &plr->anim, "player", "Idle", 0);
    bg_gameplay_add_entity(g, plr_id);
    bg_level_init(bg, &g->cur_level);
    g->plr_id = plr_id;
}

void bg_end_gameplay(struct bouncegame_core* bg)
{
}

static void bg_gameplay_input(struct bg_gameplay_state* g)
{
    struct bg_entity* plr = bg_entpool_get(g->plr_id);
    struct bg_player_data* plr_data = bg_player_data(plr);
}

void bg_gameplay_tick(struct bouncegame_core* bg)
{
    struct bg_gameplay_state* g = &bg->gameplay;
    bg_gameplay_input(g);
    bg_entities_tick(g);
    struct bg_entity* plr_ent = bg_entpool_get(g->plr_id);
    struct bg_player_data* plr_data = bg_player_data(plr_ent);
}

/********************/
/*  Rendering       */
/********************/

void bg_gameplay_draw(struct bouncegame_core* bg, float dt)
{
    struct bg_gameplay_state* g = &bg->gameplay;
    pg_tex_frame_t O_frame = pg_asset_imgset_get_cell(&bg->imgs, "font8x8", 0, 4);
    pg_tex_frame_t X_frame = pg_asset_imgset_get_cell(&bg->imgs, "font8x8", 1, 4);
    pg_quadbatch_reset(&bg->quadbatch);
    struct bg_entity* plr_ent = bg_entpool_get(g->plr_id);

    /*  Calculate view position */
    g->view_want = vec2_scale(plr_ent->pos, 8);
    vec2 view_move = vec2_trunc(vec2_sub(g->view_want, g->view_pos));
    if(fabs(view_move.x) < 8) {
        view_move.x = 0;
    } else if(fabs(view_move.x) > 40) {
        view_move.x -= LM_SGN(view_move.x) * 40;
    } else {
        view_move.x = LM_SGN(view_move.x);
    }
    if(plr_ent->flags & BG_ENTITY_FLAG_GROUND) {
        if(fabs(view_move.y) > 2) {
            view_move.y = LM_SGN(view_move.y);
        } else {
            view_move.y = 0;
        }
    } else {
        if(fabs(view_move.y) < 32) {
            view_move.y = 0;
        } else if(fabs(view_move.y) > 40) {
            view_move.y -= LM_SGN(view_move.y) * 40;
        } else {
            view_move.y = LM_SGN(view_move.y);
        }
    }
    //printf("%f %f\n", VEC_XY(view_move));
    g->view_pos = vec2_add(g->view_pos, view_move);
    g->view_pos =(vec2_scale(plr_ent->pos, 8));
    vec2 view_tx = vec2_scale(g->view_pos, -1);

    /*  Drawing entities and level  */
    //mat4 sprites_tx = mat4_identity();
    mat4 sprites_tx = mat4_translation(vec3(VEC_XY(view_tx)));
    /*
    if(plr_ent) {
        vec2 draw_pos = vec2_add(plr_ent->pos, vec2_scale(plr_ent->vel, dt));
        sprites_tx = mat4_translation(vec3(VEC_XY(vec2_floor(vec2_scale(draw_pos, -8)))));
    }*/
    pg_quadbatch_next(&bg->quadbatch, &sprites_tx);
    bg_level_draw(bg, &g->cur_level, vec2(0,0));
    bg_entities_draw(g, dt);

    /*  Draw the next four bounces  */
    vec4 colors[4][2] = {
        { vec4(1,0.5,0.5,0), vec4(0.5,0.25,0.25,0) },
        { vec4(0.5,1,0.5,0), vec4(0.25,0.5,0.25,0) },
        { vec4(0,0.5,1,0), vec4(0.25,0.25,0.5,0) },
        { vec4(0.5,1,1,0), vec4(0.25,0.5,0.5,0) } };
    int i;
    /*
    for(i = 0; i < 0; ++i) {
        vec2 draw_bounce = vec2_floor(vec2_scale(g->bounces[i].end, 8));
        pg_quadbatch_add_sprite(&bg->quadbatch, PG_EZDRAW_2D(
            .frame = O_frame, .pos = vec3(VEC_XY(draw_bounce)),
            .scale = vec2(4,4), .color_mod = vec4(0,0,0,1),
            .color_add = colors[i][0] ));
        vec2 after_bounce = vec2_add(g->bounces[i].end, vec2_scale(g->bounces[i].bounce, 0.5));
        after_bounce = vec2_floor(vec2_scale(after_bounce, 8));
        pg_quadbatch_add_sprite(&bg->quadbatch, PG_EZDRAW_2D(
            .frame = O_frame, .pos = vec3(VEC_XY(after_bounce)),
            .scale = vec2(4,4), .color_mod = vec4(0,0,0,1),
            .color_add = colors[i][1] ));
    }*/
    pg_quadbatch_draw(&bg->quadbatch, &bg->sprites_pass);
    pg_quadbatch_next(&bg->quadbatch, &sprites_tx);

    /*  Draw the mouse cursor   */
    /*
    sprites_tx = mat4_identity();
    vec2 mouse = pg_input_mouse_pos();
    mouse = vec2_mul(mouse, bg->halfres);
    pg_quadbatch_add_sprite(&bg->quadbatch, PG_EZDRAW_2D(
        .frame = X_frame, .pos = vec3(VEC_XY(mouse)),
        .scale = vec2(4,4), .color_mod = vec4(0,0,0,1),
        .color_add = vec4(0,0,1,0) ));*/

    pg_quadbatch_draw(&bg->quadbatch, &bg->sprites_pass);
    pg_quadbatch_buffer(&bg->quadbatch);
}
